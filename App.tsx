import 'react-native-reanimated'
import { GestureHandlerRootView } from 'react-native-gesture-handler'
import React, { useEffect } from 'react'
import { StatusBar, StyleSheet, Text, View,useColorScheme } from 'react-native'
// import SplashScreen from 'react-native-splash-screen';
import MainStack from './Navigation/MainStackNav/MainStackNavigation';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import reduxStore from './redux/Store';
import { toastConfig } from './components/ToastConfig';
import Toast from 'react-native-toast-message';
import SplashScreen from 'react-native-splash-screen';

const App = () => {

  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  const { store, persistor } = reduxStore();

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
       <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <MainStack />
          <Toast position="bottom" config={toastConfig} />
        </PersistGate>
      </Provider>
    </GestureHandlerRootView>
  )
}

export default App;
