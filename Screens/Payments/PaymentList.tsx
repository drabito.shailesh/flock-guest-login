import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    TouchableWithoutFeedback,
    LayoutChangeEvent,
    Platform
} from 'react-native'
import icons from '../../Constant/icons';
import moment from 'moment';
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
    FadeIn,
    Layout
} from "react-native-reanimated";
import { setBookingIdAction } from '../../redux/actions/BookingId-action';
import { useDispatch, useSelector } from 'react-redux';
import { Skeleton } from 'moti/skeleton';
import axios from '../../axiosConfig'
import RNFetchBlob from 'rn-fetch-blob';
import { getToken } from '../../utils/functions';

const { height, width } = Dimensions.get('window')


export const CollapsableContainer = ({
    children,
    expanded,
}: {
    children: React.ReactNode;
    expanded: boolean;
}) => {
    const [height, setHeight] = useState(0);
    const animatedHeight = useSharedValue(0);

    const onLayout = (event: LayoutChangeEvent) => {
        const onLayoutHeight = event.nativeEvent.layout.height;

        if (onLayoutHeight > 0 && height !== onLayoutHeight) {
            setHeight(onLayoutHeight);
        }
    };

    const collapsableStyle = useAnimatedStyle(() => {
        animatedHeight.value = expanded ? withTiming(height) : withTiming(0);

        return {
            height: animatedHeight.value,
        };
    }, [expanded]);

    return (
        <Animated.View style={[collapsableStyle, { overflow: "hidden", }]}>
            <View style={{ position: "absolute" }} onLayout={onLayout}>
                {children}
            </View>
        </Animated.View>
    );
};


const PaymentList = ({ item, navigation, loading }: any) => {
    // console.log(item?.item, '???????????????????44??????')

    const bookinId = useSelector((state: any) => state?.BookingIdReducer?.BookingId);

    const dispatch = useDispatch()
    const [expanded, setExpanded] = useState<boolean>(false);

    const onPressItem = () => {
        // console.log('first')
        setExpanded(!expanded)
    }

    const SkeletonCommonProps = {
        colorMode: 'light',
        transition: {
            type: 'timing',
            duration: 1500,
        },
        backgroundColor: '#D4D4D4',
    } as const;


    const DownloadInvoice = async (id: any) => {
        const token = await getToken();
        try {
            const date = new Date();
            const filename = `invoice${Math.floor(
                date.getTime() + date.getSeconds() / 2,
            )}.pdf`;

            const dirs = RNFetchBlob.fs.dirs;

            const path =
                Platform.OS == 'ios'
                    ? `${dirs.DocumentDir}/${filename}`
                    : `${dirs.DownloadDir}/${filename}`;
            RNFetchBlob.config({
                fileCache: true,
                appendExt: 'pdf',
                // mime: 'application/pdf',
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    title: filename,
                    description: 'Downloading PDF file...',
                    path: path,
                    mime: 'application/pdf',
                    mediaScannable: true,
                },
                path: path,
            })

                .fetch(
                    'GET', // Use GET instead of POST
                    `https://pgapidev.inn4smart.com/api/properties/164/bookings/${bookinId?.bookingId}/payments/invoices/${id}/download`,
                    {
                        Authorization: `Bearer ${token}`,
                    },
                )
                .then(response => {
                    // if (Platform.OS === 'android') {
                    RNFetchBlob.android.actionViewIntent(
                        response.path(),
                        'application/pdf',
                    );
                    //   setShowBillingModal(false);

                })
                .catch(error => {
                    console.log('Error downloading image:', error);
                    // Handle the error, such as displaying an error message
                });

        } catch (error: any) {
            console.log(error.response)
        }
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setExpanded(false)
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    return (
        <Skeleton.Group show={loading}>
            <View style={styles.wrap}>
                <TouchableWithoutFeedback onPress={() => onPressItem()}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Skeleton
                            height={50}
                            width={50}
                            radius={'round'}
                            {...SkeletonCommonProps}>
                            {item && (
                                <Animated.View
                                    layout={Layout}
                                    entering={FadeIn.duration(1500)}
                                    style={styles.iconView}
                                >
                                    <Image

                                        source={icons.payment} style={{ height: 25, width: 25, tintColor: '#fffffF' }} />
                                </Animated.View>

                            )}
                        </Skeleton>
                        <View style={{ width: width / 1.4, marginHorizontal: 5 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Skeleton height={25} {...SkeletonCommonProps}>
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)}
                                            style={{ color: '#003F6F', fontSize: 16, fontFamily: 'Poppins-Medium' }}>{item?.item?.receiptNo}</Animated.Text>
                                    )}
                                </Skeleton>


                                <Skeleton
                                    height={20}
                                    {...SkeletonCommonProps}>

                                    {item && (
                                        <Animated.View
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)}
                                            style={{
                                                backgroundColor: item?.item?.paymentStatus == 1 ? '#55AC1C1A'
                                                    : item?.item?.paymentStatus == 2 ? '#F087251A' :
                                                        item?.item?.paymentStatus == 3 ? '#EF41411A' :
                                                            '#8080801A', borderRadius: 5, justifyContent: 'center'
                                            }}>
                                            <Text style={{
                                                color: item?.item?.paymentStatus == 1 ? '#55AC1C'
                                                    : item?.item?.paymentStatus == 2 ? '#F08725' :
                                                        item?.item?.paymentStatus == 3 ? '#EF4141' : '#808080',
                                                fontSize: 12, fontFamily: 'Poppins-Medium', paddingHorizontal: 8,
                                                textAlign: 'center',
                                            }}>
                                                {item?.item?.paymentStatus == 1 ? 'Paid'
                                                    : item?.item?.paymentStatus == 2 ? 'Partially Paid' :
                                                        item?.item?.paymentStatus == 3 ? 'Not Paid' : 'Cancelled'}
                                            </Text>
                                        </Animated.View>
                                    )}
                                </Skeleton>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Skeleton
                                    {...SkeletonCommonProps}
                                >
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)} style={{ color: '#000000', fontFamily: 'Poppins-Regular', fontSize: 12 }}>{moment(item?.item?.paymentDate).format('MMM DD, YYYY')}</Animated.Text>
                                    )}
                                </Skeleton>
                                <Skeleton
                                    {...SkeletonCommonProps}
                                >
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)} style={{ color: '#EF4141', fontFamily: 'Poppins-Regular', fontSize: 14 }}>{'\u20B9'}{item?.item?.amount}</Animated.Text>
                                    )}
                                </Skeleton>
                            </View>


                        </View>
                    </View>
                </TouchableWithoutFeedback>

                <CollapsableContainer expanded={expanded}>
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, width: width / 1.17 }}>
                            <Text style={styles.label}>Payment For</Text>
                            <Text style={styles.value}>{item?.item?.paymentFor}</Text>
                        </View>


                        <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 8 }} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                            <Text style={styles.label}>Payment Mode</Text>
                            <Text style={styles.value}>{item?.item?.paymentMode}</Text>
                        </View>
                        {item?.item?.receivedBy ?
                            <>
                                <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 8 }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <Text style={styles.label}>Received By</Text>
                                    <Text style={styles.value}>{item?.item?.receivedBy}</Text>
                                </View>
                            </>
                            : null
                        }
                        {item?.item?.paymentStatus == 1 ?
                            <TouchableOpacity style={styles.downloadButton} onPress={
                                () => DownloadInvoice(item?.item?.id)
                            }>
                                <Text style={styles.buttonLabel}>Download</Text>
                            </TouchableOpacity> : null}
                    </View>
                </CollapsableContainer>

            </View>
        </Skeleton.Group>
    )
}

export default PaymentList;

const styles = StyleSheet.create({
    wrap: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 8,
        paddingHorizontal: 5,
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        paddingVertical: 10
    },
    infobutton: {
        marginLeft: width / 5.1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    iconView: {
        backgroundColor: '#003F6F',
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30
    },
    label: {
        color: 'grey',
        fontSize: 14,
        fontFamily: 'Poppins-Medium'
    },
    value: {
        color: '#003F6F',
        fontSize: 14,
        fontFamily: 'Poppins-SemiBold'
    },
    downloadButton: {
        backgroundColor: '#003F6F',
        height: 40,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 6
    },
    buttonLabel: {
        fontSize: 16,
        color: '#ffffff',
        fontFamily: 'Poppins-SemiBold'
    }
})