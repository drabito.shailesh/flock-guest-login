import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, ScrollView, Alert } from 'react-native';
import axios from '../axiosConfig'
import icons from '../Constant/icons';
import { useDispatch, useSelector } from 'react-redux';
import CustomDropdown from '../components/CustomDropdown';
import { genderOptions, bloodGroupOption } from '../Constant/DummyData';
import AnimatedTextInput from '../components/CustomInputField';
import CustomCalendar from '../components/CustomCalender';
import Toast from 'react-native-toast-message';
import moment from 'moment'
import { Formik } from 'formik';
import * as yup from 'yup';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { setUserDataAction } from '../redux/actions/userDetail-action';
import Loader from '../utils/functions/Loader'
import DateTimePickerModal from "react-native-modal-datetime-picker"

const { height, width } = Dimensions.get('window')


const Profile = ({ navigation }: any) => {
    const dispatch = useDispatch()
    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    // console.log(userDetails, '44444444444444444')
    const [currentDate, setCurrentDate] = useState(null || '');
    const [showCalendar, setShowCalendar] = useState(false);
    const [selectGender, setSelectGender] = useState('');
    const [SelectBloodGroup, setSelectBloodGroup] = useState('');
    const [profilePhoto, setProfilePhoto] = useState<any>('');
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {

        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        setCurrentDate(userDetails?.dateOfBirth)
    }, [])


    const openImagePicker = (callback?: any) => {
        // Options for the image picker
        const options: any = {
            mediaType: 'photo', // Specify that we want to pick photos
            quality: 0.8, // Image quality (0.0 to 1.0)
        };

        // Show a dialog to the user to choose between camera and gallery
        // You can customize this UI as needed
        // Here, we use a simple alert with two options
        Alert.alert(
            'Select Image Source',
            'Choose the source of your image',
            [
                {
                    text: 'Camera',
                    onPress: () => {
                        launchCamera(options, response => {
                            handleImagePickerResponse(response, callback);
                        });
                    },
                },
                {
                    text: 'Gallery',
                    onPress: () => {
                        launchImageLibrary(options, response => {
                            handleImagePickerResponse(response, callback);
                        });
                    },
                },
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
            ],
            { cancelable: true },
        );
    };

    const handleImagePickerResponse = (response: any, callback: any) => {
        // console.log(response, '..........');
        if (response.didCancel) {
            console.log('Image picker was canceled');
        } else if (response.error) {
            console.error('Image picker error:', response.error);
        } else if (response) {
            const source = response.assets[0];
            setProfilePhoto(source)
            console.log(source, '..........');
            // callback(response.uri);
        }
    };



    type FormValues = {
        firstName: string;
        lastName: string;
        emailId: string;
        mobileNo: string;
        emergencyNo: string;
        contactPerson: string;
        aadhaarNo: string;
        panNo: string;
        passportNo: string;
        gstNo:string
    };
    const initialValues: FormValues = {
        firstName: userDetails?.firstName ? userDetails?.firstName : '',
        lastName: userDetails?.lastName ? userDetails?.lastName : '',
        emailId: userDetails?.emailId ? userDetails?.emailId : '',
        mobileNo: userDetails?.mobileNo ? userDetails?.mobileNo : '',
        emergencyNo: userDetails?.emergencyNo ? userDetails?.emergencyNo : '',
        contactPerson: userDetails?.contactPerson ? userDetails?.contactPerson : '',
        aadhaarNo: userDetails?.aadhaarNo ? userDetails?.aadhaarNo : '',
        panNo: userDetails?.panNo ? userDetails?.panNo : '',
        passportNo: userDetails?.passportNo ? userDetails?.passportNo : '',
        gstNo: userDetails?.gstNo ? userDetails?.gstNo : '',
    };


    const validationSchema = yup.object().shape({
        firstName: yup.string().required('First Name is required'),
        lastName: yup.string().required('Last Name is required'),
        emailId: yup.string().email('Invalid email format').required('Email is required'),
        mobileNo: yup.string().matches(/^[0-9]+$/, 'Must be a valid phone number').required('Mobile Number is required'),
        emergencyNo: yup.string().matches(/^[0-9]+$/, 'Must be a valid phone number').required('Emergency Number is required'),
        contactPerson: yup.string().required('Contact Person is required'),
        aadhaarNo: yup.string().matches(/^[0-9]+$/, 'Must be a valid Aadhaar number').required('Aadhaar Number is required'),
        panNo: yup.string().matches(/^[A-Za-z]{5}[0-9]{4}[A-Za-z]$/, 'Must be a valid PAN number').required('PAN Number is required'),
        passportNo: yup.string().required('Passport Number is required'),
        gstNo: yup.string()
        .matches(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z][0-9]$/, 'Invalid GST number Must be in the format 22AAAAA0000A1Z5'),
    });



    const showDatepicker = () => {
        setShowCalendar(true);
    };

    const handleSignup = async (values: any) => {
        const formData = new FormData();
        formData.append("firstName", values.firstName);
        formData.append("lastName", values.lastName);
        formData.append("emailId", values.emailId);
        formData.append("mobileNo", values.mobileNo);
        formData.append("gender", selectGender);
        formData.append("status", userDetails?.status);
        formData.append("emergencyNo", values.emergencyNo);
        formData.append("contactPerson", values.contactPerson);
        formData.append("bloodGroup", SelectBloodGroup?SelectBloodGroup:"");
        formData.append("dateOfBirth", currentDate);
        formData.append("aadhaarNo", values.aadhaarNo);
        formData.append("panNo", values.panNo);
        formData.append("passportNo", values.passportNo);
        formData.append("gstNo", values.gstNo);
        // console.log(formData, './././')
        // return
        try {
            // Check if selectedFile is a File object, then append it with the key "filePath"
            if (profilePhoto) {

                const data = {
                    uri: profilePhoto?.uri,
                    type: profilePhoto?.type,
                    name: profilePhoto?.fileName,
                }
                formData.append("image", data);
            }
            setLoading(true)
            const response = await axios.put(`/auth/guest/profile`, formData)

            Toast.show({
                type: 'success',
                text1: '',
                text2: `  Profile updated successfully.`,
                visibilityTime: 2500,
                autoHide: true,
            });
            await getUserDetail()
            setLoading(false)

        } catch (error: any) {
            console.log(error)
            setLoading(false)

            Toast.show({
                type: 'error',
                text1: '',
                text2: `${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
        }
    };

    const getUserDetail = async () => {
        try {
            const response = await axios.get(`auth/guest/me`)
            dispatch(setUserDataAction(response?.data?.data))
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setProfilePhoto('')
            getUserDetail()
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
                        <Image source={icons.back} style={{ height: 21.19, width: 10.55, right: 2 }} />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Profile</Text>
                    <TouchableOpacity>
                        {/* <Image
                            source={icons.notification}
                            style={{ height: 22, width: 22, tintColor: '#ffffff' }}
                        /> */}
                    </TouchableOpacity>
                </View>

            </View>
            {loading && <Loader />}
            <View>
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={values => handleSignup(values)}>
                    {({
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        values,
                        errors,
                        isValid,
                        touched,
                        resetForm,
                    }) => (
                        <>
                            <ScrollView contentContainerStyle={{ paddingHorizontal: 25, marginTop: height / 12 - 40, paddingBottom: height / 6 }}>
                                <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => openImagePicker()}>
                                    <Image
                                        source={userDetails?.filePath != null && profilePhoto == '' ? { uri: userDetails?.filePath } : profilePhoto != '' ? { uri: profilePhoto?.uri } : icons.user}
                                        style={{ height: 80, width: 80, borderRadius: 40,borderWidth:1,borderColor:'#0000001a' }}
                                    />
                                    <Text style={styles.heading} numberOfLines={1}>
                                        {userDetails?.firstName?.charAt(0)?.toUpperCase() + userDetails?.firstName?.slice(1)}
                                        {' '} {userDetails?.lastName?.charAt(0)?.toUpperCase() + userDetails?.lastName?.slice(1)}
                                    </Text>
                                </TouchableOpacity>

                                <AnimatedTextInput
                                    label="First Name"
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    value={values.firstName}
                                    editable={values.firstName ? false : true}
                                    onChangeText={handleChange('firstName')}

                                />
                                {errors.firstName && touched.firstName ? (
                                    <Text style={styles.errortext}>{errors.firstName}</Text>
                                ) : null}

                                <AnimatedTextInput
                                    label="Last Name"
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    value={values.lastName}
                                    editable={userDetails.lastName ? false : true}
                                    onChangeText={handleChange('lastName')}
                                />
                                {errors.lastName && touched.lastName ? (
                                    <Text style={styles.errortext}>{errors.lastName}</Text>
                                ) : null}

                                <AnimatedTextInput
                                    label="Email"
                                    secureTextEntry={false}
                                    keyboardType={'email-address'}
                                    editable={userDetails.emailId ? false : true}
                                    value={values.emailId}
                                    onChangeText={handleChange('emailId')}
                                />
                                {errors.emailId && touched.emailId ? (
                                    <Text style={styles.errortext}>{errors.emailId}</Text>
                                ) : null}

                                <AnimatedTextInput
                                    label="Mobile Number"
                                    secureTextEntry={false}
                                    keyboardType={'phone-pad'}
                                    value={values.mobileNo}
                                    editable={userDetails.mobileNo ? false : true}
                                    onChangeText={handleChange('mobileNo')}
                                />
                                {errors.mobileNo && touched.mobileNo ? (
                                    <Text style={styles.errortext}>{errors.mobileNo}</Text>
                                ) : null}
                                <View style={{ marginVertical: 5 }}>
                                    <CustomDropdown
                                        options={genderOptions}
                                        disabled={userDetails?.gender ? true : false}
                                        Gender={selectGender}
                                        onSelect={setSelectGender}
                                        label='Gender'
                                        customStyle={{ height: 80, backgroundColor: "#ffffff" }} />
                                </View>
                                <AnimatedTextInput
                                    label="Emergency Phone no."
                                    secureTextEntry={false}
                                    keyboardType={'phone-pad'}
                                    value={values.emergencyNo}
                                    editable={userDetails.emergencyNo ? false : true}
                                    onChangeText={handleChange('emergencyNo')}
                                />
                                {errors.emergencyNo && touched.emergencyNo ? (
                                    <Text style={styles.errortext}>{errors.emergencyNo}</Text>
                                ) : null}
                                <AnimatedTextInput
                                    label="Contact person and Relation"
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    editable={userDetails.contactPerson ? false : true}
                                    value={values.contactPerson}
                                    onChangeText={handleChange('contactPerson')}
                                />
                                {errors.contactPerson && touched.contactPerson ? (
                                    <Text style={styles.errortext}>{errors.contactPerson}</Text>
                                ) : null}
                                <View style={{ marginVertical: 5 }}>
                                    <Text style={{ color: "#003F6F", fontSize: 14, fontFamily: 'Poppins-Regular' }} >Blood Group</Text>
                                    <CustomDropdown
                                        options={bloodGroupOption}
                                        disabled={userDetails?.bloodGroup ? true : false}
                                        value={SelectBloodGroup}
                                        setSelectBloodGroup={setSelectBloodGroup}
                                        label='Blood Group'
                                        customStyle={{ height: 260, backgroundColor: "#ffffff" }} />
                                </View>
                                <Text style={{ color: "#003F6F", fontSize: 14, fontFamily: 'Poppins-Regular', marginTop: 10, }} >Date of birth</Text>
                                <TouchableOpacity
                                    disabled={userDetails?.dateOfBirth ? true : false}
                                    onPress={() => showDatepicker()}
                                    style={[styles.dateofbirth, { borderColor: showCalendar ? '#003F6F' : '#E3E5E5' }]}>
                                    <Text style={{ color: "#003F6F" }}>{currentDate
                                        ? moment(currentDate, 'YYYY/MM/DD').format('DD/MM/YYYY') : "Date of Birth"} </Text>
                                </TouchableOpacity>

                                <AnimatedTextInput
                                    label="Aadhar Card No."
                                    secureTextEntry={false}
                                    keyboardType={'phone-pad'}
                                    editable={userDetails.aadhaarNo ? false : true}
                                    value={values.aadhaarNo}
                                    onChangeText={handleChange('aadhaarNo')}
                                />
                                {errors.aadhaarNo && touched.aadhaarNo ? (
                                    <Text style={styles.errortext}>{errors.aadhaarNo}</Text>
                                ) : null}

                                <AnimatedTextInput
                                    label="PAN Card No."
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    editable={userDetails.panNo ? false : true}
                                    value={values.panNo}
                                    onChangeText={handleChange('panNo')}
                                />
                                {errors.panNo && touched.panNo ? (
                                    <Text style={styles.errortext}>{errors.panNo}</Text>
                                ) : null}

                                <AnimatedTextInput
                                    label="Passport No."
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    editable={userDetails.passportNo ? false : true}
                                    value={values.passportNo}
                                    onChangeText={handleChange('passportNo')}
                                />
                                {errors.passportNo && touched.passportNo ? (
                                    <Text style={styles.errortext}>{errors.passportNo}</Text>
                                ) : null}


                                <AnimatedTextInput
                                    label="GST No."
                                    secureTextEntry={false}
                                    keyboardType={'default'}
                                    editable={userDetails.gstNo ? false : true}
                                    value={values.gstNo}
                                    onChangeText={handleChange('gstNo')}
                                />
                                {errors.gstNo && touched.gstNo ? (
                                    <Text style={styles.errortext}>{errors.gstNo}</Text>
                                ) : null}



                                <View style={{ alignItems: 'center', }}>

                                    <TouchableOpacity style={styles.submitbutton} onPress={() => handleSubmit()}>
                                        <Text style={{ color: '#ffffff', fontSize: 18, fontFamily: 'Inter-SemiBold' }}>Submit</Text>
                                    </TouchableOpacity>
                                </View>

                            </ScrollView>

                            <DateTimePickerModal
                                isVisible={showCalendar}
                                mode="date"
                                isDarkModeEnabled={false}
                                onConfirm={(date: any) => {
                                    console.log('A date has been picked: ', date)
                                    // moment(date).format('yyyy-MM-dd')
                                    setCurrentDate(date);
                                    setShowCalendar(false)
                                }}
                                onCancel={() => {
                                    setShowCalendar(false)
                                }}
                            // minimumDate={new Date()}
                            />

                            {/* {showCalendar && (
                                <CustomCalendar
                                    isVisible={showCalendar}
                                    onClose={() => setShowCalendar(false)}
                                    onDayPress={(day: any) => {
                                        if (day) {
                                            setShowCalendar(false);
                                            setCurrentDate(day.dateString);
                                        }
                                        setShowCalendar(false);
                                    }}
                                    currentDate={new Date()}
                                />
                            )} */}
                        </>
                    )}
                </Formik>
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#FAFAFA"
    },
    header: {
        backgroundColor: '#fffffF',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#000000",
        marginTop: 5
    },
    dateofbirth: {
        borderWidth: 1,
        paddingHorizontal: 15,
        borderRadius: 8,
        height: 50,
        backgroundColor: '#ffffff',

        marginBottom: 5,
        justifyContent: 'center'
    },
    errortext: {
        color: 'red',
        fontSize: 10,
        fontFamily: 'Poppins-Regular',
    },
    backbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#ffffff',
        height: 30,
        width: 30,
        top: 10
    },
    submitbutton: {
        height: 49,
        width: 250,
        marginVertical: 20,
        backgroundColor: "#003F6F",
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }

})