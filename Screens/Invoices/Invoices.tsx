import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList } from 'react-native';
import axios from '../../axiosConfig';
import icons from '../../Constant/icons';
import InvoiceListItem from './InvoiceListItem';

const { height, width } = Dimensions.get('window')


const Invoices = ({ navigation }: any) => {
    const [InvoiceList, setInvoiceList] = useState<any>([])
    const [loading, setLoading] = useState<boolean>(false);

    const getAllInvoices = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`payments/invoices`)
            console.log(response.data)
            setInvoiceList(response?.data?.data?.content)
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAllInvoices()

        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        getAllInvoices()
    }, [])

    const renderInvoice = (item: any) => {
        return <InvoiceListItem item={item} navigation={navigation} loading={loading} />
    }


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Invoice</Text>
                    <Text 
                    // onPress={() => navigation.navigate('Notification')}
                    >
                        {/* <Image
                            source={icons.notification}
                            style={{ height: 22, width: 22, tintColor: '#ffffff' }}
                        /> */}
                    </Text>
                </View>
            </View>
            <View style={{ paddingHorizontal: 22, marginTop: 20, paddingBottom: height / 7 }}>

                <FlatList
                    data={InvoiceList}
                    renderItem={renderInvoice}
                />
            </View>
        </View>
    )
}

export default Invoices

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff"
    },
    header: {
        backgroundColor: '#003F6F',
        padding: 22,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#ffffff"
    },

})