import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList } from 'react-native';
import axios from '../../axiosConfig';
import icons from '../../Constant/icons';
import InvoiceListItem from './InvoiceListItem';
import { useSelector } from 'react-redux';

const { height, width } = Dimensions.get('window')

const InvoiceDetailsByBooking = ({ navigation }: any) => {
    const bookinId = useSelector((state: any) => state?.BookingIdReducer?.BookingId);
    const [InvoiceList, setInvoiceList] = useState<any>([])
    const [loading, setLoading] = useState<boolean>(false);

    const getInvoiceByBookingId = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`payments/invoices?propertyId=164&bookingId=${bookinId?.bookingId}`)
            setInvoiceList(response?.data?.data?.content)

            setLoading(false)
            // console.log(response.data.data.content, "/?//////?/??/???")
        } catch (error: any) {
            console.log(error.response)
            setLoading(false)
        }
    }

    useEffect(() => {
        getInvoiceByBookingId()
    }, [bookinId])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getInvoiceByBookingId()
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    const renderInvoice = (item: any) => {
        return <InvoiceListItem item={item} navigation={navigation} loading={loading}/>
    }


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
                        <Image
                            source={icons.back}
                            style={{ height: 21.19, width: 10.55, right: 2 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Invoice Detail</Text>
                    <Text></Text>
                </View>
            </View>
            <View style={{paddingHorizontal:22,marginTop:20,paddingBottom:height/7}}>

                <FlatList
                    data={InvoiceList}
                    renderItem={renderInvoice}
                />
            </View>


        </View>
    )
}

export default InvoiceDetailsByBooking

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff"
    },
    header: {
        backgroundColor: '#003F6F',
        padding: 22,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#ffffff"
    },
    backbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#ffffff',
        height: 30,
        width: 30
    },
    invoiceoverviewcontainer: {
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        backgroundColor: '#ffffff',
        marginTop: 10
    },

})