import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Dimensions } from 'react-native';
import icons, { notification } from '../../Constant/icons';
import axios from "../../axiosConfig";
import NotificationListItem from './NotificationListItem';
import images from '../../Constant/images';
import { useSelector } from 'react-redux';
import Loader from '../../utils/functions/Loader';

const { height, width } = Dimensions.get('window');

const Notifications = ({ navigation }: any) => {

  const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);

  const [NotificationData, setNotificationData] = useState<any>([])
  const [loading, setLoading] = useState<boolean>(false)

  const getAllNotifications = async () => {
    try {
      setLoading(true)
      const response = await axios.get(`guest/notifications?pageSize=${2000}`);
      setNotificationData(response?.data?.data?.content)
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }

  useEffect(() => {
    getAllNotifications()
  }, [])


  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getAllNotifications()
    });
    // Return the  to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const renderItem = (item: any) => {
    return <NotificationListItem item={item} navigation={navigation} loading={loading} />
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.headerview}>
          <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
            <Image
              source={icons.back}
              style={{ height: 21.19, width: 10.55, right: 2 }}
            />
          </TouchableOpacity>
          <Text style={styles.heading}>Notification</Text>
          <Text></Text>
        </View>
      </View>
      {loading && <Loader />}
      <View style={styles.flatlistContainer}>
        {
          NotificationData.length < 1 && loading == false ?
            <View style={{ alignItems: 'center' }}>
              <Image
                source={images.noNotification}
                style={{ height: 250, width: 300, marginTop: height / 5, opacity: 0.7 }}
              />
              <Text style={{ color: '#000000', fontFamily: "Poppins-Medium", fontSize: 16, textAlign: 'center', paddingHorizontal: 15, marginTop: 20 }}>
                you have no notifications
              </Text>
            </View> :
            <FlatList
              data={NotificationData}
              renderItem={renderItem}
            />
        }
      </View>

    </View>
  )
}

export default Notifications

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FAFAFA"
  },
  header: {
    backgroundColor: '#ffffff',
    padding: 22,
    elevation: 2
  },
  headerview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30
  },
  heading: {
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: "#000000"
  },
  backbutton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 40,
    backgroundColor: '#ffffff',
    height: 30,
    width: 30
  },
  flatlistContainer: {
    marginTop: 20,
    paddingHorizontal: 22,
    paddingBottom: height / 6,
  }

})