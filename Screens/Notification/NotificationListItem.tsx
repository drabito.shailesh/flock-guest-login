import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, TouchableWithoutFeedback, LayoutChangeEvent } from 'react-native'
import icons from '../../Constant/icons';
import moment from 'moment';
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
    FadeIn,
    Layout,

} from "react-native-reanimated";
import { setBookingIdAction } from '../../redux/actions/BookingId-action';
import { useDispatch } from 'react-redux';
import { Skeleton } from 'moti/skeleton';

const { height, width } = Dimensions.get('window')


export const CollapsableContainer = ({
    children,
    expanded,
}: {
    children: React.ReactNode;
    expanded: boolean;
}) => {
    const [height, setHeight] = useState(0);
    const animatedHeight = useSharedValue(0);

    const onLayout = (event: LayoutChangeEvent) => {
        const onLayoutHeight = event.nativeEvent.layout.height;

        if (onLayoutHeight > 0 && height !== onLayoutHeight) {
            setHeight(onLayoutHeight);
        }
    };

    const collapsableStyle = useAnimatedStyle(() => {
        animatedHeight.value = expanded ? withTiming(height) : withTiming(0);

        return {
            height: animatedHeight.value,
        };
    }, [expanded]);

    return (
        <Animated.View style={[collapsableStyle, { overflow: "hidden" }]}>
            <View style={{ position: "absolute" }} onLayout={onLayout}>
                {children}
            </View>
        </Animated.View>
    );
};

export type ContactInfo = {
    name: string;
    email: string;
};

type ContactListItemProps = {
    contact?: ContactInfo | null;
};


const NotificationListItem = ({ item, navigation, loading }: any) => {
    // console.log(item?.item, '?????????????????????????')

    const dispatch = useDispatch()
    const [expanded, setExpanded] = useState<boolean>(false);

    const onPressItem = () => {
        // console.log('first')
        setExpanded(!expanded)
    }

    const SkeletonCommonProps = {
        colorMode: 'light',
        transition: {
            type: 'timing',
            duration: 1000,
        },
        backgroundColor: '#D4D4D4',
    } as const;

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setExpanded(false)
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    return (
        <Skeleton.Group show={loading}>
            <View style={styles.wrap}>
                <TouchableWithoutFeedback onPress={() => onPressItem()}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Skeleton
                            height={50}
                            width={50}
                            radius={'round'}
                            {...SkeletonCommonProps}>
                            {item && (
                                <Animated.View
                                    layout={Layout}
                                    entering={FadeIn.duration(1000)}
                                // style={styles.iconcontainer}
                                >
                                    <Image
                                        source={icons.notificationIcon} style={{ height: 40, width: 40 }} />
                                </Animated.View>

                            )}
                        </Skeleton>
                        <View style={{ flex: 1, marginHorizontal: 5 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <Skeleton height={25} {...SkeletonCommonProps}>
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)}
                                            style={{ color: '#003F6F', fontSize: 16, fontFamily: 'Poppins-Medium' }} numberOfLines={1} >{item?.item?.contentTitle}</Animated.Text>
                                    )}
                                </Skeleton>

                                <Skeleton height={25} {...SkeletonCommonProps}>
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)}
                                            style={{ color: '#242E42', fontSize: 10, fontFamily: 'Poppins-Medium' }} numberOfLines={1} >{moment(item?.item?.createdAt).fromNow()}</Animated.Text>
                                    )}
                                </Skeleton>

                                {/* <Skeleton
                                    height={20}
                                    {...SkeletonCommonProps}>

                                    {item && (
                                        <Animated.View
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)}
                                            style={{
                                                backgroundColor: item?.item?.bookingStatus == 1 ? '#666CFF1A'
                                                    : item?.item?.bookingStatus == 2 ? '#FF66C21A' :
                                                        item?.item?.bookingStatus == 3 ? '#22ACD81A' :
                                                            item?.item?.bookingStatus == 5 ? '#8080801A' :
                                                                item?.item?.bookingStatus == 6 ? '#EF41411A' :
                                                                    '#55AC1C1A', borderRadius: 5, justifyContent: 'center'
                                            }}>
                                            <Text style={{
                                                color: item?.item?.bookingStatus == 1 ? '#666CFF'
                                                    : item?.item?.bookingStatus == 2 ? '#F27D9B' :
                                                        item?.item?.bookingStatus == 3 ? '#22ACD8' :
                                                            item?.item?.bookingStatus == 5 ? '#808080' :
                                                                item?.item?.bookingStatus == 6 ? '#EF4141' :
                                                                    '#55AC1C', fontSize: 12, fontFamily: 'Poppins-Medium', paddingHorizontal: 8, textAlign: 'center',
                                            }}>
                                                {item?.item?.bookingStatus == 1 ? 'Draft'
                                                    : item?.item?.bookingStatus == 2 ? 'Booked' :
                                                        item?.item?.bookingStatus == 3 ? 'Confirmed' :
                                                            item?.item?.bookingStatus == 5 ? 'Cancelled' :
                                                                item?.item?.bookingStatus == 6 ? 'Closed' :
                                                                    'In-House'}
                                            </Text>
                                        </Animated.View>
                                    )}
                                </Skeleton> */}
                            </View>
                            <Skeleton
                                {...SkeletonCommonProps}
                            >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)} style={{ color: '#000000', fontFamily: 'Poppins-Regular', fontSize: 12 }}>{item?.item?.contentMessage}</Animated.Text>
                                    )}
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)} style={{ color: '#003F6F', fontFamily: 'Poppins-SemiBold', fontSize: 12 }}>{item?.item?.bookingType}</Animated.Text>
                                    )}
                                </View>

                            </Skeleton>

                        </View>
                    </View>
                </TouchableWithoutFeedback>

                {/* <CollapsableContainer expanded={expanded}>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 5 }}>
            <Image source={icons.date} style={{ height: 30, width: 30 }} />
            <Text style={{ color: '#272727', fontSize: 12.5, fontFamily: 'Poppins-Regular', paddingHorizontal: 5 }}>{moment(item?.item?.startDate).format('MMMM DD, YYYY')} - {moment(item?.item?.endDate).format('MMMM DD, YYYY')}</Text>
          </View>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 5 }}>
            <Image source={icons.location} style={{ height: 30, width: 30 }} />
            <Text style={{ color: '#272727', fontSize: 12.5, fontFamily: 'Poppins-Regular', paddingHorizontal: 5 }} numberOfLines={1} >{item?.item?.address}, {item?.item?.city}</Text>
          </View>

          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 5, }}>
            <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 5 }}>
              <Image source={icons.bed} style={{ height: 30, width: 30 }} />
              <Text style={{ color: '#272727', fontSize: 12.5, fontFamily: 'Poppins-Regular', paddingHorizontal: 5 }} numberOfLines={1} >{item?.item?.guestCount}</Text>
            </View>
            <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 5, paddingHorizontal: 20 }}>
              <Image source={icons.guest} style={{ height: 30, width: 30 }} />
              <Text style={{ color: '#272727', fontSize: 12.5, fontFamily: 'Poppins-Regular', paddingHorizontal: 5 }} numberOfLines={1} >{item?.item?.guestCount} Guest</Text>
            </View>
            <TouchableOpacity style={styles.infobutton} onPress={() => { navigation.navigate('BookingDetail'), dispatch(setBookingIdAction(item?.item)) }}>
              <Text style={{
                color: '#4C4E64DE', fontFamily: 'Poppins-Regular', fontSize: 12, paddingHorizontal: 3
              }}>More info</Text>
              <Image source={icons.arrowright} style={{ height: 10, width: 13, tintColor: '#4C4E64DE' }} />
            </TouchableOpacity>
          </View>
        </CollapsableContainer> */}

            </View>
        </Skeleton.Group>
    )
}

export default NotificationListItem

const styles = StyleSheet.create({
    wrap: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 8,
        paddingHorizontal: 8,
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        paddingVertical: 10
    },
    infobutton: {
        marginLeft: width / 5.1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    iconcontainer: {
        height: 50,
        width: 50,
        backgroundColor: '#003F6F',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30
    }
})