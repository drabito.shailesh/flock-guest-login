import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList } from 'react-native'
import icons, { booking } from '../Constant/icons'
import axios from '../axiosConfig'
import ListItem from '../components/ListItem'
import Loader from '../utils/functions/Loader'
import BookingFilterModal from '../Modal/BookingFilterModal'
import images from '../Constant/images'
import { useDispatch } from 'react-redux'
import { setUserDataAction } from '../redux/actions/userDetail-action'

const { height, width } = Dimensions.get('window')

const BookingList = ({ navigation }: any) => {

    const dispatch = useDispatch()

    const [BookingsData, setBookingsData] = useState<any>([])
    const [loading, setLoading] = useState<boolean>(false);
    const [showBookingFilterModal, setShowBookingFilterModal] = useState(false);
    const [BookingStatus, setBookingStatus] = useState(4);


    const getAllBookings = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`guests/bookings?bookingStatus=${BookingStatus}`)
            // console.log(response.data.data.content, '././.ds./s.f/e.ew3w3we')
            setBookingsData(response?.data?.data?.content)
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAllBookings()
            setBookingStatus(4)
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        getAllBookings()
        getUserDetail()
    }, [BookingStatus])

    const renderItem = (item: any) => {
        return <ListItem item={item} navigation={navigation} loading={loading} />
    }

    const getUserDetail = async () => {
        try {
            const response = await axios.get(`auth/guest/me`)
            // console.log(response?.data?.data,'??????????????????????????')
            dispatch(setUserDataAction(response?.data?.data))
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
                        <Image
                            source={icons.notification}
                            style={{ height: 22, width: 22, tintColor: '#ffffff' }}
                        />
                    </TouchableOpacity>
                </View>
                {/* {loading && <Loader />} */}
                <View style={styles.inputouterview}>
                    <Image
                        source={icons.search}
                        style={{ height: 18, width: 18, }}
                    />
                    <TextInput
                        style={styles.inputstyle}
                        placeholder='Search...'
                        placeholderTextColor={"#C2C2C2"}
                    />
                    <TouchableOpacity onPress={() => setShowBookingFilterModal(true)}>
                        <Image
                            source={icons.filter}
                            style={{ height: 18, width: 18 }}
                        />
                    </TouchableOpacity>
                </View>

            </View>
            <View style={{ paddingHorizontal: 22, marginTop: 20, paddingBottom: height / 3 }}>
                {/* {BookingsData?.length > 0 && !loading ? */}
                <FlatList
                    data={BookingsData}
                    keyExtractor={(item: any, index: number) => item?.bookingId || index.toString()}
                    renderItem={renderItem}
                />
                {/* :
                    <View style={{ alignItems: 'center', marginTop: height / 5 }}>
                        <Image source={images.nobooking} style={{ height: 214, width: 257, opacity: 0.5 }} />
                    </View>

                } */}
            </View>
            {showBookingFilterModal &&
                <BookingFilterModal
                    isVisible={showBookingFilterModal}
                    onClose={() => setShowBookingFilterModal(false)}
                    setBookingStatus={setBookingStatus}
                />
            }
        </View>
    )
}

export default BookingList

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    header: {
        backgroundColor: '#003F6F',
        padding: 22,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    heading: {
        fontSize: 18,
        color: '#ffffff',
        fontFamily: 'Poppins-SemiBold'
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    inputstyle: {
        width: width / 1.43,
        paddingHorizontal: 10,
        color: '#C2C2C2',
        fontSize: 15,
        fontFamily: 'Poppins-Medium',
        top: 3
    },
    inputouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 30,
        paddingHorizontal: 15
    }
})