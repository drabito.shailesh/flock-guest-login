import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    TouchableWithoutFeedback,
    LayoutChangeEvent,
    Platform
} from 'react-native'
import icons from '../../Constant/icons';
import moment from 'moment';
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
    FadeIn,
    Layout
} from "react-native-reanimated";
import { setBookingIdAction } from '../../redux/actions/BookingId-action';
import { useDispatch, useSelector } from 'react-redux';
import { Skeleton } from 'moti/skeleton';
import axios from '../../axiosConfig'
import RNFetchBlob from 'rn-fetch-blob';
import { getToken } from '../../utils/functions';
import Toast from 'react-native-toast-message';

const { height, width } = Dimensions.get('window')


export const CollapsableContainer = ({
    children,
    expanded,
}: {
    children: React.ReactNode;
    expanded: boolean;
}) => {
    const [height, setHeight] = useState(0);
    const animatedHeight = useSharedValue(0);

    const onLayout = (event: LayoutChangeEvent) => {
        const onLayoutHeight = event.nativeEvent.layout.height;

        if (onLayoutHeight > 0 && height !== onLayoutHeight) {
            setHeight(onLayoutHeight);
        }
    };

    const collapsableStyle = useAnimatedStyle(() => {
        animatedHeight.value = expanded ? withTiming(height) : withTiming(0);

        return {
            height: animatedHeight.value,
        };
    }, [expanded]);

    return (
        <Animated.View style={[collapsableStyle, { overflow: "hidden", }]}>
            <View style={{ position: "absolute" }} onLayout={onLayout}>
                {children}
            </View>
        </Animated.View>
    );
};


const DocumentListItem = ({ item, navigation, loading }: any) => {
    // console.log(item?.item, '???????????????????44??????')

    const bookinId = useSelector((state: any) => state?.BookingIdReducer?.BookingId);

    const dispatch = useDispatch()
    const [expanded, setExpanded] = useState<boolean>(false);

    const onPressItem = () => {
        // console.log('first')
        setExpanded(!expanded)
    }

    const SkeletonCommonProps = {
        colorMode: 'light',
        transition: {
            type: 'timing',
            duration: 1500,
        },
        backgroundColor: '#D4D4D4',
    } as const;


    const DownloadDocument = async (imagePath: any) => {
        try {
            const date = new Date();
            const dirs = RNFetchBlob.fs.dirs;
            const promises = [];
            const filename = `image${Math.floor(
                date.getTime() + date.getSeconds() / 2,
            )}.jpg`;
            let ext = imagePath.split('.').pop();
            ext = ext.indexOf('?') === -1 ? ext : ext.split('?')[0];
            const path =
                Platform.OS == 'ios'
                    ? `${dirs.DocumentDir}/${filename}`
                    : `${dirs.DownloadDir}/${filename}`;

            Toast.show({
                type: 'success',
                text1: '',
                text2: `  Please wait...`,
                visibilityTime: 2500,
                autoHide: true,
            });
            RNFetchBlob.config({
                fileCache: true,
                appendExt: 'pdf',
                // mime: 'application/pdf',
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    title: filename,
                    description: 'Downloading PDF file...',
                    path: path,
                    mime: 'application/pdf',
                    mediaScannable: true,
                },
                path: path,
            })

            RNFetchBlob.config({
                fileCache: true,
                appendExt: ext,
                // mime: 'image/jpeg',
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    title: filename,
                    description: 'Downloading image file...',
                    path: path,
                    mime: 'image/jpeg',
                    mediaScannable: true,
                },
                path: path,
            })
                .fetch('GET', imagePath)
                .then(res => {
                    Toast.show({
                        type: 'success',
                        text1: '',
                        text2: `  Image downloaded successfully`,
                        visibilityTime: 2500,
                        autoHide: true,
                    });
                    if (Platform.OS == 'ios') {
                        RNFetchBlob.ios.previewDocument(res.path());
                    }
                })
                .catch(error => {
                    console.log('Error downloading image:', imagePath, error);
                    // Handle the error, such as displaying an error message
                });

        } catch (error: any) {
            console.log(error.response)
        }

    }


    return (
        <Skeleton.Group show={loading}>
            <View style={styles.wrap}>
                <TouchableWithoutFeedback onPress={() => onPressItem()}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Skeleton
                            height={50}
                            width={50}
                            radius={'round'}
                            {...SkeletonCommonProps}>
                            {item && (
                                <Animated.View
                                    layout={Layout}
                                    entering={FadeIn.duration(1500)}
                                    style={styles.iconView}
                                >
                                    <Image

                                        source={icons.document} style={{ height: 25, width: 25, tintColor: '#fffffF' }} />
                                </Animated.View>

                            )}
                        </Skeleton>
                        <View style={{ width: width / 1.4, marginHorizontal: 15 }}>
                            <View style={{}}>
                                <Skeleton height={25} {...SkeletonCommonProps}>
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)}
                                            style={{ color: '#003F6F', fontSize: 16, fontFamily: 'Poppins-Medium' }}>{item?.item?.documentType}</Animated.Text>
                                    )}
                                </Skeleton>
                                <Skeleton
                                    {...SkeletonCommonProps}
                                >
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)} style={{ color: '#000000', fontFamily: 'Poppins-Regular', fontSize: 12 }}>{item?.item?.uploadDateTime}</Animated.Text>
                                    )}
                                </Skeleton>
                                <Skeleton
                                    {...SkeletonCommonProps}
                                >
                                    {item && (
                                        <Animated.Text
                                            layout={Layout}
                                            entering={FadeIn.duration(1500)} style={{ color: 'grey', fontFamily: 'Poppins-Regular', fontSize: 14 }}>Uploaded by: <Text style={{ color: '#003F6F' }}>{item?.item?.uploadBy}</Text></Animated.Text>
                                    )}
                                </Skeleton>

                                {/* <Skeleton
                                    height={20}
                                    {...SkeletonCommonProps}>

                                    {item && (
                                        <Animated.View
                                            layout={Layout}
                                            entering={FadeIn.duration(1000)}
                                            style={{
                                                backgroundColor: item?.item?.invoiceStatus == 1 ? '#EF41411A'
                                                    : item?.item?.invoiceStatus == 2 ? '#55AC1C1A' :
                                                        item?.item?.invoiceStatus == 3 ? '#EF41411A' :
                                                            '#8080801A', borderRadius: 5, justifyContent: 'center'
                                            }}>
                                            <Text style={{
                                                color: item?.item?.invoiceStatus == 1 ? '#F08725'
                                                    : item?.item?.invoiceStatus == 2 ? '#55AC1C' :
                                                        item?.item?.invoiceStatus == 3 ? '#EF4141' : '#808080',
                                                fontSize: 12, fontFamily: 'Poppins-Medium', paddingHorizontal: 8,
                                                textAlign: 'center',
                                            }}>
                                                {item?.item?.invoiceStatus == 1 ? 'Draft'
                                                    : item?.item?.invoiceStatus == 2 ? 'Invoiced' :
                                                        item?.item?.invoiceStatus == 3 ? 'Due' : 'Paid'}
                                            </Text>
                                        </Animated.View>
                                    )}
                                </Skeleton> */}
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            </View>


                        </View>
                    </View>
                </TouchableWithoutFeedback>

                <CollapsableContainer expanded={expanded}>
                    <View style={{ width: width / 1.17, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <TouchableOpacity style={styles.downloadButton}
                        // onPress={ () => DownloadInvoice(item?.item?.id) }
                        >
                            <Text style={styles.buttonLabel}>View</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.downloadButton} onPress={
                            () => DownloadDocument(item?.item?.documentFile)
                        }>
                            <Text style={styles.buttonLabel}>Download</Text>
                        </TouchableOpacity>
                    </View>
                </CollapsableContainer>

            </View>
        </Skeleton.Group>
    )
}

export default DocumentListItem;

const styles = StyleSheet.create({
    wrap: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 8,
        paddingHorizontal: 8,
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        paddingVertical: 10
    },
    infobutton: {
        marginLeft: width / 5.1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    iconView: {
        backgroundColor: '#003F6F',
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30
    },
    label: {
        color: 'grey',
        fontSize: 14,
        fontFamily: 'Poppins-Medium'
    },
    value: {
        color: '#003F6F',
        fontSize: 14,
        fontFamily: 'Poppins-SemiBold'
    },
    downloadButton: {
        backgroundColor: '#003F6F',
        height: 35,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        width: 140
    },
    buttonLabel: {
        fontSize: 14,
        color: '#ffffff',
        fontFamily: 'Poppins-SemiBold'
    }
})