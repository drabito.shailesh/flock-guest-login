import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList, Platform } from 'react-native';
import axios from '../../axiosConfig';
import icons from '../../Constant/icons';
import PDFView from 'react-native-view-pdf';
import { useSelector } from 'react-redux';
import { Backgroundlogin } from '../../Constant/images';
import DocumentListItem from './DocumentListItem';

const { height, width } = Dimensions.get('window');

const AllDocuments = ({ navigation }: any) => {


    const bookinId = useSelector((state: any) => state?.BookingIdReducer?.BookingId);

    // console.log(bookinId,'/.')

    const [DocumentData, setDocumentData] = useState([])
    const [loading, setLoading] = useState<boolean>(false);

    const getDocuments = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`auth/guest/documents`)
            if (bookinId?.bookingType == "DIRECT") {
                setDocumentData(response?.data?.data?.guestDocuments);
            } else {
                setDocumentData(response?.data?.data?.companyDocuments);
            }
            setLoading(false);
            console.log(response.data.data.content[0], '???????????????')
        } catch (error: any) {
            setLoading(false);
            console.log(error?.response);
        }
    }

    const resources = {
        file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
        url: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        base64: 'JVBERi0xLjMKJcfs...',
    };

    const resourceType = 'url';

    useEffect(() => {
        getDocuments()
    }, [])


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getDocuments()
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    const renderItem = (item: any) => {
        return <DocumentListItem item={item} />
    }

    return (
        <View style={styles.container}>
            {/* <PDFView
                fadeInDuration={250.0}
                style={{ flex: 1 }}
                resource={require('../assets/invoice.pdf')}
                resourceType={resourceType}
                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                onError={(error) => console.log('Cannot render PDF', error)}
            /> */}
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Documents</Text>
                    <Text></Text>
                </View>
            </View>


            <View style={{ paddingHorizontal: 22, marginTop: 20 }}>

                <FlatList
                    data={DocumentData}
                    keyExtractor={(item: any, index: number) => item?.bookingId || index.toString()}
                    renderItem={renderItem}
                // renderItem={((item: any) => {
                //     console.log(item)
                //     return (
                //         <View>
                //             <View style={styles.documentcontainer}>
                //                 <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                //                     <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'red', alignItems: 'center', justifyContent: 'center' }} >

                //                         <Image source={icons.document} style={{ height: 25, width: 25 }} />
                //                     </View>

                //                     <View style={{ paddingHorizontal: 10 }}>
                //                         <Text style={styles.documentName}>{item?.item?.documentType}</Text>
                //                         <Text style={styles.documentdate}>{item?.item?.uploadDateTime}</Text>
                //                         <Text style={[styles.documentdate, { color: 'grey' }]}>Uploaded by: <Text style={{ color: '#003F6F' }}>{item?.item?.uploadBy}</Text></Text>
                //                     </View>
                //                     {/* <View style={{justifyContent:'space-between',flexDirection:'column'}}>
                //                         <TouchableOpacity style={{backgroundColor:'yellow'}}>
                //                             <Text>Download</Text>
                //                         </TouchableOpacity>
                //                         <TouchableOpacity style={{backgroundColor:'yellow'}}>
                //                             <Text>Download</Text>
                //                         </TouchableOpacity>
                //                     </View> */}
                //                 </View>

                //             </View>
                //         </View>
                //     )
                // })}
                />

            </View>

        </View>
    )
}

export default AllDocuments;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff"
    },
    header: {
        backgroundColor: '#003F6F',
        padding: 22,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#ffffff"
    },
    documentcontainer: {
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        backgroundColor: '#ffffff',
        marginTop: 10
    },
    documentName: {
        fontSize: 16,
        fontFamily: 'Poppins-SemiBold',
        color: '#000000'
    },
    documentdate: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color: '#003F6F'
    }

})