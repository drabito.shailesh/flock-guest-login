import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, Keyboard } from 'react-native'
import icons from '../../Constant/icons'
import moment from 'moment';
import Toast from 'react-native-toast-message';
import { useSelector, useDispatch } from 'react-redux';
import axios from '../../axiosConfig'
import { Dropdown } from 'react-native-element-dropdown';
import CustomCalendar from '../../components/CustomCalender';
import * as yup from 'yup';
import { Formik } from 'formik';
import { setGuestInfoDataAction } from '../../redux/actions/GuestInfo-action';
import { setCurrentBookingDataAction } from '../../redux/actions/CurrentBookingData-action';
import Loader from '../../utils/functions/Loader';

const { height, width } = Dimensions.get('window');



const Step_2 = ({ navigation }: any) => {

    const g_BookingPropertyId = useSelector((state: any) => state?.BookingPropertyIdReducer?.BookingProprtyId);
    const g_BookingType = useSelector((state: any) => state?.BookingTypeReducer?.BookingType);
    const g_CurrentBookingData = useSelector((state: any) => state?.CurrentBookingDataReducer?.CurrentBookingData);

    // console.log(g_CurrentBookingData, '.......//././.././.')
    const guestInfo = useSelector((state: any) => state?.GuestInfoDataReducer?.GuestInfoData);
    const dispatch = useDispatch()

    const [genderValue, setGenderValue] = useState(null);
    const [isFocus, setIsFocus] = useState(true);
    const [bloodGroupValue, setBloodGroupValue] = useState(null)
    const [stateList, setStateList] = useState<any>([])
    const [permanentStateId, setPermanentStateId] = useState(null)
    const [currentStateId, setCurrentStateId] = useState(null)
    const [showCalendar, setShowCalendar] = useState<boolean>(false)
    const [DateOfBirth, setDateOfBirth] = useState<any>('');
    const [loading, setLoading] = useState<boolean>(false);
    // const [guestInfo, setGuestInfo] = useState<any>('')

    console.log(guestInfo?.passportNo, "+++++++++++++", guestInfo?.emailId)

    const initialValues = {
        mobileNo: guestInfo?.mobileNo,
        emailId: guestInfo?.emailId != "" ? guestInfo?.emailId : "",
        corporateEmail: guestInfo?.corporateEmail != "" ? guestInfo?.corporateEmail : "",
        firstName: guestInfo?.firstName != "" ? guestInfo?.firstName : "",
        lastName: guestInfo?.lastName != "" ? guestInfo?.lastName : "",
        emergencyNo: guestInfo?.emergencyNo != "" ? guestInfo?.emergencyNo : "",
        contactPerson: guestInfo?.contactPerson ? guestInfo?.contactPerson : "",
        currentCompanyId: guestInfo?.currentCompanyId != "" ? guestInfo?.currentCompanyId : "",
        companyLocationId: guestInfo?.companyLocationId != "" ? guestInfo?.companyLocationId : "",
        aadhaarNo: guestInfo?.aadhaarNo != "" ? guestInfo?.aadhaarNo : "",
        panNo: guestInfo?.panNo ? guestInfo?.panNo : "",
        passportNo: guestInfo?.passportNo ? guestInfo?.passportNo : "",
        permanentAddressType: "P",
        permanentStateId: "",
        permanentCity: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[0]?.city : "",
        permanentAddress: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[0]?.address : "",
        permanentPinCode: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[0]?.pinCode : "",
        currentAddressType: "C",
        currentStateId: "",
        currentCity: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[1]?.city : "",
        currentAddress: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[1]?.address : "",
        currentPinCode: guestInfo?.addresses?.length > 0 ? guestInfo?.addresses[1]?.pinCode : "",
        dateOfBirth: "",
        gender: "",
        bloodGroup: "",
        gstNo: guestInfo?.gstNo ? guestInfo?.gstNo : "",
    };

    const validationSchema = yup.object().shape({
        mobileNo: yup.string()
            .required('Mobile number is required'),
        emailId: yup.string().email('Invalid email address').required('Email is required'),
        corporateEmail: yup.string().email('Invalid email address'),
        firstName: yup.string().required('First name is required'),
        lastName: yup.string(),
        emergencyNo: yup.string().required('Emergency number is required'),
        contactPerson: yup.string(),
        aadhaarNo: yup.string().matches(/^[0-9]{12}$/, 'Invalid Aadhaar number').required('Aadhaar number is required'),
        panNo: yup.string().matches(/^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/, 'Invalid PAN number'),
        passportNo: yup.string(),
        permanentCity: yup.string().required('Permanent city is required'),
        permanentAddress: yup.string().required('Permanent address is required'),
        permanentPinCode: yup.string().matches(/^[0-9]{6}$/, 'Invalid PIN code').required('Permanent PIN code is required'),
        currentCity: yup.string().required('Current city is required'),
        currentAddress: yup.string().required('Current address is required'),
        currentPinCode: yup.string().matches(/^[0-9]{6}$/, 'Invalid PIN code').required('Current PIN code is required'),
        gstNo: yup.string()
            .matches(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z][0-9]$/, 'Invalid GST number Must be in the format 22AAAAA0000A1Z5'),
    });


    const validationSchema2 = yup.object().shape({
        mobileNo: yup.string()
            .required('Mobile number is required'),
        emailId: yup.string().email('Invalid email address').required('Email is required'),
        corporateEmail: yup.string().email('Invalid email address').required('Official Email is required'),
        firstName: yup.string().required('First name is required'),
        lastName: yup.string(),
        gstNo: yup.string()
            .matches(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z][0-9]$/, 'Invalid GST number Must be in the format 22AAAAA0000A1Z5'),
    });

    const genderList = [
        {
            label: "Male",
            value: "MALE"
        },
        {
            label: "female",
            value: "FEMALE"
        }
    ]

    const bloodGroupList = [
        { name: 'AB+', enum: 'AB_Positive' },
        { name: 'AB-', enum: 'AB_Negative' },
        { name: 'A+', enum: 'A_Positive' },
        { name: 'A-', enum: 'A_Negative' },
        { name: 'B+', enum: 'B_Positive' },
        { name: 'B-', enum: 'B_Negative' },
        { name: 'O+', enum: 'O_Positive' },
        { name: 'O-', enum: 'O_Negative' },
    ]

    const getStateList = async () => {
        try {
            const response = await axios.get(`states`)
            setStateList(response?.data?.data)
            // console.log(response.data,'==================')
        } catch (error) {
            console.log(error)
        }
    }

    const getGuestInfo = async () => {
        try {
            const response = await axios.get(`auth/guest/info`)
            dispatch(setGuestInfoDataAction(response?.data?.data))
            // console.log(response.data.data, '--------------')
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    const handleSubmitStep_2 = async (values: any) => {
        // navigation.navigate("Step_3")
        // return
        if (genderValue == null) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  Please select gender`,
                visibilityTime: 2500,
                autoHide: true,
            });
            return
        }

        if (currentStateId == null && g_BookingType == "DIRECT") {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  Please select state`,
                visibilityTime: 2500,
                autoHide: true,
            });
            return
        }

        if (DateOfBirth == '' && g_BookingType == "DIRECT") {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  Please select date of birth`,
                visibilityTime: 2500,
                autoHide: true,
            });
            return
        }

        try {
            const formdata = new FormData();

            values.gender = genderValue
            values.bloodGroup = bloodGroupValue
            values.dateOfBirth = DateOfBirth
            values.permanentStateId = permanentStateId
            values.currentStateId = currentStateId

            formdata.append('mobileNo', values.mobileNo)
            formdata.append('emailId', values.emailId)
            formdata.append('corporateEmail', values.corporateEmail)
            formdata.append('firstName', values.firstName)
            formdata.append('lastName', values.lastName)
            formdata.append('gender', values.gender)
            formdata.append('gstNo', values.gstNo)
            if (g_BookingType == "DIRECT") {
                formdata.append('emergencyNo', values.emergencyNo)
                formdata.append('contactPerson', values.contactPerson)
                formdata.append('currentCompanyId', values.currentCompanyId)
                formdata.append('companyLocationId', values.companyLocationId)
                formdata.append('bloodGroup', values.bloodGroup ? values.bloodGroup : "")
                formdata.append('dateOfBirth', values.dateOfBirth)
                formdata.append('aadhaarNo', values.aadhaarNo)
                formdata.append('panNo', values.panNo)
                formdata.append('passportNo', values.passportNo)
                formdata.append('guestAddresses[0].addressType', 'P')
                formdata.append('guestAddresses[0].stateId', values.permanentStateId)
                formdata.append('guestAddresses[0].city', values.permanentCity)
                formdata.append('guestAddresses[0].address', values.permanentAddress)
                formdata.append('guestAddresses[0].pinCode', values.permanentPinCode)
                formdata.append('guestAddresses[1].addressType', 'C')
                formdata.append('guestAddresses[1].stateId', values.currentStateId)
                formdata.append('guestAddresses[1].city', values.currentCity)
                formdata.append('guestAddresses[1].address', values.currentAddress)
                formdata.append('guestAddresses[1].pinCode', values.currentPinCode)
            }
            setLoading(true)
            const response = await axios.put(`auth/guest/profile`, formdata)
            await getGuestInfo()
            if (g_CurrentBookingData?.id) {
                navigation.navigate("Step_3")
            } else {
                await handleCreateBooking()
            }
            setLoading(false)
        } catch (error: any) {
            console.log(error.response)
            setLoading(false)
        }
    }

    const handleCreateBooking = async () => {
        try {
            const response = await axios.post(`booking-invite?propertyId=${g_BookingPropertyId}&bookingType=${g_BookingType}`)
            // console.log(response.data, '....................')
            dispatch(setCurrentBookingDataAction(response?.data?.data))
            navigation.navigate("Step_3")
        } catch (error: any) {
            setLoading(false)
            console.log(error.response, '????????????????')
        }
    }

    useEffect(() => {
        setGenderValue(guestInfo?.gender)
        setBloodGroupValue(guestInfo?.bloodGroup ? guestInfo?.bloodGroup : null)
        setDateOfBirth(guestInfo?.dateOfBirth)
        if (guestInfo?.addresses?.length > 0) {
            setCurrentStateId(guestInfo?.addresses[1]?.stateId)
            setPermanentStateId(guestInfo?.addresses[0]?.stateId)
        } else {
            setCurrentStateId(null)
            setPermanentStateId(null)
        }
        getGuestInfo()
        getStateList()
    }, [])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setGenderValue(guestInfo?.gender)
            setBloodGroupValue(guestInfo?.bloodGroup ? guestInfo?.bloodGroup : null)
            setDateOfBirth(guestInfo?.dateOfBirth)
            if (guestInfo?.addresses?.length > 0) {
                setCurrentStateId(guestInfo?.addresses[1]?.stateId)
                setPermanentStateId(guestInfo?.addresses[0]?.stateId)
            } else {
                setCurrentStateId(null)
                setPermanentStateId(null)
            }
            getGuestInfo()
            getStateList()
        });
        return unsubscribe;
    }, [navigation]);



    return (
        <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Personal Details</Text>
                    <Text></Text>
                </View>
            </View>
            {loading && <Loader />}
            <ScrollView contentContainerStyle={{ paddingHorizontal: 20, marginTop: 20, paddingBottom: height / 13 - 20 }} >
                <Formik
                    initialValues={initialValues}
                    validationSchema={g_BookingType == "DIRECT" ? validationSchema : validationSchema2}
                    onSubmit={values => handleSubmitStep_2(values)}
                // innerRef={formikRef}
                >
                    {({
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        values,
                        errors,
                        touched,
                        resetForm,
                    }) => (
                        <>
                            {/* {console.log(values, '-*****************')} */}
                            <Text style={styles.inputLabel} >Phone Number<Text style={{ color: "#EF4141" }} >*</Text></Text>
                            <TextInput
                                placeholder="Enter your mobile no."
                                placeholderTextColor={'#003F6F50'}
                                keyboardType={'phone-pad'}
                                editable={false}
                                maxLength={10}
                                style={styles.textInput}
                                value={values?.mobileNo}
                                onBlur={handleBlur('mobileNo')}
                                onChangeText={text => {
                                    handleChange('mobileNo')(text);
                                    if (text.length === 10) {
                                        Keyboard.dismiss();
                                    }
                                }}
                                returnKeyType='done'
                            />
                            {errors.mobileNo && touched.mobileNo && (
                                <Text style={styles.errorText}>{String(errors.mobileNo)}</Text>
                            )}

                            <Text style={styles.inputLabel} >Personal Email<Text style={{ color: "#EF4141" }} >*</Text></Text>
                            <TextInput
                                placeholder="Enter your email"
                                placeholderTextColor={'#003F6F50'}
                                secureTextEntry={false}
                                keyboardType={'email-address'}
                                style={styles.textInput}
                                value={values?.emailId?.toString()}
                                onBlur={handleBlur('emailId')}
                                onChangeText={handleChange('emailId')}
                                returnKeyType='done'
                                editable={guestInfo?.emailId ? false : true}
                            />
                            {errors.emailId && touched.emailId && (
                                <Text style={styles.errorText}>{String(errors.emailId)}</Text>
                            )}

                            <Text style={styles.inputLabel} >Official Email</Text>
                            <TextInput
                                placeholder="Enter your corporate email"
                                placeholderTextColor={'#003F6F50'}
                                secureTextEntry={false}
                                keyboardType={'email-address'}
                                style={styles.textInput}
                                value={values.corporateEmail}
                                onBlur={handleBlur('corporateEmail')}
                                onChangeText={handleChange('corporateEmail')}
                                returnKeyType='done'
                                editable={guestInfo?.corporateEmail ? false : true}
                            />
                            {errors.corporateEmail && touched.corporateEmail && (
                                <Text style={styles.errorText}>{String(errors.corporateEmail)}</Text>
                            )}

                            <Text style={styles.inputLabel} >First Name<Text style={{ color: "#EF4141" }} >*</Text></Text>
                            <TextInput
                                placeholder="Enter your firstname"
                                placeholderTextColor={'#003F6F50'}
                                secureTextEntry={false}
                                keyboardType={'default'}
                                style={styles.textInput}
                                value={values.firstName}
                                onBlur={handleBlur('firstName')}
                                onChangeText={handleChange('firstName')}
                                returnKeyType='done'
                                editable={guestInfo?.firstName ? false : true}
                            />
                            {errors.firstName && touched.firstName && (
                                <Text style={styles.errorText}>{String(errors.firstName)}</Text>
                            )}

                            <Text style={styles.inputLabel} >Last Name</Text>
                            <TextInput
                                placeholder="Enter your last name"
                                placeholderTextColor={'#003F6F50'}
                                secureTextEntry={false}
                                keyboardType={'default'}
                                style={styles.textInput}
                                value={values.lastName}
                                onBlur={handleBlur('lastName')}
                                onChangeText={handleChange('lastName')}
                                returnKeyType='done'
                                editable={guestInfo?.lastName ? false : true}
                            />

                            {errors.lastName && touched.lastName && (
                                <Text style={styles.errorText}>{String(errors.lastName)}</Text>
                            )}





                            <Text style={styles.inputLabel} >Gender<Text style={{ color: "#EF4141" }} >*</Text></Text>
                            <Dropdown
                                disable={guestInfo?.gender ? true : false}
                                style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                                placeholderStyle={styles.placeholderStyle}
                                selectedTextStyle={styles.selectedTextStyle}
                                inputSearchStyle={styles.inputSearchStyle}
                                iconStyle={styles.iconStyle}
                                data={genderList}
                                itemTextStyle={styles.itemTextStyle}
                                maxHeight={300}
                                labelField={"label"}
                                valueField="value"
                                placeholder={'Select Property'}
                                value={genderValue}
                                onFocus={() => setIsFocus(true)}
                                onBlur={() => { setIsFocus(false) }}
                                onChange={(item: any) => {
                                    setGenderValue(item?.value);
                                    setIsFocus(false);
                                }}
                            />

                            <Text style={styles.inputLabel} >GST No<Text style={{ color: "#EF4141" }} ></Text></Text>
                            <TextInput
                                placeholder="Enter GST No."
                                placeholderTextColor={'#003F6F50'}
                                secureTextEntry={false}
                                editable={guestInfo?.gstNo ? false : true}
                                keyboardType={'phone-pad'}
                                maxLength={15}
                                style={styles.textInput}
                                value={values.gstNo}
                                onBlur={handleBlur('gstNo')}
                                onChangeText={text => {
                                    handleChange('gstNo')(text);
                                    if (text.length === 15) {
                                        Keyboard.dismiss();
                                    }
                                }}
                                returnKeyType='done'
                            />

                            {errors.gstNo && touched.gstNo && (
                                <Text style={styles.errorText}>{String(errors.gstNo)}</Text>
                            )}

                            {g_BookingType == "DIRECT" ?
                                <>
                                    <Text style={styles.inputLabel} >Emergency Phone Number<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your emergency mobile no."
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        editable={guestInfo?.emergencyNo ? false : true}
                                        keyboardType={'phone-pad'}
                                        maxLength={10}
                                        style={styles.textInput}
                                        value={values.emergencyNo}
                                        onBlur={handleBlur('emergencyNo')}
                                        onChangeText={text => {
                                            handleChange('emergencyNo')(text);
                                            if (text.length === 10) {
                                                Keyboard.dismiss();
                                            }
                                        }}
                                        returnKeyType='done'
                                    />

                                    {errors.emergencyNo && touched.emergencyNo && (
                                        <Text style={styles.errorText}>{String(errors.emergencyNo)}</Text>
                                    )}


                                    <Text style={styles.inputLabel} >Contact Person and Relation</Text>
                                    <TextInput
                                        placeholder="Enter your contact person name"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        style={styles.textInput}
                                        editable={guestInfo?.contactPerson ? false : true}
                                        value={values.contactPerson}
                                        onBlur={handleBlur('contactPerson')}
                                        onChangeText={handleChange('contactPerson')}
                                        returnKeyType='done'
                                    />

                                    {errors.contactPerson && touched.contactPerson && (
                                        <Text style={styles.errorText}>{String(errors.contactPerson)}</Text>
                                    )}


                                    <Text style={styles.inputLabel} >Blood Group</Text>
                                    <Dropdown
                                        style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        inputSearchStyle={styles.inputSearchStyle}
                                        iconStyle={styles.iconStyle}
                                        disable={guestInfo?.bloodGroup ? true : false}
                                        data={bloodGroupList}
                                        itemTextStyle={styles.itemTextStyle}
                                        maxHeight={300}
                                        labelField={"name"}
                                        valueField="enum"
                                        placeholder={'Select Property'}
                                        // searchPlaceholder="Search..."
                                        value={bloodGroupValue}
                                        onFocus={() => setIsFocus(true)}
                                        onBlur={() => { setIsFocus(false) }}
                                        onChange={(item: any) => {
                                            setBloodGroupValue(item?.enum);
                                            setIsFocus(false);
                                        }}
                                    />


                                    <Text style={styles.inputLabel} >Date of Birth<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TouchableOpacity
                                        disabled={guestInfo?.dateOfBirth ? true : false}
                                        style={styles.datepickerbutton} onPress={() => setShowCalendar(true)}>
                                        <Text style={styles.datelabel}>{DateOfBirth ? moment(DateOfBirth).format('DD-MM-YYYY') : 'Select date of birth  🗓️'}</Text>
                                    </TouchableOpacity>

                                    <Text style={styles.inputLabel} >Aadhaar Number<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your aadhaar number"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'phone-pad'}
                                        editable={guestInfo?.aadhaarNo ? false : true}
                                        maxLength={12}
                                        style={styles.textInput}
                                        value={values.aadhaarNo}
                                        onBlur={handleBlur('aadhaarNo')}
                                        onChangeText={text => {
                                            handleChange('aadhaarNo')(text);
                                            if (text.length === 12) {
                                                Keyboard.dismiss();
                                            }
                                        }}
                                        returnKeyType='done'
                                    />

                                    {errors.aadhaarNo && touched.aadhaarNo && (
                                        <Text style={styles.errorText}>{String(errors.aadhaarNo)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >PAN Number</Text>
                                    <TextInput
                                        placeholder="Enter PAN number"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        editable={guestInfo?.panNo ? false : true}
                                        keyboardType={'default'}
                                        maxLength={10}
                                        style={styles.textInput}
                                        value={values.panNo}
                                        onBlur={handleBlur('panNo')}
                                        onChangeText={text => {
                                            handleChange('panNo')(text);
                                            if (text.length === 10) {
                                                Keyboard.dismiss();
                                            }
                                        }}
                                        returnKeyType='done'
                                    />

                                    {errors.panNo && touched.panNo && (
                                        <Text style={styles.errorText}>{String(errors.panNo)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >Passport Number</Text>
                                    <TextInput
                                        placeholder="Enter Passport number"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        
                                        style={styles.textInput}
                                        value={values.passportNo}
                                        onBlur={handleBlur('passportNo')}
                                        onChangeText={handleChange('passportNo')}
                                        returnKeyType='done'
                                        editable={guestInfo?.passportNo ? false : true}
                                    />

                                    {errors.passportNo && touched.passportNo && (
                                        <Text style={styles.errorText}>{String(errors.passportNo)}</Text>
                                    )}

                                    <Text style={[styles.inputLabel, { textAlign: 'center', fontSize: 18, fontFamily: 'Inter-Bold' }]} >Passport Number</Text>
                                    <Text style={[styles.inputLabel, { textAlign: 'center', marginVertical: 0 }]} >Fill your address details here</Text>

                                    <View style={styles.addressTypeView} >
                                        <Text style={{ fontSize: 18, fontFamily: 'Inter-Bold', color: '#000000' }}>Current Address</Text>
                                    </View>

                                    <Text style={styles.inputLabel} >Full Address<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your address"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        style={styles.textInput}
                                        value={values.currentAddress}
                                        onBlur={handleBlur('currentAddress')}
                                        onChangeText={
                                            handleChange('currentAddress')
                                        }
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />

                                    {errors.currentAddress && touched.currentAddress && (
                                        <Text style={styles.errorText}>{String(errors.currentAddress)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >City<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your city"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        style={styles.textInput}
                                        value={values.currentCity}
                                        onBlur={handleBlur('currentCity')}
                                        onChangeText={
                                            handleChange('currentCity')
                                        }
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />

                                    {errors.currentCity && touched.currentCity && (
                                        <Text style={styles.errorText}>{String(errors.currentCity)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >State<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <Dropdown
                                     disable={guestInfo?.addresses?.length > 0 ? true : false}
                                        style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        inputSearchStyle={styles.inputSearchStyle}
                                        iconStyle={styles.iconStyle}
                                        search
                                        searchPlaceholder='Search...'
                                        data={stateList}
                                        itemTextStyle={styles.itemTextStyle}
                                        maxHeight={300}
                                        labelField={"name"}
                                        valueField="id"
                                        placeholder={'Select state'}
                                        // searchPlaceholder="Search..."
                                        value={currentStateId}
                                        onFocus={() => setIsFocus(true)}
                                        onBlur={() => { setIsFocus(false) }}
                                        onChange={(item: any) => {
                                            setCurrentStateId(item?.id);
                                            setIsFocus(false);
                                        }}
                                    />

                                    <Text style={styles.inputLabel} >Pincode<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your pinCode"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'phone-pad'}
                                        maxLength={6}
                                        style={styles.textInput}
                                        value={values.currentPinCode}
                                        onBlur={handleBlur('currentPinCode')}
                                        onChangeText={text => {
                                            handleChange('currentPinCode')(text);
                                            if (text.length === 6) {
                                                Keyboard.dismiss();
                                            }
                                        }}
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />
                                    {errors.currentPinCode && touched.currentPinCode && (
                                        <Text style={styles.errorText}>{String(errors.currentPinCode)}</Text>
                                    )}

                                    <View style={styles.addressTypeView} >
                                        <Text style={{ fontSize: 18, fontFamily: 'Inter-Bold', color: '#000000' }}>Permanent Address</Text>
                                    </View>

                                    <Text style={styles.inputLabel} >Full Address<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your address"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        style={styles.textInput}
                                        value={values.permanentAddress}
                                        onBlur={handleBlur('permanentAddress')}
                                        onChangeText={
                                            handleChange('permanentAddress')
                                        }
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />

                                    {errors.permanentAddress && touched.permanentAddress && (
                                        <Text style={styles.errorText}>{String(errors.permanentAddress)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >City<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your city"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'default'}
                                        style={styles.textInput}
                                        value={values.permanentCity}
                                        onBlur={handleBlur('permanentCity')}
                                        onChangeText={
                                            handleChange('permanentCity')
                                        }
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />

                                    {errors.permanentCity && touched.permanentCity && (
                                        <Text style={styles.errorText}>{String(errors.permanentCity)}</Text>
                                    )}

                                    <Text style={styles.inputLabel} >State<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <Dropdown
                                     disable={guestInfo?.addresses?.length > 0 ? true : false}
                                        style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        inputSearchStyle={styles.inputSearchStyle}
                                        iconStyle={styles.iconStyle}
                                        data={stateList}
                                        itemTextStyle={styles.itemTextStyle}
                                        maxHeight={300}
                                        search
                                        searchPlaceholder='Search...'
                                        labelField={"name"}
                                        valueField="id"
                                        placeholder={'Select state'}
                                        // searchPlaceholder="Search..."
                                        value={permanentStateId}
                                        onFocus={() => setIsFocus(true)}
                                        onBlur={() => { setIsFocus(false) }}
                                        onChange={(item: any) => {
                                            setPermanentStateId(item?.id);
                                            setIsFocus(false);
                                        }}
                                    />

                                    <Text style={styles.inputLabel} >Pincode<Text style={{ color: "#EF4141" }} >*</Text></Text>
                                    <TextInput
                                        placeholder="Enter your pinCode"
                                        placeholderTextColor={'#003F6F50'}
                                        secureTextEntry={false}
                                        keyboardType={'phone-pad'}
                                        maxLength={6}
                                        style={styles.textInput}
                                        value={values.permanentPinCode}
                                        onBlur={handleBlur('permanentPinCode')}
                                        onChangeText={text => {
                                            handleChange('permanentPinCode')(text);
                                            if (text.length === 6) {
                                                Keyboard.dismiss();
                                            }
                                        }}
                                        returnKeyType='done'
                                        editable={guestInfo?.addresses?.length > 0 ? false : true}
                                    />

                                    {errors.permanentPinCode && touched.permanentPinCode && (
                                        <Text style={styles.errorText}>{String(errors.permanentPinCode)}</Text>
                                    )}
                                </>
                                : null}

                            <View style={styles.bottombuttonouterview}>
                                <TouchableOpacity style={styles.Bottombutton}
                                    onPress={handleSubmit}
                                >
                                    <Text style={[styles.Bottombuttonlabels]}>Next</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.Bottombutton, { backgroundColor: '#003F6F1a' }]} onPress={() => navigation.goBack()}>
                                    <Text style={[styles.Bottombuttonlabels, { color: "#003F6F" }]}>Previous</Text>
                                </TouchableOpacity>
                            </View>
                        </>
                    )}
                </Formik>
            </ScrollView>
            {showCalendar && (
                <CustomCalendar
                    isVisible={showCalendar}
                    onClose={() => setShowCalendar(false)}
                    onDayPress={(day: any) => {
                        if (day) {
                            setShowCalendar(false);
                            setDateOfBirth(day?.dateString);
                        }
                        setShowCalendar(false);
                    }}
                    // minDate={startDate}
                    currentDate={DateOfBirth}
                // maxDate={moment('').format('DD-MM-YYYY')}
                />
            )}
        </View>
    )
}

export default Step_2

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-Bold',
        color: "#000000"
    },
    bottombuttonouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 30
    },
    Bottombuttonlabels: {
        color: "#ffffff",
        fontFamily: 'Inter-Bold',
        fontSize: 16
    },
    Bottombutton: {
        borderWidth: 1,
        borderColor: '#003F6F',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 2.8,
        borderRadius: 8,
        backgroundColor: '#003F6F'
    },
    textInput: {
        color: '#003F6F',
        fontFamily: 'Poppins-Medium',
        paddingHorizontal: 13,
        fontSize: 16,
        borderWidth: 1,
        borderColor: "lightgrey",
        borderRadius: 8
    },
    inputLabel: {
        color: '#272727',
        fontSize: 14,
        fontFamily: "Poppins-Medium",
        marginVertical: 10
    },
    addressTypeView: {
        backgroundColor: '#DEDEDE',
        paddingVertical: 10,
        paddingHorizontal: 5,
        marginTop: 15,
        borderRadius: 8
    },
    errorText: {
        fontSize: 12,
        color: 'red',
        fontFamily: 'Poppins-Regular',
        top: 5,
    },
    dropdown: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 6,
        paddingHorizontal: 10,
        // marginTop:15
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 15,
        color: '#003F6F50',
        fontFamily: 'Poppins-Regular'
    },
    selectedTextStyle: {
        fontSize: 15,
        color: '#003F6F',
        fontFamily: 'Poppins-Medium'
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 15,
        color: 'grey',
        paddingVertical: 5,
        fontFamily: 'Poppins-Regular'
    },
    itemTextStyle: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'grey'
    },
    datepickerbutton: {
        height: 40,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 6,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    datelabel: {
        color: "#003F6F",
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
    },
})