import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, Keyboard, Platform, Pressable } from 'react-native'
import icons from '../../Constant/icons'
import moment from 'moment';
import Toast from 'react-native-toast-message';
import { useSelector, useDispatch } from 'react-redux';
import axios from '../../axiosConfig'
import { Dropdown } from 'react-native-element-dropdown';
import CustomCalendar from '../../components/CustomCalender';
import * as yup from 'yup';
import { Formik, useFormik } from 'formik';
import Loader from '../../utils/functions/Loader';
import { launchImageLibrary } from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import { setCurrentBookingDataAction } from '../../redux/actions/CurrentBookingData-action';

const { height, width } = Dimensions.get('window');


const Step_4 = ({ navigation }: any) => {

    const dispatch = useDispatch()
    let imageGalleryRef: any = useRef([]);
    const g_BookingPropertyId = useSelector((state: any) => state?.BookingPropertyIdReducer?.BookingProprtyId);
    const g_BookingType = useSelector((state: any) => state?.BookingTypeReducer?.BookingType);
    const g_CurrentBookingData = useSelector((state: any) => state?.CurrentBookingDataReducer?.CurrentBookingData);
    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    const guestInfo = useSelector((state: any) => state?.GuestInfoDataReducer?.GuestInfoData);
    const g_BookingStep1Data = useSelector((state: any) => state?.BookingStep1Reducer?.BookingStep1);
    const g_BookingDates = useSelector((state: any) => state?.BookingStep1Reducer?.bookingDates);

    const [loading, setLoading] = useState<boolean>(false);
    const [documentTypesList, setDocumentTypeList] = useState<any>([]);
    const [GuestDocuments, setGuestDocuments] = useState<any>([]);


    const getDocumentTypes = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`auth/guest/document-types?organizationId=${userDetails?.organizationId}&pageSize=100&status=ACTIVE&bookingType=${g_BookingType}`);
            // console.log(response?.data?.data,'???????????')
            // Assuming response.data.data is an array of document types
            const DocumentTypeList = response?.data?.data || [];
            // setDocumentTypeList(DocumentTypeList)
            // Fetch guest documents
            const guestResponse: any = await getGuestDocument();
            // Combine guest documents with document types based on typeSlug
            const combinedList = DocumentTypeList.map((item: any) => {
                const matchingGuestDocument = guestResponse?.data?.data?.content?.find((document: any) => document.documentTypeSlug === item.typeSlug);

                // console.log(matchingGuestDocument)

                if (matchingGuestDocument) {
                    return {
                        ...item,
                        guestDocuments: matchingGuestDocument?.files,
                        guestDocumentsId: matchingGuestDocument?.id
                    };
                } else {
                    return {
                        ...item,
                        guestDocuments: [], // Empty array if no match
                        guestDocumentsId: null
                    };
                }
            });


            // console.log(combinedList[0], '................////////////////')

            setDocumentTypeList(combinedList);
            setLoading(false)
        } catch (error) {
            console.log(error);
            setLoading(false)
        }
    };



    const getGuestDocument = async () => {
        try {

            if (g_BookingType == "DIRECT") {
                const response = await axios.get(`auth/guest/documents`);
                return response;
            } else {
                const response = await axios.get(`company/${g_CurrentBookingData?.companyDetails?.id}/documents`);
                return response;
            }


        } catch (error: any) {
            setLoading(false)
            console.log(error.response);
        }
    };



    const choosePhotoFromLibrary = async (id: any) => {
        try {
            const results: any = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
                allowMultiSelection: true
            });

            await UploadDocument(id, results)

            // console.log(results, '????????????')
            return
            results.forEach((element: any) => {
                imageGalleryRef.current = [
                    ...imageGalleryRef.current,
                    { url: element.uri },
                ];
            });
        } catch (err: any) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    };


    const UploadDocument = async (typeId: any, result: any) => {
        try {
            const formData = new FormData();
            formData.append(`documentRequest[0].documentTypeId`, typeId);
            result?.forEach((item: any, i: number) => {
                formData.append(`documentRequest[0].documentImages[${i}]`, item);
            });

            if (g_BookingType == "DIRECT") {
                const response = await axios.post(`auth/guest/documents`, formData);
                await getDocumentTypes()
            } else {
                const response = await axios.post(`company/${g_CurrentBookingData?.companyDetails?.id}/documents`, formData);
                await getDocumentTypes()
            }

        } catch (error: any) {
            console.log(error.response)
        }
    }


    const handleGuestDocument = async (docId: any, fileId: any) => {
        try {
            const response = await axios.delete(`auth/guest/documents/${docId}/file/${fileId}`,);
            await getDocumentTypes()
        } catch (error) {
            console.log(error)
        }
    };


    // console.log(g_CurrentBookingData, '7777777777777777777777')

    const handleUpdateBooking = async () => {
        // navigation.navigate("Step_5")
        // return
        const validationErrors = documentTypesList
            .filter((item: any) => item.isRequired && item.guestDocuments.length < 1)
            .map((item: any) => `${item.typeName} is required`);
        if (validationErrors?.length > 0) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${validationErrors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
            return
        }

        try {
            setLoading(true)

            let guestAddressId = 0
            const guestDocumentsIds = documentTypesList
                .map((item: any) => item?.guestDocumentsId) // Map to get guestDocumentsId
                .filter((id:any) => id !== null); // Filter out null values

            console.log(guestDocumentsIds);
// return
            for (let i = 0; i <= guestInfo?.addresses?.length; i++) {
                if (guestInfo?.addresses[i]?.addressType == "C") {
                    guestAddressId = guestInfo?.addresses[i]?.id
                    break
                }
            }

            const data = {
                draftStage: 3,
                guestId: guestInfo?.id,
                guestAddressId: guestAddressId,
                companyId: g_CurrentBookingData?.companyDetails?.id,
                companyLocationId: g_CurrentBookingData?.companyLocationDetails?.id,
                documentIds: guestDocumentsIds
            }
            // console.log(data, '????????????????')
            const response = await axios.put(`bookings/${g_CurrentBookingData?.id}?propertyId=${g_BookingPropertyId}&bookingType=${g_BookingType}`, data)
            dispatch(setCurrentBookingDataAction(response?.data?.data))
            await updateBookingDraftStage_4()
            setLoading(false)
        } catch (error: any) {
            setLoading(false)
// console.log(error.response)
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
        }
    }


    const updateBookingDraftStage_4 = async () => {

        const validationErrors = documentTypesList
            .filter((item: any) => item.isRequired && item.guestDocuments.length < 1)
            .map((item: any) => `${item.typeName} is required`);

        if (validationErrors?.length > 0) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${validationErrors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
            return
        }

        try {
            setLoading(true)

            let guestAddressId = 0
             const guestDocumentsIds = documentTypesList
            .map((item: any) => item?.guestDocumentsId) // Map to get guestDocumentsId
            .filter((id:any) => id !== null);

            for (let i = 0; i <= guestInfo?.addresses?.length; i++) {
                if (guestInfo?.addresses[i]?.addressType === "C") {
                    guestAddressId = guestInfo?.addresses[i]?.id
                    break
                }
            }

            const newArray = g_BookingStep1Data.map((obj: any) => {
                const { id, breakfast, currentRate, ...rest } = obj; // destructuring assignment to remove 'foo' from the object
                return rest;
            });


            const data = {
                draftStage: 4,
                guestId: guestInfo?.id,
                guestAddressId: guestAddressId,
                companyId: g_CurrentBookingData?.companyDetails?.id,
                companyLocationId: g_CurrentBookingData?.companyLocationDetails?.id,
                documentIds: guestDocumentsIds,
                checkInDate: g_BookingDates?.startDate,
                checkOutDate: g_BookingDates?.endDate,
                bookingDetailsRequest: newArray
            }

            // console.log(data, '???????????--------')
            // return

            const response = await axios.put(`bookings/${g_CurrentBookingData?.id}?propertyId=${g_BookingPropertyId}&bookingType=${g_BookingType}`, data)
            dispatch(setCurrentBookingDataAction(response?.data?.data))
            navigation.navigate("Step_5")
            setLoading(false)
        } catch (error: any) {
            setLoading(false)
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
        }
    }


    useEffect(() => {
        getDocumentTypes()
    }, [])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getDocumentTypes()
        });
        return unsubscribe;
    }, [navigation]);

    return (
        <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Document Upload</Text>
                    <Text></Text>
                </View>
            </View>
            {loading && <Loader />}

            <ScrollView contentContainerStyle={{ paddingHorizontal: 20, marginTop: 20, paddingBottom: height / 13 - 20 }} >
                {
                    documentTypesList?.map((item: any, index: any) => (
                        <View key={index} >
                            <Text style={{ color: '#000000', fontSize: 17, fontFamily: 'Inter-SemiBold', marginVertical: 10 }}>{item?.typeName}<Text style={{ color: "#EF4141" }}>{item?.isRequired ? "*" : ""}</Text></Text>
                            {item?.guestDocuments?.length > 0 ?
                                <View>
                                    {
                                        item?.guestDocuments?.map((document: any, docIndex: any) => (
                                            <View key={docIndex}>
                                                <View style={styles.filesouterView}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }} >
                                                        <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Inter-Medium',flex:1 }} numberOfLines={1} >{document?.fileName}</Text>
                                                        <TouchableOpacity
                                                            style={{ alignItems: 'center', justifyContent: 'center' }}
                                                            onPress={() => handleGuestDocument(item?.guestDocumentsId, document?.id)}
                                                        >
                                                            <Image
                                                                source={icons.delet}
                                                                style={{ height: 30, width: 30 }}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        ))
                                    }
                                </View>
                                :
                                <View>
                                    <View style={styles.coverphoto}>
                                        <TouchableOpacity
                                            style={{ alignItems: 'center', justifyContent: 'center' }}
                                            onPress={() => choosePhotoFromLibrary(item?.id)}>
                                            <Image
                                                source={icons.plus}
                                                style={{ height: 30, width: 30 }}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins-Medium',
                                                    marginTop: 10,
                                                    color: '#222425',
                                                }}>
                                                Click to upload
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }
                        </View>
                    ))
                }


                <View style={styles.bottombuttonouterview}>
                    <TouchableOpacity style={styles.Bottombutton}
                        onPress={() => handleUpdateBooking()}
                    >
                        <Text style={[styles.Bottombuttonlabels]}>Next</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.Bottombutton, { backgroundColor: '#003F6F1a' }]} onPress={() => navigation.goBack()}>
                        <Text style={[styles.Bottombuttonlabels, { color: "#003F6F" }]}>Previous</Text>
                    </TouchableOpacity>
                </View>

            </ScrollView>

        </View>
    )
}

export default Step_4

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-Bold',
        color: "#000000"
    },
    bottombuttonouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 30
    },
    Bottombuttonlabels: {
        color: "#ffffff",
        fontFamily: 'Inter-Bold',
        fontSize: 16
    },
    Bottombutton: {
        borderWidth: 1,
        borderColor: '#003F6F',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 2.8,
        borderRadius: 8,
        backgroundColor: '#003F6F'
    },
    coverphoto: {
        height: 159,
        // backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: '#D7D7D7',
        marginTop: 15,
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    filesouterView: {
        height: 50,
        backgroundColor: '#07bc0c1a',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: '#07bc0c',
        marginVertical: 5,
        borderRadius: 7,
        paddingHorizontal: 10,
        // alignItems: 'center',
        justifyContent: 'center',
    }
})













{/* 

                <View style={styles.coverphoto}>
                    <TouchableOpacity
                        style={{ alignItems: 'center', justifyContent: 'center' }}
                        onPress={() => openGallery()}>
                        <Image
                            source={icons.plus}
                            style={{ height: 30, width: 30 }}
                        />
                        <Text
                            style={{
                                fontSize: 16,
                                fontFamily: 'Poppins-Medium',
                                marginTop: 10,
                                color: '#222425',
                            }}>
                            Click to upload
                        </Text>
                    </TouchableOpacity>
                </View> */}




// const getDocumentTypes = async () => {
//     try {
//         const response = await axios.get(`auth/guest/document-types?organizationId=${userDetails?.organizationId}&pageSize=100&status=ACTIVE&bookingType=${g_BookingType}`);
//         setDocumentTypeList(response?.data?.data)

//         setTimeout(async () => {

//             await getGuestDocument()
//         }, 1000);
//         // console.log(response.data, '121311212')
//     } catch (error) {
//         console.log(error)
//     }
// }