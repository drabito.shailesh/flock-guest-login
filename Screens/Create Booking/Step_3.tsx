import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, Keyboard } from 'react-native'
import icons from '../../Constant/icons'
import moment from 'moment';
import Toast from 'react-native-toast-message';
import { useSelector, useDispatch } from 'react-redux';
import axios from '../../axiosConfig'
import { Dropdown } from 'react-native-element-dropdown';
import CustomCalendar from '../../components/CustomCalender';
import * as yup from 'yup';
import { Formik, useFormik } from 'formik';
import Loader from '../../utils/functions/Loader';
import { setCurrentBookingDataAction } from '../../redux/actions/CurrentBookingData-action';

const { height, width } = Dimensions.get('window');

const Step_3 = ({ navigation }: any) => {

  const g_BookingPropertyId = useSelector((state: any) => state?.BookingPropertyIdReducer?.BookingProprtyId);
  const g_BookingType = useSelector((state: any) => state?.BookingTypeReducer?.BookingType);
  const g_CurrentBookingData = useSelector((state: any) => state?.CurrentBookingDataReducer?.CurrentBookingData);
  const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
  const guestInfo = useSelector((state: any) => state?.GuestInfoDataReducer?.GuestInfoData);

  // console.log(g_CurrentBookingData, '??????????')

  const dispatch = useDispatch()

  const [stateList, setStateList] = useState<any>([])
  const [StateId, setStateId] = useState(null)
  const [loading, setLoading] = useState<boolean>(false);
  const [isFocus, setIsFocus] = useState(true);
  const [companyValue, setCompanyValue] = useState(null)
  const [companyList, setCompanyList] = useState<any>([])
  const [companyLocationId, setCompanyLocationId] = useState<any>(null)


  // console.log(companyValue, '???????????????')

  const initialValues = {
    companyName: "",
    webSite: "",
    address: "",
    city: "",
    stateId: "",
    pinCode: "",
    contactEmail: "",
    contactPhone: ""
  }

  const validationSchema = yup.object().shape({
    companyName: yup.string().required('Company name is required'),
    webSite: yup.string(),
    address: yup.string().required('Address is required'),
    city: yup.string().required('City is required'),
    pinCode: yup.string().matches(/^[0-9]{6}$/, 'Invalid PIN code').required('PIN code is required'),
    contactEmail: yup.string().email('Invalid email address'),
    contactPhone: yup.string().matches(/^[0-9]{10}$/, 'Invalid phone number'),
  });

  const formik = useFormik({
    initialValues: initialValues,
    // validationSchema: validationSchema,
    onSubmit: values => {
      handleSubmitStep_3(values);
    },
  });

  const getCompanyList = async () => {
    try {
      const response = await axios.get(`organizations/${userDetails?.organizationId}/company`)
      // console.log(response.data.data)
      setCompanyList(response?.data?.data?.content)
    } catch (error) {
      console.log(error)
    }
  }

  const getStateList = async () => {
    try {
      const response = await axios.get(`states`)
      setStateList(response?.data?.data)
      // console.log(response.data,'==================')
    } catch (error) {
      console.log(error)
    }
  }

  const getCurrentCompanyData = async () => {
    try {
      const response = await axios.get(`auth/guest/current-company`)
      const companyData = response?.data?.data;
      // console.log(companyData, '45545454544454545')
      formik.setValues({
        ...formik.values,
        companyName: companyData?.companyName || "",
        webSite: companyData?.webSite || "",
        address: companyData?.companyLocation?.address || "",
        city: companyData?.companyLocation?.city || "",
        pinCode: companyData?.companyLocation?.pinCode || "",
        contactEmail: companyData?.contactEmail || "",
        contactPhone: companyData?.contactPhone || ""
      });
      setCompanyLocationId(companyData?.companyLocation?.id ? companyData?.companyLocation?.id : null)
      setStateId(companyData?.companyLocation?.stateId ? companyData?.companyLocation?.stateId : null)
      setCompanyValue(companyData?.id)
    } catch (error) {
      console.log(error)
    }
  }

  const getCompanyDataById = async () => {
    try {
      // const response = await axios.get(`organizations/${userDetails?.organizationId}/company/${companyValue}`)
      // const companyData = response?.data?.data;
      // // console.log(companyData)
      // formik.setValues({
      //   ...formik.values,
      //   companyName: companyData?.companyName || "",
      //   webSite: companyData?.webSite || "",
      //   address: companyData?.companyLocation?.address || "",
      //   city: companyData?.companyLocation?.city || "",
      //   pinCode: companyData?.companyLocation?.pinCode || "",
      //   contactEmail: companyData?.contactEmail || "",
      //   contactPhone: companyData?.contactPhone || ""
      // });
      // setStateId(companyData?.companyLocation?.stateId ? companyData?.companyLocation?.stateId : null)
      for (let i = 0; i <= companyList?.length; i++) {
        if (companyList[i]?.id == companyValue) {
          formik.setValues({
            ...formik.values,
            companyName: companyList[i]?.companyName || "",
            webSite: companyList[i]?.webSite || "",
            address: companyList[i]?.companyLocation?.address || "",
            city: companyList[i]?.companyLocation?.city || "",
            pinCode: companyList[i]?.companyLocation?.pinCode || "",
            contactEmail: companyList[i]?.contactEmail || "",
            contactPhone: companyList[i]?.contactPhone || ""
          });
          setStateId(companyList[i]?.companyLocation?.stateId ? companyList[i]?.companyLocation?.stateId : null)
        }
      }
    } catch (error) {
      console.log(error)
    }
  }


  const handleSubmitStep_3 = async (values: any) => {
    const formdata = new FormData();
    values.stateId = StateId
    formdata.append('companyName', values.companyName)
    formdata.append('contactEmail', values.contactEmail)
    formdata.append('contactPhone', values.contactPhone)
    formdata.append('webSite', values.webSite)
    formdata.append('companyLocationRequest.address', values.address)
    formdata.append('companyLocationRequest.city', values.city)
    formdata.append('companyLocationRequest.stateId', values.stateId)
    formdata.append('companyLocationRequest.pinCode', values.pinCode)

    try {
      setLoading(true)
      if (companyValue) {
        const response = await axios.put(`auth/guest/company/${companyValue}`, formdata)
        const companyData = response?.data?.data;
        await handleUpdateBooking(companyData?.id)

      } else {
        const response = await axios.post(`auth/guest/company`, formdata)
        const companyData = response?.data?.data;
        setCompanyLocationId(companyData?.companyLocation?.id)
        await handleUpdateBooking(companyData?.id)
      }


      // console.log(companyData?.id, '**********************', guestInfo?.addresses)

      setLoading(false)
    } catch (error: any) {
      setLoading(false)
      Toast.show({
        type: 'error',
        text1: '',
        text2: `  ${error?.response?.data?.errors[0]}`,
        visibilityTime: 2500,
        autoHide: true,
      });
      console.log(error)
    }
  }



  const handleUpdateBooking = async (companyId: any) => {

    let guestAddressId = 0

    for (let i = 0; i <= guestInfo?.addresses?.length; i++) {
      if (guestInfo?.addresses[i]?.addressType === "C") {
        guestAddressId = guestInfo?.addresses[i]?.id
        break
      }
    }
    const data = {
      draftStage: 2,
      guestId: guestInfo?.id,
      guestAddressId: guestAddressId,
      companyId: companyId,
      companyLocationId: companyLocationId
    }
    // navigation.navigate("Step_4")
    // return
    try {
      setLoading(true)
      const response = await axios.put(`bookings/${g_CurrentBookingData?.id}?propertyId=${g_BookingPropertyId}&bookingType=${g_BookingType}`, data)

      // console.log(response.data.data, '[[[[[[[[[[[[[[[[')

      dispatch(setCurrentBookingDataAction(response?.data?.data))
      navigation.navigate("Step_4")
      setLoading(false)
    } catch (error: any) {
      setLoading(false)
      Toast.show({
        type: 'error',
        text1: '',
        text2: `  ${error?.response?.data?.errors[0]}`,
        visibilityTime: 2500,
        autoHide: true,
      });
      console.log(error.response)
    }
  }



  useEffect(() => {
    getCompanyDataById()
    getCompanyList()
  }, [companyValue])


  useEffect(() => {
    getStateList()
    getCurrentCompanyData()
    getCompanyList()
  }, [])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getStateList()
      getCurrentCompanyData()
      getCompanyList()
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
      <View style={styles.header}>
        <View style={styles.headerview}>
          <TouchableOpacity>
            <Image
              source={icons.drawer}
              style={{ height: 22, width: 22 }}
            />
          </TouchableOpacity>
          <Text style={styles.heading}>Company Details</Text>
          <Text></Text>
        </View>
      </View>
      {loading && <Loader />}

      <ScrollView contentContainerStyle={{ paddingHorizontal: 20, marginTop: 20, paddingBottom: height / 13 - 20 }} >
        <Formik
          {...formik}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={values => handleSubmitStep_3(values)}
        >
          <>
            <Text style={styles.inputLabel} >Search Company Name<Text style={{ color: "#EF4141" }} ></Text></Text>
            <Dropdown
              style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              data={companyList}
              itemTextStyle={styles.itemTextStyle}
              maxHeight={300}
              labelField={"companyName"}
              valueField="id"
              placeholder={'Select Company'}
              value={companyValue}
              onFocus={() => setIsFocus(true)}
              onBlur={() => { setIsFocus(false) }}
              onChange={(item: any) => {
                setCompanyValue(item?.id);
                setIsFocus(false);
              }}
              renderRightIcon={() =>
                <TouchableOpacity style={{ height: 25, width: 25, alignItems: 'center', justifyContent: 'center' }} onPress={() => setCompanyValue(null)} >
                    {companyValue?
                    <Image source={icons.cross} style={{ height: 12, width: 12,tintColor:'grey' }} />
                    :
                    <Image source={icons.downarrow} style={styles.iconStyle} />
                    }
                </TouchableOpacity>
            }
             
            />
            <Text style={styles.inputLabel} >Company Name<Text style={{ color: "#EF4141" }} >*</Text></Text>
            <TextInput
              placeholder="Enter your company name"
              placeholderTextColor={'#003F6F50'}
              keyboardType={'default'}
              style={styles.textInput}
              value={formik.values?.companyName}
              onBlur={formik.handleBlur('companyName')}
              onChangeText={formik.handleChange('companyName')}
              returnKeyType='done'
            />
            {formik.errors.companyName && formik.touched.companyName && (
              <Text style={styles.errorText}>{String(formik.errors.companyName)}</Text>
            )}

            <Text style={styles.inputLabel} >Website<Text style={{ color: "#EF4141" }} ></Text></Text>
            <TextInput
              placeholder="Enter your webSite"
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'default'}
              style={styles.textInput}
              value={formik?.values?.webSite}
              onBlur={formik?.handleBlur('webSite')}
              onChangeText={formik?.handleChange('webSite')}
              returnKeyType='done'
            />
            {formik?.errors.webSite && formik?.touched.webSite && (
              <Text style={styles.errorText}>{String(formik?.errors.webSite)}</Text>
            )}

            <Text style={styles.inputLabel} >Contact Email</Text>
            <TextInput
              placeholder="Enter your email"
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'email-address'}
              style={styles.textInput}
              value={formik?.values.contactEmail}
              onBlur={formik?.handleBlur('contactEmail')}
              onChangeText={formik?.handleChange('contactEmail')}
              returnKeyType='done'
            />
            {formik?.errors.contactEmail && formik?.touched.contactEmail && (
              <Text style={styles.errorText}>{String(formik?.errors.contactEmail)}</Text>
            )}

            <Text style={styles.inputLabel} >Contact Phone<Text style={{ color: "#EF4141" }} ></Text></Text>
            <TextInput
              placeholder="Enter your mobile no."
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'phone-pad'}
              maxLength={10}
              style={styles.textInput}
              value={formik?.values.contactPhone}
              onBlur={formik?.handleBlur('contactPhone')}
              onChangeText={formik?.handleChange('contactPhone')}
              returnKeyType='done'
            />
            {formik?.errors.contactPhone && formik?.touched.contactPhone && (
              <Text style={styles.errorText}>{String(formik?.errors.contactPhone)}</Text>
            )}

            <Text style={styles.inputLabel} >Address<Text style={{ color: "#EF4141" }} >*</Text></Text>
            <TextInput
              placeholder="Enter your address"
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'default'}
              style={styles.textInput}
              value={formik?.values.address}
              onBlur={formik?.handleBlur('address')}
              onChangeText={
                formik?.handleChange('address')
              }
              returnKeyType='done'
            />

            {formik?.errors.address && formik?.touched.address && (
              <Text style={styles.errorText}>{String(formik?.errors.address)}</Text>
            )}

            <Text style={styles.inputLabel} >City<Text style={{ color: "#EF4141" }} >*</Text></Text>
            <TextInput
              placeholder="Enter your city"
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'default'}
              style={styles.textInput}
              value={formik?.values.city}
              onBlur={formik?.handleBlur('city')}
              onChangeText={
                formik?.handleChange('city')
              }
              returnKeyType='done'
            />

            {formik?.errors.city && formik?.touched.city && (
              <Text style={styles.errorText}>{String(formik?.errors.city)}</Text>
            )}

            <Text style={styles.inputLabel} >State<Text style={{ color: "#EF4141" }} >*</Text></Text>
            <Dropdown
              style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
              placeholderStyle={styles.placeholderStyle}
              selectedTextStyle={styles.selectedTextStyle}
              inputSearchStyle={styles.inputSearchStyle}
              iconStyle={styles.iconStyle}
              data={stateList}
              itemTextStyle={styles.itemTextStyle}
              maxHeight={300}
              search
              searchPlaceholder='Search...'
              labelField={"name"}
              valueField="id"
              placeholder={'Select state'}
              // searchPlaceholder="Search..."
              value={StateId}
              onFocus={() => setIsFocus(true)}
              onBlur={() => { setIsFocus(false) }}
              onChange={(item: any) => {
                setStateId(item?.id);
                setIsFocus(false);
              }}
            />

            <Text style={styles.inputLabel} >Pincode<Text style={{ color: "#EF4141" }} >*</Text></Text>
            <TextInput
              placeholder="Enter your pinCode"
              placeholderTextColor={'#003F6F50'}
              secureTextEntry={false}
              keyboardType={'phone-pad'}
              maxLength={6}
              style={styles.textInput}
              value={formik?.values.pinCode}
              onBlur={formik?.handleBlur('pinCode')}
              onChangeText={text => {
                formik?.handleChange('pinCode')(text);
                if (text.length === 6) {
                  Keyboard.dismiss();
                }
              }}
              returnKeyType='done'
            />

            {formik?.errors.pinCode && formik?.touched.pinCode && (
              <Text style={styles.errorText}>{String(formik?.errors.pinCode)}</Text>
            )}

            <View style={styles.bottombuttonouterview}>
              <TouchableOpacity style={styles.Bottombutton}
                onPress={formik.handleSubmit}
              >
                <Text style={[styles.Bottombuttonlabels]}>Next</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.Bottombutton, { backgroundColor: '#003F6F1a' }]} onPress={() => navigation.goBack()}>
                <Text style={[styles.Bottombuttonlabels, { color: "#003F6F" }]}>Previous</Text>
              </TouchableOpacity>
            </View>
          </>
        </Formik>
      </ScrollView>

    </View>
  )
}

export default Step_3

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#ffffff',
    padding: 22,
    elevation: 2
  },
  headerview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30
  },
  heading: {
    fontSize: 18,
    fontFamily: 'Inter-Bold',
    color: "#000000"
  },
  bottombuttonouterview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: 30
  },
  Bottombuttonlabels: {
    color: "#ffffff",
    fontFamily: 'Inter-Bold',
    fontSize: 16
  },
  Bottombutton: {
    borderWidth: 1,
    borderColor: '#003F6F',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    width: width / 2.8,
    borderRadius: 8,
    backgroundColor: '#003F6F'
  },
  textInput: {
    color: '#003F6F',
    fontFamily: 'Poppins-Medium',
    paddingHorizontal: 13,
    fontSize: 16,
    borderWidth: 1,
    borderColor: "lightgrey",
    borderRadius: 8
  },
  inputLabel: {
    color: '#272727',
    fontSize: 14,
    fontFamily: "Poppins-Medium",
    marginVertical: 10
  },
  errorText: {
    fontSize: 12,
    color: 'red',
    fontFamily: 'Poppins-Regular',
    top: 5,
  },
  dropdown: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
    // marginTop:15
  },
  label: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 22,
    top: 8,
    zIndex: 999,
    paddingHorizontal: 8,
    fontSize: 14,
  },
  placeholderStyle: {
    fontSize: 15,
    color: '#003F6F50',
    fontFamily: 'Poppins-Regular'
  },
  selectedTextStyle: {
    fontSize: 15,
    color: '#003F6F',
    fontFamily: 'Poppins-Medium'
  },
  iconStyle: {
    width: 15,
    height: 8.3,
},
  inputSearchStyle: {
    height: 40,
    fontSize: 15,
    color: 'grey',
    paddingVertical: 5,
    fontFamily: 'Poppins-Regular'
  },
  itemTextStyle: {
    fontSize: 15,
    fontFamily: 'Poppins-Regular',
    color: 'grey'
  }
})