import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Alert,
    Keyboard,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native';
import PaymentStatusModal from '../../Modal/PaymentStatusModal'
import AllInOneSDKManager from 'paytm_allinone_react-native';
import Loader from '../../utils/functions/Loader';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import { useSelector, useDispatch } from 'react-redux';
import axios from '../../axiosConfig'
import icons from '../../Constant/icons';

const { height, width } = Dimensions.get('window');

const Step_5 = ({ navigation }: any) => {

    const dispatch = useDispatch()

    const g_BookingPropertyId = useSelector((state: any) => state?.BookingPropertyIdReducer?.BookingProprtyId);
    const g_BookingType = useSelector((state: any) => state?.BookingTypeReducer?.BookingType);
    const g_CurrentBookingData = useSelector((state: any) => state?.CurrentBookingDataReducer?.CurrentBookingData);
    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    const guestInfo = useSelector((state: any) => state?.GuestInfoDataReducer?.GuestInfoData);

    const [amount, setAmount] = useState(100);
    const [visiblePaymentStatus, setvisiblePaymentStatus] =
        useState<boolean>(false);
    const [PaymentStatus, setPaymentStatus] = useState<any>('');
    const [bookingData, setBookingData] = useState<any>('');
    const [loading, setLoading] = useState<boolean>(false);




    const startPayment = async () => {
        // console.log('>>>>>>......',bookingData)
        // return
        const data = {
            amount: bookingData?.bookingAmount,
        };
        console.log(data)
        // return
        try {
            const response = await axios.post(
                `properties/${g_BookingPropertyId}/bookings/${g_CurrentBookingData?.id}/payments/paytm`,
                data,
            );

            // console.log(response.data.data, '........../////')
            let orderId = response?.data?.data?.content?.body?.orderId
            let merId = response?.data?.data?.content?.body?.mid
            let txnt = response?.data?.data?.content?.payment_gateway_response?.txnToken
            let val = response?.data?.data?.content?.body?.value
            let callbackUrl = response?.data?.data?.content?.body?.callBackUrl
            let stag = true
            let appInkeReted = true


            await openPaytm(orderId, merId, txnt, val, callbackUrl, stag, appInkeReted)
        } catch (error: any) {
            console.log(error.response, '1111111111111111111111');
        }
    };

    const openPaytm = async (
        ordrId: any,
        merId: any,
        txnt: any,
        val: any,
        callbackUrl: any,
        stag: any,
        appInkeReted: any) => {
        AllInOneSDKManager.startTransaction(
            ordrId,
            merId,
            txnt,
            val,
            callbackUrl,
            stag,
            appInkeReted,
            ''
        )
            .then(async result => {
                console.log(result, 'then');
                await handleUpdateTransaction(ordrId)
                setPaymentStatus(result)
                setvisiblePaymentStatus(true)
                // console.warn(res2.data);
            })
            .catch(err => {
                console.log(err, '/////////???????????????')
            });
    }


    const getBookingDetailById = async () => {
        try {
            const response = await axios.get(`organizations/${userDetails?.organizationId}/properties/${g_BookingPropertyId}/bookings/${g_CurrentBookingData?.id}`,)
            setBookingData(response?.data?.data)
            // console.log(response?.data?.data?.bookingRoomTypeDetails, '?????')
        } catch (error: any) {
            console.log(error.response)
        }
    }

    const handleUpdateTransaction = async (orderId: any) => {
        const data = { orderId: orderId }

        try {
            const response = await axios.post(`properties/${g_BookingPropertyId}/bookings/${g_CurrentBookingData?.id}/payments/paytm/status`, data)
            console.log(response?.data?.data, '--------------------------___________')
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getBookingDetailById()
    }, [])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getBookingDetailById()
        });
        return unsubscribe;
    }, [navigation]);

// console.log(bookingData?.bookingRoomTypeDetails,'????????????????????',g_BookingType)


    return (
        <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>

            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Booking Summary</Text>
                    <Text></Text>
                </View>
            </View>
            {loading && <Loader />}
            <View style={{ padding: 22 }}>
                <View style={styles.securityDepositeView}>
                    <Text style={{ textAlign: 'center', color: '#000000', fontFamily: 'Inter-Medium' }}>Pay <Text style={{ color: "#EF4141" }}>₹</Text> {g_CurrentBookingData?.securityDeposit} Security Deposit in order to confirm your booking</Text>
                </View>

                {
                    bookingData?.bookingRoomTypeDetails?.map((item: any, index: any) => {
                        return (
                            <View key={index} style={styles.roomTypeOuterView}>
                                <Text style={styles.roomTypeName}>{item?.roomTypeName} <Text style={{ fontSize: 14, fontFamily: "Inter-Regular" }}>({item?.roomTypeDescription})</Text> </Text>
                                <View style={styles.roomTypeDetailView}>
                                    <Image source={icons?.date} style={{ height: 30, width: 30 }} />
                                    <Text style={{ color: "#808080", paddingHorizontal: 10, fontSize: 14, fontFamily: 'Inter-Medium' }}>{moment(bookingData?.checkInDate).format("DD MMM, YYYY")} - {moment(bookingData?.checkOutDate).format("DD MMM, YYYY")}</Text>

                                </View>
                                <View style={styles.roomTypeDetailView}>
                                    <Image source={icons?.bed} style={{ height: 30, width: 30 }} />
                                    <Text style={{ color: "#808080", paddingHorizontal: 10, fontSize: 14, fontFamily: 'Inter-Medium' }}>{item?.bedCount} Beds</Text>
                                </View>
                                <View style={styles.roomTypeDetailView}>
                                    <Image source={icons?.price} style={{ height: 30, width: 30 }} />
                                    {/* {g_BookingType=="DIRECT"?"per day":"per month"} */}
                                    <Text style={{ color: "#808080", paddingHorizontal: 10, fontSize: 14, fontFamily: 'Inter-SemiBold' }}>₹{item?.roomPrice} <Text style={{ fontFamily: 'Inter-Regular' }}>({item?.roomRate}/{item?.stayType=="LONG_STAY"?"per month":"per day"})</Text></Text>
                                </View>
                            </View>
                        )

                    })
                }

            </View>

            <View style={styles.bottombuttonouterview}>
                <TouchableOpacity style={styles.Bottombutton}
                    onPress={() => startPayment()}
                >
                    <Text style={[styles.Bottombuttonlabels]}>Pay</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.Bottombutton, { backgroundColor: '#003F6F1a' }]} onPress={() => navigation.goBack()}>
                    <Text style={[styles.Bottombuttonlabels, { color: "#003F6F" }]}>Previous</Text>
                </TouchableOpacity>
            </View>


            <PaymentStatusModal
                status={PaymentStatus}
                visible={visiblePaymentStatus}
                navigation={navigation}
                // orderId={orderId}
                dismiss_drawer={() => { setvisiblePaymentStatus(false), navigation.navigate('Bookings') }}
            />
        </View>
    )
}

export default Step_5

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-Bold',
        color: "#000000"
    },
    bottombuttonouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 30
    },
    Bottombuttonlabels: {
        color: "#ffffff",
        fontFamily: 'Inter-Bold',
        fontSize: 16
    },
    Bottombutton: {
        borderWidth: 1,
        borderColor: '#003F6F',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 2.8,
        borderRadius: 8,
        backgroundColor: '#003F6F'
    },
    securityDepositeView: {
        height: 50,
        backgroundColor: '#07bc0c1a',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: '#07bc0c',
        marginVertical: 5,
        borderRadius: 7,
        paddingHorizontal: 10,
        // alignItems: 'center',
        justifyContent: 'center',
    },
    roomTypeOuterView: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: 'lightgrey',
        marginVertical: 10,
        borderRadius: 7,
        padding: 10,
        // alignItems: 'center',
        // justifyContent: 'center',
        // flex: 1,
        elevation: 2
    },
    roomTypeName: {
        fontSize: 18,
        fontFamily: 'Inter-Bold',
        color: "#000000"
    },
    roomTypeDetailView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 5
    }
})