import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import icons from '../../Constant/icons'
import axios from '../../axiosConfig'
import { Dropdown } from 'react-native-element-dropdown';
import CustomCalendar from '../../components/CustomCalender';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import { useSelector, useDispatch } from 'react-redux';
import { setBookingStep1Action, setBookingStep1DatesAction } from '../../redux/actions/Step_1_bookingData-action';
import { setBookingPropertyIdAction } from '../../redux/actions/BookingPropertyId-action';
import { setGuestInfoDataAction } from '../../redux/actions/GuestInfo-action';
import Loader from '../../utils/functions/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker"
const { height, width } = Dimensions.get('window');

const Step_1 = ({ navigation }: any) => {

    const dispatch = useDispatch()

    const g_BookingType = useSelector((state: any) => state?.BookingTypeReducer?.BookingType);
    const g_BookingStep1Data = useSelector((state: any) => state?.BookingStep1Reducer?.BookingStep1);
    const g_BookingPropertyId = useSelector((state: any) => state?.BookingPropertyIdReducer?.BookingProprtyId);


    // console.log(g_BookingStep1Data)

    const [propertyList, setPropertyList] = useState([])
    const [value, setValue] = useState(null);
    const [isFocus, setIsFocus] = useState(true);
    const [showCalendar, setShowCalendar] = useState<boolean>(false)
    const [showValiToCalendar, setShowValiToCalendar] = useState<boolean>(false)
    const [startDate, setstartDate] = useState<any>('');
    const [endDate, setendDate] = useState<any>('');
    const [isBreakFast, setisBreakFast] = useState<any>(true);
    const [LengthOfStay, setLengthOfStay] = useState<string>("LONG_STAY");
    const [loading, setLoading] = useState<boolean>(false);

    const initialavailabilityListModal: any = {
        room_type_name: '',
        room_type_id: 0,
        beds: 0,
        room_type_description: '',
        roomRate: null
    }

    const [availabilityList, setAvailabilityList] = useState([{ ...initialavailabilityListModal }])
    const [showRoomTypes, setShowRoomTypes] = useState<boolean>(false);



    function randomId() {
        return Math.floor(Math.random());
    }

    const initialRoomTypeModal = {
        id: randomId(),
        roomTypeId: null,
        roomRateId: null,
        noOfBeds: 1,
        breakfast: isBreakFast,
        currentRate: 0
    }

    const [roomTypes, setRoomsTypes] = useState<any>([{ ...initialRoomTypeModal }]);

    // console.log(g_BookingStep1Data.length)
    // useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         if (g_BookingStep1Data.length < 1) {
    //             console.log('ddddddddddd')
    //             setValue(null)
    //             setAvailabilityList([{ ...initialavailabilityListModal }])
    //             setRoomsTypes([{ ...initialRoomTypeModal }])
    //         }
    //     });
    //     return unsubscribe;
    // }, [navigation]);

    useEffect(() => {
        if (g_BookingStep1Data.length < 1) {
            // console.log('fefefa')
            setstartDate('')
            setendDate("")
            setValue(null)
            setAvailabilityList([{ ...initialavailabilityListModal }])
            setRoomsTypes([{ ...initialRoomTypeModal }])
        }
    }, [g_BookingStep1Data?.length])




    const gelAllProperties = async () => {
        try {
            const response = await axios.get(`auth/guest/properties`)
            setPropertyList(response?.data?.data)
        } catch (error) {
            console.log(error)
        }
    }




    const getBookingAvailability = async () => {
        try {
            const initialDate = moment(startDate).format('DD-MM-YYYY')
            const lastDate = moment(endDate).format('DD-MM-YYYY')
            const response = await axios.get(`properties/${value}/availability/booking-invite?startDate=${initialDate}&endDate=${lastDate}`)
            const newarray: any = []
            setAvailabilityList(response?.data?.data)
            response?.data?.data.map((item: any) => (
                newarray.push({
                    room_type_name: item.room_type_name,
                    room_type_id: item.room_type_id,
                    beds: item.beds,
                    room_type_description: item.room_type_description,
                    isRemoved: false
                })
            ))
            response?.data?.data ? setAvailabilityList(newarray) : setAvailabilityList([]);
            setShowRoomTypes(true)
        } catch (error: any) {
            console.log(error.response)
        }
    }

    const getRoomRateBasedOnStayType = async (objCurrentRoomType: any, index?: any) => {
        // console.log(objCurrentRoomType,'.......??????????')
        const objBody = {
            stayType: LengthOfStay,
            isBreakfast: objCurrentRoomType?.breakfast,
            startDate: moment(startDate).format("DD-MM-YYYY"),
            roomTypeId: objCurrentRoomType?.roomTypeId,
        }

        if (objBody.startDate == "Invalid date") {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  Please select startDate and endDate`,
                visibilityTime: 2500,
                autoHide: true,
            });
            // You can handle this validation error accordingly
            return;
        }
        else if (objBody.roomTypeId == null) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  Please select room type`,
                visibilityTime: 2500,
                autoHide: true,
            });
            // You can handle this validation error accordingly
            return; // Exit the function or handle the error as needed
        }


        // console.log(objBody, '............')
        try {
            const response = await axios.post(`properties/${value}/room-rates/filter`, objBody)
            const data = response?.data?.data;
            objCurrentRoomType.roomRateId = data?.id;
            const arrRoomTypesavailabilityList = [...availabilityList];


            let updatedAvailabilityList: any = arrRoomTypesavailabilityList.map((item: any, i) =>
                i == index ? { ...item, roomRate: data?.price } : item
            );
            // console.log(updatedAvailabilityList, '555555555555555555')
            setAvailabilityList(updatedAvailabilityList);
            objCurrentRoomType.currentRate = data?.price;

            return objCurrentRoomType;
        } catch (error: any) {
            objCurrentRoomType.currentRate = 0;
            objCurrentRoomType.roomRateId = null;
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
            console.log(error?.response?.data?.errors[0])
        }
    }

    // const handleStayType = async (val: string, index: number) => {
    //     setLengthOfStay(val);
    //     // if (ls_bookingType === enumBookingTypes?.DIRECT) {
    //     const arrRoomTypes = [...roomTypes];

    //     let updatedRoomTypes = await getRoomRateBasedOnStayType(arrRoomTypes[index], val, index);

    //     arrRoomTypes[index] = updatedRoomTypes ? updatedRoomTypes : arrRoomTypes[index];

    //     setRoomsTypes(arrRoomTypes);
    //     // }
    // }

    const handleBreakFast = async (e: any, index: number) => {
        // console.log(e,index)
        setisBreakFast(e)
        const arrRoomTypes = [...roomTypes];

        let objCurrentRoomType = arrRoomTypes[index];
        objCurrentRoomType.breakfast = e;

        let updatedRoomTypes = await getRoomRateBasedOnStayType(objCurrentRoomType, index);
        arrRoomTypes[index] = updatedRoomTypes ? updatedRoomTypes : objCurrentRoomType;
        setRoomsTypes(arrRoomTypes);
    }

    const handleRoomType = async (selectedRoomTypeID: number, index: number) => {
        // console.log(selectedRoomTypeID, index, '//////////////////')
        const arrRoomTypes = [...roomTypes];
        let objCurrentRoomType = arrRoomTypes[index];

        objCurrentRoomType.roomTypeId = selectedRoomTypeID;

        objCurrentRoomType.noOfBeds = 1;

        let updatedRoomTypes = await getRoomRateBasedOnStayType(objCurrentRoomType, index);
        arrRoomTypes[index] = updatedRoomTypes ? updatedRoomTypes : objCurrentRoomType;
        setRoomsTypes(arrRoomTypes);
    }


    const handleAddRoomType = () => {
        const arrRoomTypes = [...roomTypes];
        let id = arrRoomTypes?.length + 1;

        arrRoomTypes.forEach((item: any) => {
            if (item?.id === id) {
                id = randomId();
            }
        })

        setRoomsTypes((p: any) => ([
            ...p,
            { ...initialRoomTypeModal, ["id"]: id }
        ]))
    }

    const deleteRoomTypes = (index: number) => {
        let arrRoomTypes: any = [...roomTypes];

        arrRoomTypes.splice(index, 1);

        setRoomsTypes(arrRoomTypes);
    }


    const handleBeds = async (index: any, isDecereased: boolean) => {
        const arrRoomTypes = [...roomTypes];
        let objCurrentRoomType = arrRoomTypes[index];

        if (objCurrentRoomType?.noOfBeds) {
            if (isDecereased) {
                objCurrentRoomType.noOfBeds = --objCurrentRoomType.noOfBeds;
            } else {
                let currentBedsWRTRoomType = getSelectedRoomType(objCurrentRoomType);

                if (objCurrentRoomType?.noOfBeds < currentBedsWRTRoomType[0]?.beds) {
                    objCurrentRoomType.noOfBeds = ++objCurrentRoomType.noOfBeds;
                }
            }
        }

        let updatedRoomTypes = await getRoomRateBasedOnStayType(objCurrentRoomType);
        arrRoomTypes[index] = updatedRoomTypes ? updatedRoomTypes : objCurrentRoomType;
        arrRoomTypes[index].currentRate = arrRoomTypes[index]?.currentRate * arrRoomTypes[index]?.noOfBeds;
        setRoomsTypes(arrRoomTypes);
    }


    function getSelectedRoomType(objCurrentRoomType: any) {
        if (availabilityList && availabilityList?.length) return availabilityList?.filter((item) => item?.room_type_id === objCurrentRoomType?.roomTypeId)
        else return [];
    }


    const handleSubmitForm = async () => {
        await getGuestInfo()
        // navigation.navigate("Step_2")
        // return
        try {

            if (value == null) {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Please select Property`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                return
            }

            if (startDate == "" || endDate == "") {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Please select dates.`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                return
            }

            let isRoomTypeAndRateSelected = roomTypes.some((obj: any) => !obj.roomTypeId);
            if (isRoomTypeAndRateSelected) {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Please select room type.`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                return
            }

            isRoomTypeAndRateSelected = roomTypes.some((obj: any) => !obj.roomRateId);
            if (isRoomTypeAndRateSelected) {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Room rate not found.`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                return
            }
            setLoading(true)

            const firstDate = moment(startDate).format('YYYY-MM-DD')
            const lastDate = moment(endDate).format('YYYY-MM-DD')
            // console.log(startDate,endDate)
            const response = await axios.get(`properties/${value}/check-booking?bookingType=${g_BookingType}&startDate=${firstDate}&endDate=${lastDate}`)
            // console.log(response?.data?.data)
            // return
            if (!response?.data?.data) {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Booking already exist with this duration`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                setLoading(false)
                return
            }

            const bookingdates = {
                startDate: startDate,
                endDate: endDate
            }

            dispatch(setBookingStep1Action(roomTypes))
            dispatch(setBookingStep1DatesAction(bookingdates))
            dispatch(setBookingPropertyIdAction(value))
            navigation.navigate("Step_2")
            setLoading(false)
        } catch (error: any) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `  ${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
            console.log(error.response)
            setLoading(false)
        }
    }

    const getGuestInfo = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`auth/guest/info`)
            dispatch(setGuestInfoDataAction(response?.data?.data))
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    useEffect(() => {
        gelAllProperties()
    }, [])



    useEffect(() => {
        if (value && startDate != '' && endDate != '') {
            getBookingAvailability()
        }
    }, [value, startDate, endDate])


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            gelAllProperties()
        });
        return unsubscribe;
    }, [navigation]);


    // console.log(roomTypes, '444545454545885')

    return (
        <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Booking Details</Text>
                    <Text></Text>
                </View>

            </View>
            {loading && <Loader />}
            <ScrollView contentContainerStyle={{ paddingHorizontal: 22, marginTop: 25, paddingBottom: height / 10 }}>

                <Text style={styles.Inputlabels}>Select Property<Text style={{ color: "#ED285A" }}>*</Text></Text>
                <Dropdown
                    style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={propertyList}
                    itemTextStyle={styles.itemTextStyle}
                    maxHeight={300}
                    labelField={"propertyName"}
                    valueField="id"
                    placeholder={'Select Property'}
                    // searchPlaceholder="Search..."
                    value={value}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => { setIsFocus(false) }}
                    onChange={(item: any) => {
                        setValue(item?.id);
                        dispatch(setBookingPropertyIdAction(item?.id))
                        setIsFocus(false);
                    }}
                    renderRightIcon={() =>
                        <TouchableOpacity style={{ height: 25, width: 25, alignItems: 'center', justifyContent: 'center' }} onPress={() => setValue(null)} >
                            {value ?
                                <Image source={icons.cross} style={{ height: 12, width: 12, tintColor: 'grey' }} />
                                :
                                <Image source={icons.downarrow} style={styles.iconStyle} />
                            }
                        </TouchableOpacity>
                    }
                />

                <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ alignItems: 'flex-start' }}>
                        <Text style={styles.Inputlabels}>Start Date<Text style={{ color: "#ED285A" }}>*</Text></Text>
                        <TouchableOpacity style={styles.datepickerbutton} onPress={() => setShowCalendar(true)}>
                            <Text style={styles.datelabel}>{startDate ? moment(startDate).format('DD-MM-YYYY') : 'Select start date  🗓️'}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems: 'flex-start' }}>
                        <Text style={styles.Inputlabels}>End Date<Text style={{ color: "#ED285A" }}>*</Text></Text>
                        <TouchableOpacity style={styles.datepickerbutton} onPress={() => setShowValiToCalendar(true)}>
                            <Text style={styles.datelabel}>{endDate ? moment(endDate).format('DD-MM-YYYY') : 'Select end date  🗓️'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <Text style={styles.Inputlabels}>Length of Stay<Text style={{ color: "red" }}>*</Text></Text>
                <View style={styles.breakFastOuterView}>
                    <TouchableOpacity style={[styles.radiobutton,]} onPress={() => setLengthOfStay("LONG_STAY")}>
                        <View style={[styles.radiobuttonouterview, { borderWidth: LengthOfStay == "LONG_STAY" ? 6 : 1, borderColor: LengthOfStay == "LONG_STAY" ? "#003F6F" : "grey" }]} />
                        <Text style={[styles.radiobuttonlabel, { color: LengthOfStay == "LONG_STAY" ? "grey" : "lightgrey" }]}>Long Stay</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.radiobutton, {}]} onPress={() => setLengthOfStay("SHORT_STAY")}>
                        <View style={[styles.radiobuttonouterview, { borderWidth: LengthOfStay == "SHORT_STAY" ? 6 : 1, borderColor: LengthOfStay == "SHORT_STAY" ? "#003F6F" : "grey" }]} />

                        <Text style={[styles.radiobuttonlabel, { color: LengthOfStay == "SHORT_STAY" ? "grey" : "lightgrey" }]}>Short Stay</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                    <Text style={styles.Inputlabels}>Select RoomType<Text style={{ color: "#ED285A" }}>*</Text></Text>
                    {
                        g_BookingType == "CORPORATE" ?
                            <TouchableOpacity style={styles.addroomtypebutton} onPress={() => handleAddRoomType()}>
                                <Text style={[styles.addroomlabels]}>+ Add room type</Text>
                            </TouchableOpacity>
                            : null
                    }
                </View>
                {
                    roomTypes?.map((item: any, localIndex: any) => (

                        <View style={{ marginTop: 5, borderColor: "lightgrey", borderWidth: 1, padding: 10, borderRadius: 5, flex: 1 }}>
                            <View style={{}}>
                                <Dropdown
                                    style={[styles.dropdown, isFocus && { borderColor: 'grey' }]}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={styles.selectedTextStyle}
                                    inputSearchStyle={styles.inputSearchStyle}
                                    iconStyle={styles.iconStyle}
                                    data={availabilityList}
                                    itemTextStyle={styles.itemTextStyle}
                                    maxHeight={300}
                                    labelField={"room_type_name"}
                                    valueField="room_type_id"
                                    placeholder={'Select Room Type'}
                                    value={item?.roomTypeId}
                                    onFocus={() => setIsFocus(true)}
                                    onBlur={() => { setIsFocus(false) }}
                                    onChange={(item: any) => {
                                        // setValue(item?.id);
                                        // setIsFocus(false);
                                        handleRoomType(item?.room_type_id, localIndex)
                                    }}
                                />

                            </View>
                            <View style={{ alignItems: 'flex-end' }}>
                                {
                                    item?.currentRate > 0 ?
                                        <Text style={[styles.roomRateText, { color: "red" }]}>₹{item?.currentRate}</Text> : null
                                }
                            </View>
                            <View style={[styles.breakFastOuterView, { marginTop: 15 }]}>
                                <Text style={styles.Inputlabels}>Breakfast Included<Text style={{ color: "red" }}>*</Text></Text>
                                <TouchableOpacity style={[styles.radiobutton,]} onPress={() => handleBreakFast(true, localIndex)}>
                                    <View style={[styles.radiobuttonouterview, { borderWidth: item?.breakfast == true ? 6 : 1, borderColor: item?.breakfast == true ? "#003F6F" : "grey" }]} />
                                    <Text style={[styles.radiobuttonlabel, { color: item?.breakfast == true ? "grey" : "lightgrey" }]}>YES</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.radiobutton, {}]} onPress={() => handleBreakFast(false, localIndex)}>
                                    <View style={[styles.radiobuttonouterview, { borderWidth: item?.breakfast == false ? 6 : 1, borderColor: item?.breakfast == false ? "#003F6F" : "grey" }]} />

                                    <Text style={[styles.radiobuttonlabel, { color: item?.breakfast == false ? "grey" : "lightgrey" }]}>NO</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                {
                                    g_BookingType == "CORPORATE" ?
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity style={{}} disabled={item?.noOfBeds === 1} onPress={() => handleBeds(localIndex, true)}>
                                                <Image source={icons.minus} style={{ height: 25, width: 25 }} />
                                            </TouchableOpacity>

                                            <Text style={{ color: 'green', fontFamily: 'Inter-Bold', fontSize: 16, paddingHorizontal: 10 }}>{item?.noOfBeds}</Text>

                                            <TouchableOpacity style={{}} onPress={() => handleBeds(localIndex, false)}>
                                                <Image source={icons.addbed} style={{ height: 25, width: 25 }} />
                                            </TouchableOpacity>
                                        </View>
                                        : null
                                }

                                {
                                    g_BookingType == "CORPORATE" && roomTypes?.length > 1 ?
                                        <View style={{}}>
                                            <TouchableOpacity style={{}} onPress={() => deleteRoomTypes(localIndex)}>
                                                <Image source={icons.delet} style={{ height: 30, width: 30 }} />
                                            </TouchableOpacity>
                                        </View>
                                        : null
                                }
                            </View>



                        </View>
                        // </TouchableOpacity>
                    ))
                }


                <View style={styles.bottombuttonouterview}>
                    <TouchableOpacity style={styles.Bottombutton} onPress={() => handleSubmitForm()}>
                        <Text style={[styles.Bottombuttonlabels]}>Next</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.Bottombutton, { backgroundColor: '#003F6F1a' }]} onPress={() => navigation.goBack()}>
                        <Text style={[styles.Bottombuttonlabels, { color: "#003F6F" }]}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>


            <DateTimePickerModal
                isVisible={showCalendar}
                mode="date"
                isDarkModeEnabled={false}
                onConfirm={(date: any) => {
                    console.log('A date has been picked: ', date)
                    // moment(date).format('yyyy-MM-dd')
                    setstartDate(date);
                    setShowCalendar(false)
                }}
                onCancel={() => {
                    setShowCalendar(false)
                }}
            minimumDate={new Date()}
            />

            <DateTimePickerModal
                isVisible={showValiToCalendar}
                mode="date"
                isDarkModeEnabled={false}
                onConfirm={(date: any) => {
                    console.log('A date has been picked: ', date)
                    setendDate(date);
                    setShowValiToCalendar(false)
                }}
                onCancel={() => {
                    setShowValiToCalendar(false)
                }}
            minimumDate={new Date()}
            />


            {/* {showValiToCalendar && (
    <CustomCalendar
        isVisible={showValiToCalendar}
        onClose={() => setShowValiToCalendar(false)}
        onDayPress={(day: any) => {
            if (day) {
                setShowValiToCalendar(false);
                setendDate(day?.dateString);
            }
            setShowValiToCalendar(false);
        }}
        minDate={startDate}
        currentDate={endDate}
    />
)} */}
            {/* {showCalendar && (
                <CustomCalendar
                    isVisible={showCalendar}
                    onClose={() => setShowCalendar(false)}
                    onDayPress={(day: any) => {
                        if (day) {
                            setShowCalendar(false);
                            setstartDate(day?.dateString);
                        }
                        setShowCalendar(false);
                    }}
                    // minDate={startDate}
                    currentDate={startDate}
                // maxDate={moment('').format('DD-MM-YYYY')}
                />
            )} */}

        </View>
    )
}

export default Step_1

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-Bold',
        color: "#000000"
    },
    Inputlabels: {
        color: '#272727',
        fontFamily: 'Inter-Medium',
        fontSize: 14,
        marginVertical: 10,
        // textAlign:'left'
    },
    dropdown: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 6,
        paddingHorizontal: 10,
        // marginTop:15
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 15,
        color: 'grey',
        fontFamily: 'Poppins-Regular'
    },
    selectedTextStyle: {
        fontSize: 15,
        color: 'grey',
        fontFamily: 'Poppins-Regular'
    },
    iconStyle: {
        width: 15,
        height: 8.3,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 15,
        color: 'grey',
        paddingVertical: 5,
        fontFamily: 'Poppins-Regular'
    },
    itemTextStyle: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'grey'
    },
    datepickerbutton: {
        height: 40,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 6,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    datelabel: {
        color: "#000000",
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
    },
    breakFastOuterView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // marginTop: 15
        // width: width / 1.9
    },
    radiobutton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 5
    },
    radiobuttonouterview: {
        borderWidth: 6,
        height: 25,
        width: 25,
        borderRadius: 20,
        borderColor: '#003F6F'
    },
    radiobuttonlabel: {
        fontSize: 15,
        fontFamily: 'Poppins-Medium',
        color: 'grey',
        paddingHorizontal: 7
    },
    roomTypesListView: {
        borderWidth: 1,
        backgroundColor: '#ffffff',
        elevation: 2,
        borderColor: "lightgrey",
        borderRadius: 6,
        paddingVertical: 7,
        marginTop: 5,
        paddingHorizontal: 10,

    },
    roomTypeName: {
        color: '#000000',
        fontFamily: "Inter-Medium",
        fontSize: 16
    },
    roomRateText: {
        color: '#000000',
        fontFamily: "Inter-Medium",
        fontSize: 12
    },
    addroomlabels: {
        color: "#55AC1C",
        fontFamily: 'Inter-Medium',
        fontSize: 14,
    },
    addroomtypebutton: {
        height: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    Bottombutton: {
        borderWidth: 1,
        borderColor: '#003F6F',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 2.8,
        borderRadius: 8,
        backgroundColor: '#003F6F'
    },
    bottombuttonouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 30
    },
    Bottombuttonlabels: {
        color: "#ffffff",
        fontFamily: 'Inter-Bold',
        fontSize: 16
    }
})