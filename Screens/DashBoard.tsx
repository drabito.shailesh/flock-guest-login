import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, FlatList, LayoutChangeEvent, Dimensions } from 'react-native'
import icons from '../Constant/icons'
import axios from '../axiosConfig'
import DueAmountPaymentModal from '../Modal/DueAmountPaymentModal'
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
    FadeIn,
    Layout
} from "react-native-reanimated";
import RoomTypeDetailsList from '../components/RoomTypeDetailsList';
import moment from 'moment'
import { useDispatch } from 'react-redux'
import { setBookingIdAction } from '../redux/actions/BookingId-action'
const { height, width } = Dimensions.get('window')



const DashBoard = ({ navigation }: any) => {

    const dispatch = useDispatch()

    const [DueAmountDetails, setDueAmountDetails] = useState<any>('')
    const [showDueAmountPaymentModal, setShowDueAmountPaymentModal] = useState(false);
    const [BookingsDetails, setBookingsDetails] = useState<any>([])
    const [PreviousBookings, setPreviousBookings] = useState<any>('');

    const getDuesAmountDetails = async () => {
        try {
            const response = await axios.get(`/guests/bookings/guest-due-payment`)
            setDueAmountDetails(response?.data?.data)
            // console.log(response.data.data, '/././ds./d./ed.f/e')
        } catch (error) {
            console.log(error)
        }
    }

    const getCurrentDetails = async () => {
        try {
            const response = await axios.get(`guests/bookings/details`)
            setBookingsDetails(response?.data?.data?.content)
            // console.log(response.data.data, '/././ds./d./ed.f/e')
        } catch (error) {
            console.log(error)
        }
    }

    const getPreviousDetails = async () => {
        try {
            const response = await axios.get(`guests/bookings/details?isPreviousBooking=${true}`)
            setPreviousBookings(response?.data?.data?.content[0])
            // console.log(response?.data?.data?.content[0], '/././ds./d./ed.f/e')
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getDuesAmountDetails();
        getCurrentDetails();
        getPreviousDetails();
    }, [])

    const SectionButtons = ({ label, description, onPress }: any) => {
        return (
            <TouchableOpacity style={styles.sectionButtons} onPress={onPress}>
                <View>
                    <Text style={{ fontFamily: 'Poppins-Medium', color: '#003F6F', fontSize: 14 }}>{label}</Text>
                    <Text style={{ fontFamily: 'Poppins-Regular', color: 'grey', fontSize: 13 }}>{description}</Text>
                </View>
                <Image source={icons.rightArrow} style={{ height: 12, width: 8 }} />
            </TouchableOpacity>
        )
    }

    const renderBeds = (item: any) => {
        return <RoomTypeDetailsList item={item} navigation={navigation} />
    }

    const renderItem = (item: any) => {
        return (
            <View style={styles.DueAmountOuterView}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={styles.roomTypestext}>Booking No: <Text style={{ color: '#003F6F' }}>#{item?.item?.bookingNo}</Text></Text>
                    <View
                        style={{
                            backgroundColor: item?.item?.bookingStatus == 1 ? '#666CFF1A'
                                : item?.item?.bookingStatus == 2 ? '#FF66C21A' :
                                    item?.item?.bookingStatus == 3 ? '#22ACD81A' :
                                        item?.item?.bookingStatus == 5 ? '#8080801A' :
                                            item?.item?.bookingStatus == 6 ? '#EF41411A' :
                                                '#55AC1C1A', borderRadius: 5, justifyContent: 'center'
                        }}>
                        <Text style={{
                            color: item?.item?.bookingStatus == 1 ? '#666CFF'
                                : item?.item?.bookingStatus == 2 ? '#F27D9B' :
                                    item?.item?.bookingStatus == 3 ? '#22ACD8' :
                                        item?.item?.bookingStatus == 5 ? '#808080' :
                                            item?.item?.bookingStatus == 6 ? '#EF4141' :
                                                '#55AC1C', fontSize: 12, fontFamily: 'Poppins-Medium', paddingHorizontal: 8, textAlign: 'center',
                        }}>
                            {item?.item?.bookingStatus == 1 ? 'Draft'
                                : item?.item?.bookingStatus == 2 ? 'Booked' :
                                    item?.item?.bookingStatus == 3 ? 'Confirmed' :
                                        item?.item?.bookingStatus == 5 ? 'Cancelled' :
                                            item?.item?.bookingStatus == 6 ? 'Closed' :
                                                'In-House'}
                        </Text>
                    </View>
                </View>
                <Text style={styles.roomTypestext}>Address: <Text style={{ color: '#003F6F' }}>{item?.item?.propertyAddress}</Text></Text>
                <Text style={styles.roomTypestext}>Booking Type: <Text style={{ color: '#003F6F' }}>{item?.item?.bookingType}</Text></Text>
                <Text style={styles.roomTypestext}>Due Amount: <Text style={{ color: '#EF4141' }}>{'\u20B9'}{item?.item?.balanceDue}</Text></Text>
                <Text style={styles.roomTypestext}>RoomType:</Text>
                <FlatList
                    data={item?.item?.bedsData}
                    nestedScrollEnabled={true}
                    renderItem={renderBeds}
                />
                <View style={{ height: 1, backgroundColor: '#000000', marginVertical: 10 }} />

                <View>
                    <SectionButtons
                        label={'Payments'}
                        description={'Your payment details of rent,food,items etc.'}
                    />
                    <View style={{ height: 1, backgroundColor: '#000000', marginVertical: 10 }} />
                    <SectionButtons
                        label={'Invoices'}
                        description={'Your invoices for rent,food,items etc.'}
                        onPress={() => { navigation.navigate('InvoiceDetail'), dispatch(setBookingIdAction(item?.item)) }}
                    />
                    <View style={{ height: 1, backgroundColor: '#000000', marginVertical: 10 }} />
                    <SectionButtons
                        label={'House Documents'}
                        description={'Rental Aggrments, Membership Aggrements etc'}
                        onPress={() => { navigation.navigate('Documents'), dispatch(setBookingIdAction(item?.item)) }}
                    />
                </View>
            </View>
        )

    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                            source={icons.drawer}
                            style={{ height: 22, width: 22 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>DashBoard</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
                        <Image
                            source={icons.notification}
                            style={{ height: 22, width: 22, tintColor: '#ffffff' }}
                        />
                    </TouchableOpacity>
                </View>
            </View>

            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 22, marginTop: 20, paddingBottom: height / 7 }} nestedScrollEnabled={true}>
                {DueAmountDetails?.totalDueAmount > 0 ?
                    <View style={styles.DueAmountOuterView}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>Balance Amount</Text>
                            <Text style={{ color: '#EF4141', fontSize: 14, fontFamily: 'Poppins-Medium' }}>{'\u20B9'} {DueAmountDetails?.totalDueAmount}</Text>

                        </View>
                        <TouchableOpacity style={styles.payButton} onPress={() => setShowDueAmountPaymentModal(true)}>
                            <Text style={{ color: '#ffffff', fontSize: 18, fontFamily: 'Poppins-SemiBold' }}>Pay</Text>
                        </TouchableOpacity>
                        {/* <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 15 }} />
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <TouchableOpacity style={styles.rowButtons}>
                            <Text style={{ color: '#003F6F', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>Past Payments</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.rowButtons}>
                            <Text style={{ color: '#003F6F', fontSize: 14, fontFamily: 'Poppins-SemiBold', textAlign: 'right' }}>Invoices</Text>
                        </TouchableOpacity>
                    </View> */}
                    </View> : null}

                <View>
                    <Text style={[styles.heading, { color: '#003F6F', marginTop: 20 }]}>Current Bookings</Text>
                    <FlatList
                        nestedScrollEnabled={true}
                        data={BookingsDetails}
                        keyExtractor={(item: any, index: number) => item?.bookingId || index.toString()}
                        renderItem={renderItem}
                    />
                    <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <Text style={[styles.heading, { color: '#003F6F' }]}>Previous Bookings</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Bookings')}>
                            <Text style={[styles.heading, { color: '#003F6F', fontSize: 15 }]}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.DueAmountOuterView}>
                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.roomTypestext}>
                                Booking No: <Text style={{ color: '#003F6F' }}>#{PreviousBookings?.bookingNo}</Text>
                            </Text>
                            <View
                                style={{
                                    backgroundColor: PreviousBookings?.bookingStatus == 1 ? '#666CFF1A'
                                        : PreviousBookings?.bookingStatus == 2 ? '#FF66C21A' :
                                            PreviousBookings?.bookingStatus == 3 ? '#22ACD81A' :
                                                PreviousBookings?.bookingStatus == 5 ? '#8080801A' :
                                                    PreviousBookings?.bookingStatus == 6 ? '#EF41411A' :
                                                        '#55AC1C1A', borderRadius: 5, justifyContent: 'center'
                                }}>
                                <Text style={{
                                    color: PreviousBookings?.bookingStatus == 1 ? '#666CFF'
                                        : PreviousBookings?.bookingStatus == 2 ? '#F27D9B' :
                                            PreviousBookings?.bookingStatus == 3 ? '#22ACD8' :
                                                PreviousBookings?.bookingStatus == 5 ? '#808080' :
                                                    PreviousBookings?.bookingStatus == 6 ? '#EF4141' :
                                                        '#55AC1C', fontSize: 12, fontFamily: 'Poppins-Medium', paddingHorizontal: 8, textAlign: 'center',
                                }}>
                                    {PreviousBookings?.bookingStatus == 1 ? 'Draft'
                                        : PreviousBookings?.bookingStatus == 2 ? 'Booked' :
                                            PreviousBookings?.bookingStatus == 3 ? 'Confirmed' :
                                                PreviousBookings?.bookingStatus == 5 ? 'Cancelled' :
                                                    PreviousBookings?.bookingStatus == 6 ? 'Closed' :
                                                        'In-House'}
                                </Text>
                            </View>
                        </View>
                        <Text style={styles.roomTypestext}>
                            Address: <Text style={{ color: '#003F6F' }}>{PreviousBookings?.propertyAddress}</Text>
                        </Text>
                        <Text style={styles.roomTypestext}>
                            Booking Type: <Text style={{ color: '#003F6F' }}>{PreviousBookings?.bookingType}</Text>
                        </Text>
                        <Text style={styles.roomTypestext}>
                            Due Amount: <Text style={{ color: '#EF4141' }}>{'\u20B9'}{PreviousBookings?.balanceDue}</Text>
                        </Text>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                <Text style={styles.dateText}>From {moment(PreviousBookings?.startDate).format('MMM DD,YYYY')}</Text>
                                <Text style={styles.dateText}> To {moment(PreviousBookings?.endDate).format('MMM DD,YYYY')}</Text>
                            </View>

                        </View>
                    </View>
                </View>

            </ScrollView>
            {showDueAmountPaymentModal &&
                <DueAmountPaymentModal
                    isVisible={showDueAmountPaymentModal}
                    onClose={() => setShowDueAmountPaymentModal(false)}
                    DueAmountBookingList={DueAmountDetails?.dueAmountByBooking}
                />
            }
        </View>
    )
}
export default DashBoard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff"
    },
    header: {
        backgroundColor: '#003F6F',
        padding: 22,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#ffffff"
    },
    DueAmountOuterView: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 8,
        paddingHorizontal: 15,
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 1,
        paddingVertical: 10
    },
    payButton: {
        backgroundColor: '#003F6F',
        height: 35,
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    rowButtons: {
        justifyContent: 'center',
        height: 40,
        width: 120,
    },
    roomTypestext: {
        color: '#000000',
        fontSize: 15,
        fontFamily: 'Poppins-SemiBold',
        marginTop: 5
    },
    sectionButtons: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dateText: {
        color: '#EF4141',
        fontFamily: 'Poppins-Medium'
    }


})

