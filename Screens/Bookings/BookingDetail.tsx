import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, ScrollView } from 'react-native'
import icons from '../../Constant/icons'
import { useSelector } from 'react-redux';
import axios from '../../axiosConfig'
import moment from 'moment'
import Loader from '../../utils/functions/Loader';
import AllInOneSDKManager from 'paytm_allinone_react-native';
import PaymentStatusModal from '../../Modal/PaymentStatusModal';

const { height, width } = Dimensions.get('window');

const BookingDetail = ({ navigation }: any) => {
    const bookingDetail = useSelector((state: any) => state?.BookingIdReducer?.BookingId);
    // console.log(bookingDetail, '././..')
    const [BookingDetail, setBookingDetail] = useState<any>("")
    const [stateName, setStateName] = useState<any>("")
    const [loading, setLoading] = useState(false);
    const [visiblePaymentStatus, setvisiblePaymentStatus] =
        useState<boolean>(false);
    const [PaymentStatus, setPaymentStatus] = useState<any>('');

    const GetBookingDetailbyId = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`guests/bookings/${bookingDetail?.bookingId}`);
            await getStateName(response?.data?.stateId)
            setBookingDetail(response?.data)
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    const getStateName = async (stateId?: any) => {
        try {
            setLoading(true)
            const response = await axios.get("states")
            setLoading(false)
            const state = response?.data?.data?.find((item: any) => item.id === stateId);
            if (state) {
                setStateName(state?.name);
                return state.name;
            } else {
                // console.log('State not found');
                return null; // or some default value
            }
            console.log(response?.data?.data,)
            for (let i = 1; i < response?.data?.data?.length; i++) {
                if (stateId == response?.data?.data[i]?.id) {
                    console.log(response?.data?.data[i]?.name, '>>>>>>>>>>>>>>>>')
                }
            }
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }


    // /api/properties/{propertyId}/bookings/{bookingId}/accommodation


    const getBookingAccomodation = async () => {
        try {
            const response = await axios.get(`properties/${bookingDetail?.propertyId}/bookings/${bookingDetail?.bookingId}/accommodation`)
            // console.log(response.data.data , '333333333333333333333333')
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        // GetBookingDetailbyId()
        getBookingAccomodation()
    }, [bookingDetail?.bookingId])


    const startPayment = async () => {
        const data = {
            amount: bookingDetail?.balanceDue,
        };
        try {
            const response = await axios.post(
                `properties/${bookingDetail?.propertyId}/bookings/${bookingDetail?.bookingId}/payments/paytm`,
                data,
            );

            // console.log(response.data.data, '........../////')
            let orderId = response?.data?.data?.content?.body?.orderId
            let merId = response?.data?.data?.content?.body?.mid
            let txnt = response?.data?.data?.content?.payment_gateway_response?.txnToken
            let val = response?.data?.data?.content?.body?.value
            let callbackUrl = response?.data?.data?.content?.body?.callBackUrl
            let stag = true
            let appInkeReted = true


            await openPaytm(orderId, merId, txnt, val, callbackUrl, stag, appInkeReted)
        } catch (error: any) {
            console.log(error.response, '1111111111111111111111');
        }
    };

    const openPaytm = async (
        ordrId: any,
        merId: any,
        txnt: any,
        val: any,
        callbackUrl: any,
        stag: any,
        appInkeReted: any) => {
        AllInOneSDKManager.startTransaction(
            ordrId,
            merId,
            txnt,
            val,
            callbackUrl,
            stag,
            appInkeReted,
            ''
        )
            .then(async result => {
                console.log(result, 'then');
                await handleUpdateTransaction(ordrId)
                setPaymentStatus(result)
                setvisiblePaymentStatus(true)
            })
            .catch(err => {
                console.log(err, '/////////???????????????')
            });
    }

    const handleUpdateTransaction = async (orderId: any) => {
        const data = { orderId: orderId }
        try {
            const response = await axios.post(`properties/${bookingDetail?.propertyId}/bookings/${bookingDetail?.bookingId}/payments/paytm/status`, data)
            //   console.log(response?.data?.data, '--------------------------___________')
        } catch (error) {
            console.log(error)
        }
    }



    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            GetBookingDetailbyId()
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headingouterview}>
                    <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
                        <Image source={icons.back} style={{ height: 21.19, width: 10.55, right: 2 }} />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Booking Detail</Text>
                    <Text style={styles.heading}></Text>
                </View>
            </View>
            {loading && <Loader />}
            <ScrollView contentContainerStyle={{ paddingBottom: height / 10 - 30, marginTop: 22 }}>
                <View style={{ backgroundColor: "#ffffff", padding: 20, elevation: 2 }}>
                    <View style={styles.checkinOuterView}>

                        <View>
                            <Text style={styles.checkInLabel} >Check in :</Text>
                            <Text style={styles.checkInValue}>{moment(bookingDetail?.startDate).format('DD MMM, YYYY')}</Text>
                        </View>
                        <Image source={icons.Line} style={styles.Line} />
                        <View>
                            <View>
                                <Text style={styles.checkInLabel} >Check in :</Text>
                                <Text style={styles.checkInValue}>{moment(bookingDetail?.endDate).format('DD MMM, YYYY')}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.seprator} />
                    <View style={styles.checkinOuterView}>
                        <Text style={styles.checkInLabel} >Pending amount to be paid :</Text>
                        <Text style={[styles.checkInValue, { color: "red" }]}>{'\u20B9'}{bookingDetail?.balanceDue}</Text>
                    </View>
                    {/* {console.log(bookingDetail)&&bookingDetail?.bookingStatus!=5} */}
                    {bookingDetail?.balanceDue > 0 && bookingDetail?.bookingStatus != 5 ?
                        <View style={{ alignItems: 'center' }}>
                            <TouchableOpacity style={styles.paymentButton} onPress={() => startPayment()} >
                                <Text style={styles.paymentbuttonlabel}>Make Payment</Text>
                            </TouchableOpacity>
                        </View>
                        : null}
                </View>

                <View style={{ backgroundColor: "#ffffff", padding: 20, marginTop: 20, flex: 1, elevation: 2 }}>
                    <Text style={styles.Reserevation}>Reserevation Details</Text>

                    <Text style={styles.ReserevationLabel}>Booking Date: <Text style={styles.ReserevationValue}>{moment(bookingDetail?.createdAt).format('DD MMM, YYYY')}</Text></Text>
                    <Text style={styles.ReserevationLabel}>Booking No: <Text style={styles.ReserevationValue}>{bookingDetail?.bookingNo}</Text> </Text>
                    <Text style={styles.ReserevationLabel}>Booking Type: <Text style={styles.ReserevationValue}>{bookingDetail?.bookingType}</Text></Text>
                    <Text style={styles.ReserevationLabel}>Number of Guest: <Text style={styles.ReserevationValue}>{bookingDetail?.bedsData?.length}</Text></Text>
                    <Text style={styles.ReserevationLabel}>Property: <Text style={styles.ReserevationValue}>{bookingDetail?.propertyName}</Text></Text>

                    {bookingDetail?.bedsData?.length > 0 ?
                        <>
                            {bookingDetail?.bedsData?.map((item: any) => (
                                <View style={{ backgroundColor: '#ffffff', elevation: 1, marginVertical: 5, padding: 10, borderRadius: 8 }}>
                                    <Text style={{ color: '#0000005a', fontSize: 14, fontFamily: "Poppins-Medium" }}>Room Type : <Text style={{ color: '#000000', }}>{item?.roomType}</Text> </Text>
                                    <Text style={{ color: '#0000005a', fontSize: 14, fontFamily: "Poppins-Medium" }}>Room Number :  <Text style={{ color: '#000000', }}>{item?.roomNo}-{item?.bedNo}</Text></Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                                        <Text style={{ color: '#0000005a', fontSize: 14, fontFamily: "Poppins-Medium" }}>Your per day rent is :  </Text>
                                        <Text style={{ color: '#003F6F', }}>{'\u20B9'}{item?.rate}</Text>
                                    </View>
                                </View>
                            ))}
                        </> : null}





                </View>

                <TouchableOpacity activeOpacity={0.8} style={styles.tabButtons} onPress={() => navigation.navigate('BookingPayments')}>
                    <View>
                        <Text style={styles.ReserevationValue}>Payments</Text>
                        <Text style={[styles.ReserevationLabel, { fontSize: 14 }]}>Your payment details of rent, food etc.</Text>
                    </View>
                    <Image
                        source={icons.rightArrow} style={{ height: 15, width: 8.8 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.tabButtons} onPress={() => navigation.navigate('BookingInvoices')} >
                    <View>
                        <Text style={styles.ReserevationValue}>Invoices</Text>
                        <Text style={[styles.ReserevationLabel, { fontSize: 14 }]}>Your invoice for rent, food etc.</Text>
                    </View>
                    <Image
                        source={icons.rightArrow} style={{ height: 15, width: 8.8 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.tabButtons} onPress={() => navigation.navigate('BookingPenalties')}>
                    <View>
                        <Text style={styles.ReserevationValue}>Penalties</Text>
                        <Text style={[styles.ReserevationLabel, { fontSize: 14 }]}>Your payment details of rent, food etc.</Text>
                    </View>
                    <Image
                        source={icons.rightArrow} style={{ height: 15, width: 8.8 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8} style={styles.tabButtons} onPress={() => navigation.navigate('BookingDocument')}>
                    <Text style={styles.ReserevationValue}>Documents</Text>
                    <Image
                        source={icons.rightArrow} style={{ height: 15, width: 8.8 }}
                    />
                </TouchableOpacity>
            </ScrollView>
            <PaymentStatusModal
                status={PaymentStatus}
                visible={visiblePaymentStatus}
                navigation={navigation}
                // orderId={orderId}
                dismiss_drawer={() => { setvisiblePaymentStatus(false), navigation.navigate('Bookings') }}
            />
        </View>
    )
}

export default BookingDetail

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    heading: {
        color: '#000000',
        fontSize: 20,
        fontFamily: 'Poppins-SemiBold'
    },
    headingouterview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20
    },
    backbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#ffffff',
        height: 30,
        width: 30
    },
    subheading: {
        color: "#272727",
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold'
    },
    checkInLabel: {
        color: "#808080",
        fontSize: 14,
        fontFamily: "Inter-Medium"
    },
    checkInValue: {
        color: "#272727",
        fontSize: 16,
        fontFamily: "Inter-SemiBold"
    },
    Line: {
        height: 50,
        width: 2
    },
    seprator: {
        backgroundColor: '#E6E6E6',
        height: 1,
        marginVertical: 15
    },
    checkinOuterView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1,
    },
    paymentButton: {
        backgroundColor: '#ef4141',
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        borderRadius: 6,
        marginTop: 15
    },
    paymentbuttonlabel: {
        color: '#ffffff',
        fontSize: 15,
        fontFamily: 'Inter-SemiBold'
    },
    Reserevation: {
        color: '#000000',
        fontSize: 18,
        fontFamily: 'Inter-SemiBold'
    },
    ReserevationLabel: {
        color: '#808080',
        fontSize: 16,
        fontFamily: 'Inter-Regular',
        marginVertical: 3
    },
    ReserevationValue: {
        color: '#000000',
        fontSize: 16,
        fontFamily: 'Inter-Medium'
    },
    tabButtons: {
        backgroundColor: "#ffffff",
        padding: 20,
        elevation: 2,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: "space-between"
    }
})