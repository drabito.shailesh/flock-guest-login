import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Dimensions } from 'react-native'
import icons from '../../Constant/icons'
import axios from '../../axiosConfig';
import moment from 'moment'
import PenaltiDetailModal from '../../Modal/PenaltiDetailModal';
import Animated, {
    FadeIn,
    Layout
} from "react-native-reanimated";
import { Skeleton } from 'moti/skeleton';
import { useSelector } from 'react-redux';
import Loader from '../../utils/functions/Loader';
import images from '../../Constant/images';

const { height, width } = Dimensions.get('window');

const BookingPenalties = ({ navigation }: any) => {
    const bookingDetail = useSelector((state: any) => state?.BookingIdReducer?.BookingId);
    const [BookingPenaltieslist, setBookingPenaltiesList] = useState<any>([])
    const [BookingPenaltiesDetail, setBookingPenaltiesDetail] = useState<any>('')
    const [showPenaltiDetailModal, setShowPenaltiDetailModal] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    const SkeletonCommonProps = {
        colorMode: 'light',
        transition: {
            type: 'timing',
            duration: 1500,
        },
        backgroundColor: '#D4D4D4',
    } as const;

    const gelAllBookingPenalties = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`properties/${bookingDetail?.propertyId}/penalties?bookingId=${bookingDetail?.bookingId}&pageSize=${1500}`);
            setBookingPenaltiesList(response?.data?.data?.content)
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    useEffect(() => {
        gelAllBookingPenalties()
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            gelAllBookingPenalties()
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
                        <Image source={icons.back} style={{ height: 21.19, width: 10.55, right: 2 }} />
                    </TouchableOpacity>

                    <Text style={styles.heading}>Penalties</Text>
                    <Text></Text>
                </View>
            </View>
            {
                loading && <Loader />
            }
            <View style={{ padding: 22, paddingBottom: height / 7 }}>
                {
                    BookingPenaltieslist?.length < 1 ?
                        <View style={{ alignItems: 'center' }}>
                            <Image
                                source={images.noPenalty}
                                style={{ height: 300, width: 300, marginTop: height / 5, opacity: 0.7 }}
                            />
                        </View>
                        :
                        <FlatList
                            data={BookingPenaltieslist}
                            // keyExtractor={(item: any) => { item?.id?.toString() }}
                            renderItem={({ item }: any) => {
                                return (
                                    <View style={styles.ListOuterView}>
                                        <Text style={styles.bookingNo}>Booking No : <Text style={{ fontFamily: 'Poppins-SemiBold', color: "#000000" }}>{item?.bookingNo}</Text></Text>
                                        <View style={styles.seprator} />
                                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', flex: 1 }}>
                                            <Text style={styles.value} >{item?.penaltyType}</Text>
                                            <Text style={[styles.value, { color: "#ef4141" }]}>{'\u20B9'}{item?.amount}</Text>
                                        </View>
                                        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', flex: 1, marginTop: 10 }}>
                                            <Text style={styles.value} >Penalty Date</Text>
                                            {/* moment().format("DD MMM, YYYY") */}
                                            <Text style={[styles.value]}>{moment(item?.date).format("DD MMM, YYYY")}</Text>
                                        </View>
                                    </View>
                                )
                            }}
                        />

                }

            </View>
            {
                showPenaltiDetailModal &&
                <PenaltiDetailModal
                    isVisible={showPenaltiDetailModal}
                    onClose={() => setShowPenaltiDetailModal(false)}
                    item={BookingPenaltiesDetail}
                />
            }
        </View>
    )
}

export default BookingPenalties

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FAFAFA"
    },
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-SemiBold',
        color: "#000000"
    },
    backbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#ffffff',
        height: 30,
        width: 30
    },
    ListOuterView: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 8,
        elevation: 2,
        paddingVertical: 15
    },
    bookingNo: {
        fontSize: 13,
        fontFamily: 'Inter-Medium',
        color: '#808080',
        paddingHorizontal: 15
    },
    seprator: {
        backgroundColor: "#E6E6E6",
        height: 1,
        marginVertical: 15
    },
    value: {
        fontSize: 14,
        fontFamily: 'Inter-Medium',
        color: '#272727',
        paddingHorizontal: 15
    }
})