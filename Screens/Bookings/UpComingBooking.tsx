import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, Dimensions, RefreshControl } from 'react-native'
import axios from '../../axiosConfig'
import images from '../../Constant/images'
import CustomIndicator from '../../utils/functions/CustomIndicator'
import icons from '../../Constant/icons'
import moment from 'moment'
import { useSelector, useDispatch } from 'react-redux'
import { setBookingIdAction } from '../../redux/actions/BookingId-action'
import Loader from '../../utils/functions/Loader'

const { height, width } = Dimensions.get('window')

const UpComingBooking = ({ navigation }: any) => {

  const dispatch = useDispatch()

  const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
  const pageNo = useRef(0);
  const totalPages = useRef(0);

  const [UpComingBookingList, setUpComingBookingList] = useState([])
  const [refreshing, setRefreshing] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [bottomLoader, setBottomLoader] = useState(false);
  const [loading, setLoading] = useState<boolean>(true);


  const getAllUpComingBooking = async () => {
    setLoading(true)
    try {
      const response = await axios.get(`guests/bookings/details?isFutureBooking=${true}`)
      // console.log(response.data.data)
      setUpComingBookingList(response?.data?.data);
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }

  const getminimaster = async () => {
    try {
      const response = await axios.get(`mm`)
      // console.log(response.data.data.bookingStatus)
    } catch (error) {
      console.log(error)
    }
  }

  const _onRefresh = () => {
    setRefreshing(true);
    getAllUpComingBooking();
    setRefreshing(false);
  };


  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getAllUpComingBooking()

    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    getminimaster()
    getAllUpComingBooking()
  }, [])




  const renderBookings = (item: any) => {
    return (
      <TouchableOpacity onPress={() => { navigation.navigate('BookingDetail'), dispatch(setBookingIdAction(item?.item)) }}>
        <View style={styles.cardOuterView}>
          <View style={styles.cardUperView}>
            <View>
              <Text style={styles.propertyName} >{item?.item?.propertyName}</Text>
              <Text style={styles.createdAt}>{moment(item?.item?.createdAt).format("DD MMM,YYYY")}</Text>
            </View>
            <Image source={icons.rightArrow} style={{ height: 14, width: 8 }} />
          </View>
          <View style={styles.seprator} />

          <View style={styles.cardUperView}>
            <Text style={styles.propertyName} >Booking No: {item?.item?.bookingNo}</Text>
            <View style={[styles.bookingstatusview, { backgroundColor: item?.item?.bookingStatus == 3 ? "#003F6F1a" : "#7239ea1a" }]}>
              <Text style={[styles.bookingstatus, { color: item?.item?.bookingStatus == 3 ? "#003F6F" : "#7239ea" }]}>{item?.item?.bookingStatus == 3 ? "Confirmed" : "Booked"}</Text>
            </View>
          </View>
          <View style={styles.seprator} />
          {item?.item?.bookingRoomTypeDetails?.map((RoomTypeDetails: any) => {
            // console.log(RoomTypeDetails,'????????????')
            return (
              <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                <View style={styles.roomimageOuterView}>
                  {RoomTypeDetails?.roomTypeImages?.length > 0 ?
                    <Image
                      source={{ uri: RoomTypeDetails?.roomTypeImages[0]?.filePath }}
                      style={{ height: 100, width: 80, borderRadius: 5 }}
                    /> :
                    <Image
                      source={icons.room}
                      style={{ height: 50, width: 50, borderRadius: 5, tintColor: 'lightgrey' }}
                    />
                  }
                </View>
                <View style={{ flex: 1, paddingHorizontal: 5 }}>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Room Type: <Text style={{ color: "#272727" }}>{RoomTypeDetails?.roomTypeName}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Booking Type: <Text style={{ color: "#272727" }}>{item?.item?.bookingType}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check In: <Text style={{ color: "#272727" }}>{moment(item?.item?.startDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check Out: <Text style={{ color: "#272727" }}>{moment(item?.item?.endDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                </View>
              </View>
            )
          })}


        </View>
      </TouchableOpacity>
    )
  }

  // <TouchableOpacity></TouchableOpacity>

  // console.log(UpComingBookingList[0])

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
      <View style={{ paddingHorizontal: 20, marginTop: 20, paddingBottom: height / 15 - 50 }}>
        {loading && <Loader />}
        {UpComingBookingList?.length < 1 && loading == false ?
          <View style={{ alignItems: 'center' }}>
            <Image
              source={images.nobooking}
              style={{ height: 295, width: 310, marginTop: height / 5, opacity: 0.7 }}
            />
            <Text style={{ color: '#000000', fontFamily: "Poppins-Medium", fontSize: 16, textAlign: 'center', paddingHorizontal: 15, marginTop: 20 }}>
              you have no upcoming bookings
            </Text>
          </View>
          :
          <FlatList
            data={UpComingBookingList}
            showsVerticalScrollIndicator={false}
            renderItem={renderBookings}
            onEndReachedThreshold={0.5}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={_onRefresh}
              />
            }
            keyExtractor={(item: any) =>
              item?.bookingId?.toString()
            }
          />
        }
      </View>
    </View>
  )
}

export default UpComingBooking

const styles = StyleSheet.create({
  cardOuterView: {
    borderWidth: 1,
    borderColor: '#E6E6E6',
    marginVertical: 5,
    borderRadius: 6,
    paddingVertical: 10,
    elevation: 2,
    backgroundColor: '#ffffff',
  },
  cardUperView: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5
  },
  propertyName: {
    color: "#272727",
    fontFamily: 'Inter-SemiBold',
    fontSize: 16
  },
  createdAt: {
    color: "#808080",
    fontFamily: 'Inter-SemiBold',
    fontSize: 14
  },
  seprator: {
    borderWidth: 0.7,
    borderColor: '#E6E6E6',
    marginVertical: 8
  },
  bookingstatus: {
    color: "#003F6F",
    fontFamily: 'Inter-Medium',
    fontSize: 12
  },
  bookingstatusview: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    backgroundColor: '#003F6F1A',
    borderRadius: 5,
    paddingVertical: 4
  },
  roomimageOuterView: {
    borderWidth: 0.7,
    marginHorizontal: 8,
    borderRadius: 5,
    borderColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    width: 80
  }

})