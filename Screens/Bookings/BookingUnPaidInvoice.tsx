import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, FlatList, Platform } from 'react-native';
import axios from '../../axiosConfig';
import icons from '../../Constant/icons';
import moment from 'moment';
import { getToken } from '../../utils/functions';
import RNFetchBlob from 'rn-fetch-blob';
import { useSelector } from 'react-redux';
import AllInOneSDKManager from 'paytm_allinone_react-native';

const { height, width } = Dimensions.get('window');


const BookingUnPaidInvoice = ({ navigation }: any) => {

    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    const bookingDetail = useSelector((state: any) => state?.BookingIdReducer?.BookingId);
    // console.log(userDetails,'?????????????????')

    const [BookingUnPaidInvoicesData, setBookingUnPaidInvoicesData] = useState<any>([])
    const [loading, setLoading] = useState<boolean>(false);
    const [totalPaidAmount, setTotalPaidAmount] = useState<number>(0);

    const getAllBookingUnPaidInvoice = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`payments/invoices?status=UNPAID&pageSize${1500}&propertyId=${bookingDetail?.propertyId}&bookingId=${bookingDetail?.bookingId}`);
            setBookingUnPaidInvoicesData(response?.data?.data?.content);
            const totalAmount = response?.data?.data?.content?.reduce((total: any, item: any) => total + item?.netReceivable, 0);
            setTotalPaidAmount(totalAmount)
            setLoading(false)
        } catch (error: any) {
            setLoading(false)
            console.log(error.response)
        }
    }


    useEffect(() => {
        getAllBookingUnPaidInvoice()
    }, [])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAllBookingUnPaidInvoice()
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    const downloadBookingUnPaidInvoices = async (item: any) => {
        const token = await getToken();
        try {
            const date = new Date();
            const filename = `invoice${Math.floor(
                date.getTime() + date.getSeconds() / 2,
            )}.pdf`;

            const dirs = RNFetchBlob.fs.dirs;

            const path =
                Platform.OS == 'ios'
                    ? `${dirs.DocumentDir}/${filename}`
                    : `${dirs.DownloadDir}/${filename}`;
            RNFetchBlob.config({
                fileCache: true,
                appendExt: 'pdf',
                // mime: 'application/pdf',
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    title: filename,
                    description: 'Downloading PDF file...',
                    path: path,
                    mime: 'application/pdf',
                    mediaScannable: true,
                },
                path: path,
            })

                .fetch(
                    'GET', // Use GET instead of POST
                    `https://pgapidev.inn4smart.com/api/organizations/${userDetails?.organizationId}/bookings/${item?.bookingId}/invoices/${item?.id}/download`,
                    {
                        Authorization: `Bearer ${token}`,
                    },
                )
                .then(response => {
                    // if (Platform.OS === 'android') {
                    RNFetchBlob.android.actionViewIntent(
                        response.path(),
                        'application/pdf',
                    );
                    //   setShowBillingModal(false);

                })
                .catch(error => {
                    console.log('Error downloading image:', error);
                    // Handle the error, such as displaying an error message
                });

        } catch (error: any) {
            console.log(error.response)
        }
    }


    const startPayment = async (item: any) => {
        const data = {
            amount: item?.netReceivable,
            invoiceId: item?.id,
        };
        try {
            const response = await axios.post(
                `properties/${item?.propertyId}/bookings/${item?.bookingId}/payments/paytm`,
                data,
            );

            // console.log(response.data.data, '........../////')
            let orderId = response?.data?.data?.content?.body?.orderId
            let merId = response?.data?.data?.content?.body?.mid
            let txnt = response?.data?.data?.content?.payment_gateway_response?.txnToken
            let val = response?.data?.data?.content?.body?.value
            let callbackUrl = response?.data?.data?.content?.body?.callBackUrl
            let stag = true
            let appInkeReted = true


            await openPaytm(orderId, merId, txnt, val, callbackUrl, stag, appInkeReted)
        } catch (error: any) {
            console.log(error.response, '1111111111111111111111');
        }
    };

    const openPaytm = async (
        ordrId: any,
        merId: any,
        txnt: any,
        val: any,
        callbackUrl: any,
        stag: any,
        appInkeReted: any) => {
        AllInOneSDKManager.startTransaction(
            ordrId,
            merId,
            txnt,
            val,
            callbackUrl,
            stag,
            appInkeReted,
            ''
        )
            .then(async result => {
                console.log(result, 'then');
                // console.warn(res2.data);
            })
            .catch(err => {
                console.log(err, '/////////???????????????')
            });
    }


    const renderItem = (item: any) => {
        // console.log(item?.item)
        return (
            <View >
                <View style={styles.cardUperView}>
                    <Text style={styles.createdAt}>{moment(item?.item?.billingStartDate).format("DD MMM,YYYY")}</Text>
                    <Text style={styles.createdAt}>Due Date: {moment(item?.item?.billingEndDate).format("DD MMM,YYYY")}</Text>
                </View>
                <View style={styles.cardOuterView}>
                    <View style={[styles.cardUperView, { paddingHorizontal: 15 }]}>
                        <Text style={styles.propertyName} >Invoice No: {item?.item?.invoiceNo}</Text>
                    </View>
                    <View style={styles.seprator} />
                    <View style={[styles.cardUperView, { paddingHorizontal: 15 }]}>
                        <Text style={styles.createdAt}>{item?.item?.invoiceFor} Invoice</Text>
                        <Text style={[styles.createdAt, { color: '#ef4141' }]}>{'\u20B9'}{item?.item?.netReceivable}</Text>
                    </View>
                    <View style={[styles.cardUperView, { paddingHorizontal: 15 }]}>
                        <Text style={styles.createdAt}>Total</Text>
                        <Text style={[styles.createdAt, { color: '#000000' }]}>{'\u20B9'}{item?.item?.invoiceAmount}</Text>
                    </View>
                    <View style={styles.seprator} />
                    <View style={[styles.cardUperView, { paddingHorizontal: 15 }]}>

                        <TouchableOpacity style={styles.downloadbutton} onPress={() => downloadBookingUnPaidInvoices(item?.item)}>

                            <Text style={[styles.createdAt, { color: '#003F6F', fontSize: 15 }]}>Download</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.downloadbutton} onPress={() => startPayment(item?.item)}>

                            <Text style={[styles.createdAt, { color: '#003F6F', fontSize: 15 }]}>Make Payment</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }


    return (
        <View style={styles.container}>
            <View style={{ marginTop: 20, paddingHorizontal: 22, paddingBottom: height / 7 }}>
                <View style={styles.padidAmountView}>
                    <Text style={styles.propertyName}>Total Unpaid Amount</Text>
                    <Text style={styles.totalAmounttext}>{'\u20B9'}{totalPaidAmount}</Text>
                </View>
                <FlatList
                    data={BookingUnPaidInvoicesData}
                    renderItem={renderItem}
                />
            </View>
        </View>
    )
}

export default BookingUnPaidInvoice

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    header: {
        backgroundColor: '#fffffF',
        padding: 22,
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Inter-SemiBold',
        color: "#000000"
    },
    BookingUnPaidInvoicescontainer: {
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        backgroundColor: '#ffffff',
        marginTop: 10
    },
    cardOuterView: {
        borderWidth: 1,
        borderColor: '#E6E6E6',
        marginVertical: 5,
        borderRadius: 6,
        paddingVertical: 10,
        elevation: 2,
        backgroundColor: '#ffffff',
    },
    cardUperView: {
        // paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 5,
        flex: 1
    },
    propertyName: {
        color: "#272727",
        fontFamily: 'Inter-SemiBold',
        fontSize: 14
    },
    createdAt: {
        color: "#808080",
        fontFamily: 'Inter-SemiBold',
        fontSize: 14
    },
    seprator: {
        borderWidth: 0.7,
        borderColor: '#E6E6E6',
        marginVertical: 8
    },
    bookingstatus: {
        color: "#003F6F",
        fontFamily: 'Inter-Medium',
        fontSize: 12
    },
    bookingstatusview: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 8,
        backgroundColor: '#003F6F1A',
        borderRadius: 5,
        paddingVertical: 4
    },
    roomimageOuterView: {
        borderWidth: 0.7,
        marginHorizontal: 8,
        borderRadius: 5,
        borderColor: 'lightgrey',
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        width: 80
    },
    backButton: {
        backgroundColor: '#FAFAFA',
        height: 35,
        width: 35,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    padidAmountView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ef41411a',
        borderWidth: 0.8,
        borderColor: '#ef4141',
        borderStyle: 'dashed',
        borderRadius: 8,
        paddingVertical: 15,
        marginBottom: 20
    },
    totalAmounttext: {
        color: '#ef4141',
        fontSize: 20,
        fontFamily: 'Inter-Bold'
    },
    downloadbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#003F6F1a',
        height: 40,
        width: width / 2.8,
        borderRadius: 8
    }
})