import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList, Platform } from 'react-native';
import axios from '../../axiosConfig';
import icons from '../../Constant/icons';
import PDFView from 'react-native-view-pdf';
import { useSelector } from 'react-redux';
import Toast from 'react-native-toast-message';
import Loader from '../../utils/functions/Loader';
import RNFetchBlob from 'rn-fetch-blob';

const { height, width } = Dimensions.get('window');

const BookingDocument = ({ navigation }: any) => {


    const bookinId = useSelector((state: any) => state?.BookingIdReducer?.BookingId);

    const [DocumentData, setDocumentData] = useState([])
    const [loading, setLoading] = useState<boolean>(false);

    const getBookingDocument = async () => {
        try {
            setLoading(true)
            const response = await axios.get(`properties/${bookinId?.propertyId}/bookings/${bookinId?.bookingId}/guest-documents`)
            if (bookinId?.bookingType == "DIRECT") {
                setDocumentData(response?.data?.data?.guestDocuments);
            } else {
                setDocumentData(response?.data?.data?.companyDocuments);
            }
            setLoading(false);
        } catch (error: any) {
            setLoading(false);
            console.log(error?.response);
        }
    }

    const resources = {
        file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
        url: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        base64: 'JVBERi0xLjMKJcfs...',
    };

    const resourceType = 'url';

    useEffect(() => {
        getBookingDocument()
    }, [bookinId?.bookingId])


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getBookingDocument()
        });
        // Return the  to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    const DownloadDocument = async (imagePath: any) => {
        let ext = imagePath.split('.').pop();
        ext = ext.indexOf('?') === -1 ? ext : ext.split('?')[0];
        try {
            const date = new Date();
            const dirs = RNFetchBlob.fs.dirs;
            const filename = `${ext}${Math.floor(
                date.getTime() + date.getSeconds() / 2,
            )}.${ext}`;

            const path =
                Platform.OS == 'ios'
                    ? `${dirs.DocumentDir}/${filename}`
                    : `${dirs.DownloadDir}/${filename}`;

            Toast.show({
                type: 'success',
                text1: '',
                text2: `  Please wait...`,
                visibilityTime: 2500,
                autoHide: true,
            });

            const mime = ext == 'jpg' || ext == 'jpeg' || ext == 'png' ? 'image/jpeg' : 'application/pdf'

            RNFetchBlob.config({
                fileCache: true,
                appendExt: ext,
                // mime: 'image/jpeg',
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    title: filename,
                    description: 'Downloading image file...',
                    path: path,
                    mime: mime,
                    mediaScannable: true,
                },
                path: path,
            })
                .fetch('GET', imagePath)
                .then(res => {
                    Toast.show({
                        type: 'success',
                        text1: '',
                        text2: `  Document downloaded successfully`,
                        visibilityTime: 2500,
                        autoHide: true,
                    });
                    if (Platform.OS == 'ios') {
                        RNFetchBlob.ios.previewDocument(res.path());
                    }
                })
                .catch(error => {
                    console.log('Error downloading image:', imagePath, error);
                    // Handle the error, such as displaying an error message
                });

        } catch (error: any) {
            console.log(error.response)
        }

    }


    const renderItem = (item: any) => {
        // console.log(item?.item)
        return (
            <View style={{ padding: 15, backgroundColor: '#ffffff', marginVertical: 8 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <View style={{ paddingHorizontal: 5 }}>
                        <Text style={{ color: "#272727", fontSize: 16, fontFamily: 'Inter-SemiBold' }}>{item?.item?.documentType}</Text>
                        <Text style={{ color: "lightgrey", fontSize: 14, fontFamily: 'Inter-SemiBold', marginTop: 6 }}>Added on : <Text style={{ color: "#272727" }}>{item?.item?.uploadDateTime}</Text></Text>
                    </View>
                    <TouchableOpacity style={styles.downloadButton} onPress={
                        () => DownloadDocument(item?.item?.documentFile)
                    }>
                        <Image source={icons.download} style={{ height: 20, width: 20, tintColor: '#000000' }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerview}>
                    <TouchableOpacity style={styles.backbutton} onPress={() => navigation.goBack()}>
                        <Image
                            source={icons.back}
                            style={{ height: 21.19, width: 10.55, right: 2 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Document</Text>
                    <Text></Text>
                </View>
            </View>
            {loading && <Loader />}

            <View style={{ marginTop: 20 }}>
                <FlatList
                    data={DocumentData}
                    keyExtractor={(item: any, index: number) => item?.documentId || index.toString()}
                    renderItem={renderItem}
                />
            </View>

        </View>
    )
}

export default BookingDocument

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FAFAFA"
    },
    header: {
        backgroundColor: '#ffffff',
        padding: 22,
        elevation: 2
    },
    headerview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30
    },
    heading: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: "#000000"
    },
    documentcontainer: {
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        backgroundColor: '#ffffff',
        marginTop: 10
    },
    documentName: {
        fontSize: 16,
        fontFamily: 'Poppins-SemiBold',
        color: '#000000'
    },
    documentdate: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color: '#003F6F'
    },
    backbutton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#ffffff',
        height: 30,
        width: 30
    },
    downloadButton: {
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 4,
        backgroundColor: '#2727270A',
        borderColor: "#2727270A"
    }

})