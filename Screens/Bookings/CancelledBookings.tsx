import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, Dimensions, RefreshControl, Animated } from 'react-native'
import axios from '../../axiosConfig'
import images from '../../Constant/images'
import CustomIndicator from '../../utils/functions/CustomIndicator'
import icons from '../../Constant/icons'
import moment from 'moment'
import AllInOneSDKManager from 'paytm_allinone_react-native';
import { useSelector, useDispatch } from 'react-redux'
import { setBookingIdAction } from '../../redux/actions/BookingId-action'
import PaymentStatusModal from '../../Modal/PaymentStatusModal'
import CreateBookingOptionModal from '../../Modal/CreateBookingOptionModal'
import Loader from '../../utils/functions/Loader'

const { height, width } = Dimensions.get('window')

const CancelledBookings = ({ navigation }: any) => {

  const dispatch = useDispatch()

  const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
  // console.log(userDetails)
  const pageNo = useRef(0);
  const totalPages = useRef(0);

  const [CancelledBookingList, setCancelledBookingList] = useState([])
  const [refreshing, setRefreshing] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [bottomLoader, setBottomLoader] = useState(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [visiblePaymentStatus, setvisiblePaymentStatus] = useState<boolean>(false);
  const [visibleBookingOptionModal, setvisibleBookingOptionModal] = useState<boolean>(false);
  const [PaymentStatus, setPaymentStatus] = useState<any>('');


  const getAllCancelledBookings = async () => {
    setLoading(true)
    try {
      const response = await axios.get(`guests/bookings/details?isCancelledBooking=${true}`)
      // console.log(response.data.data)
      setCancelledBookingList(response?.data?.data);
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }
  }

  const getminimaster = async () => {
    try {
      const response = await axios.get(`mm`)
      // console.log(response.data.data.paymentStatus)
    } catch (error) {
      console.log(error)
    }
  }

  const _onRefresh = () => {
    setRefreshing(true);
    getAllCancelledBookings();
    setRefreshing(false);
  };


  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getAllCancelledBookings()

    });
    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    getminimaster()
    getAllCancelledBookings()
    startAnimation()
  }, [])

  const startPayment = async (item: any) => {
    // console.log(item,'>>>>>>......')
    // return
    const data = {
      amount: item?.bookingAmount,
    };
    try {
      const response = await axios.post(
        `properties/${item?.propertyId}/bookings/${item?.bookingId}/payments/paytm`,
        data,
      );

      // console.log(response.data.data, '........../////')
      let orderId = response?.data?.data?.content?.body?.orderId
      let merId = response?.data?.data?.content?.body?.mid
      let txnt = response?.data?.data?.content?.payment_gateway_response?.txnToken
      let val = response?.data?.data?.content?.body?.value
      let callbackUrl = response?.data?.data?.content?.body?.callBackUrl
      let stag = true
      let appInkeReted = true


      await openPaytm(orderId, merId, txnt, val, callbackUrl, stag, appInkeReted, item?.propertyId, item?.bookingId)
    } catch (error: any) {
      console.log(error.response, '1111111111111111111111');
    }
  };

  const openPaytm = async (
    ordrId: any,
    merId: any,
    txnt: any,
    val: any,
    callbackUrl: any,
    stag: any,
    appInkeReted: any,
    propertyId: any,
    bookingId: any
  ) => {
    AllInOneSDKManager.startTransaction(
      ordrId,
      merId,
      txnt,
      val,
      callbackUrl,
      stag,
      appInkeReted,
      ''
    )
      .then(async result => {
        console.log(result, 'then');
        await handleUpdateTransaction(ordrId, propertyId, bookingId)
        setPaymentStatus(result)
        setvisiblePaymentStatus(true)
        getAllCancelledBookings()
        // console.warn(res2.data);
      })
      .catch(err => {
        console.log(err, '/////////???????????????')
      });
  }


  const handleUpdateTransaction = async (orderId: any, propertyId: any, bookingId: any) => {
    const data = { orderId: orderId }
    try {
      const response = await axios.post(`properties/${propertyId}/bookings/${bookingId}/payments/paytm/status`, data)
      // console.log(response?.data?.data, '--------------------------___________')
    } catch (error) {
      console.log(error)
    }
  }


  const opacity = useRef(new Animated.Value(1)).current;
  const startAnimation = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(opacity, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }),
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }),
      ]),
    ).start();
  };


  const renderBookings = (item: any) => {
    return (
      <TouchableOpacity style={styles.cardOuterView} onPress={() => { navigation.navigate('BookingDetail'), dispatch(setBookingIdAction(item?.item)) }}>
        <View style={styles.cardUperView}>
          <View>
            <Text style={styles.propertyName} >{item?.item?.propertyName}</Text>
            <Text style={styles.createdAt}>{moment(item?.item?.createdAt).format("DD MMM,YYYY")}</Text>
          </View>
          <Image source={icons.rightArrow} style={{ height: 14, width: 8 }} />
        </View>
        <View style={styles.seprator} />

        <View style={styles.cardUperView}>
          <Text style={styles.propertyName} >Booking No: {item?.item?.bookingNo}</Text>
          <View style={[styles.bookingstatusview, { backgroundColor: "#ff00001a" }]}>
            <Text style={[styles.bookingstatus, { color: "#ff00009a" }]}>{"Cancelled"}</Text>
          </View>
        </View>
        <View style={styles.seprator} />

        <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                <View style={styles.roomimageOuterView}>
                    <Image
                      source={icons.room}
                      style={{ height: 50, width: 50, borderRadius: 5, tintColor: 'lightgrey' }}
                    />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 5 }}>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Booking Type: <Text style={{ color: "#272727" }}>{item?.item?.bookingType}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check In: <Text style={{ color: "#272727" }}>{moment(item?.item?.startDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check Out: <Text style={{ color: "#272727" }}>{moment(item?.item?.endDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                </View>
              </View>
              {
                item?.item?.bookingStatus == 2 ?
                  <>
                    <View style={styles.seprator} />
                    <View style={{ alignItems: 'flex-end' }}>
                      <TouchableOpacity style={styles.paybutton} onPress={() => startPayment(item?.item)}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontFamily: 'Inter-Medium' }}>Pay Now</Text>
                      </TouchableOpacity>
                    </View>
                  </>
                  : null
              }
            </View>


{/* 
        {item?.item?.bookingRoomTypeDetails?.map((RoomTypeDetails: any, index: any) => {
          // console.log(RoomTypeDetails)
          return (
            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                <View style={styles.roomimageOuterView}>
                  {RoomTypeDetails?.roomTypeImages?.length > 0 ?
                    <Image
                      source={{ uri: RoomTypeDetails?.roomTypeImages[0]?.filePath }}
                      style={{ height: 100, width: 80, borderRadius: 5 }}
                    /> :
                    <Image
                      source={icons.room}
                      style={{ height: 50, width: 50, borderRadius: 5, tintColor: 'lightgrey' }}
                    />
                  }
                </View>
                <View style={{ flex: 1, paddingHorizontal: 5 }}>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Room Type: <Text style={{ color: "#272727" }}>{RoomTypeDetails?.roomTypeName}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Booking Type: <Text style={{ color: "#272727" }}>{item?.item?.bookingType}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check In: <Text style={{ color: "#272727" }}>{moment(item?.item?.startDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                  <Text style={{ fontSize: 14, fontFamily: 'Inter-Medium', color: "#808080" }}>
                    Check Out: <Text style={{ color: "#272727" }}>{moment(item?.item?.endDate).format("DD MMM,YYYY")}</Text>
                  </Text>
                </View>
              </View>
              {
                item?.item?.bookingStatus == 2 ?
                  <>
                    <View style={styles.seprator} />
                    <View style={{ alignItems: 'flex-end' }}>
                      <TouchableOpacity style={styles.paybutton} onPress={() => startPayment(item?.item)}>
                        <Text style={{ color: '#ffffff', fontSize: 15, fontFamily: 'Inter-Medium' }}>Pay Now</Text>
                      </TouchableOpacity>
                    </View>
                  </>
                  : null
              }
            </View>
          )
        })} */}

      </TouchableOpacity>
    )
  }

  // console.log(CancelledBookingList[0])

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
      <View style={{ paddingHorizontal: 20, marginTop: 20, paddingBottom: height / 15 - 50 }}>
        {loading && <Loader />}
        {CancelledBookingList?.length < 1 && loading == false ?
          <View style={{ alignItems: 'center' }}>
            <Image
              source={images.nobooking}
              style={{ height: 295, width: 310, marginTop: height / 5, opacity: 0.7 }}
            />
            <Text style={{ color: '#000000', fontFamily: "Poppins-Medium", fontSize: 16, textAlign: 'center', paddingHorizontal: 15, marginTop: 20 }}>
              you have no Cancelled bookings
            </Text>
          </View>
          :
          <FlatList
            data={CancelledBookingList}
            showsVerticalScrollIndicator={false}
            renderItem={renderBookings}
            onEndReachedThreshold={0.5}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={_onRefresh}
              />
            }
            keyExtractor={(item: any, index: any) =>
              item?.bookingId?.toString() || index?.toString()
            }
          />
        }
      </View>

      <Animated.View
        style={{ alignItems: 'flex-end', bottom: 20, opacity, right: 20, position: 'absolute' }}>
        <TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#003F6F', borderRadius: 50, height: 55, width: 55 }}
          onPress={() => { setvisibleBookingOptionModal(true) }}
        >
          <Text style={{ fontSize: 40, color: '#ffffff', bottom: 4 }}>
            +
          </Text>
        </TouchableOpacity>
      </Animated.View>

      <PaymentStatusModal
        status={PaymentStatus}
        visible={visiblePaymentStatus}
        navigation={navigation}
        // orderId={orderId}
        dismiss_drawer={() => { setvisiblePaymentStatus(false), navigation.navigate('Bookings') }}
      />

      <CreateBookingOptionModal
        visible={visibleBookingOptionModal}
        navigation={navigation}
        // orderId={orderId}
        dismiss_drawer={() => { setvisibleBookingOptionModal(false) }}
      />
    </View>
  )
}

export default CancelledBookings

const styles = StyleSheet.create({
  cardOuterView: {
    borderWidth: 1,
    borderColor: '#E6E6E6',
    marginVertical: 5,
    borderRadius: 6,
    paddingVertical: 10,
    elevation: 2,
    backgroundColor: '#ffffff',
  },
  cardUperView: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5
  },
  propertyName: {
    color: "#272727",
    fontFamily: 'Inter-SemiBold',
    fontSize: 16
  },
  createdAt: {
    color: "#808080",
    fontFamily: 'Inter-SemiBold',
    fontSize: 14
  },
  seprator: {
    borderWidth: 0.7,
    borderColor: '#E6E6E6',
    marginVertical: 8
  },
  bookingstatus: {
    color: "#003F6F",
    fontFamily: 'Inter-Medium',
    fontSize: 12
  },
  bookingstatusview: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    backgroundColor: '#003F6F1A',
    borderRadius: 5,
    paddingVertical: 4
  },
  roomimageOuterView: {
    borderWidth: 0.7,
    marginHorizontal: 8,
    borderRadius: 5,
    borderColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    width: 80
  },
  paybutton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#198754',
    borderRadius: 5,
    height: 35,
    width: 100,
    marginRight: 10
  }

})