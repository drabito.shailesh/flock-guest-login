import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, Dimensions, TextInput, TouchableOpacity, Keyboard } from 'react-native'
import images from '../../Constant/images'
import icons from '../../Constant/icons'
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import * as yup from 'yup';
import { Formik } from 'formik';
import axios from '../../axiosConfig'
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../utils/functions/Loader';
import { setLoginDataAction } from '../../redux/actions/login-action';
import { useDispatch, useSelector } from 'react-redux';
import { setUserDataAction } from '../../redux/actions/userDetail-action';

const { height, width } = Dimensions.get('window')
const CELL_COUNT = 6;

const Login = () => {


    const dispatch = useDispatch();

    const formikRef: any = useRef(null);
    const [showOtpField, setShowOtpField] = useState(false);
    const [phoneNumber, setPhonenumber] = useState('');
    const [value, setValue] = useState('');
    const [loading, setLoading] = useState(false);
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [otpTimer, setOtpTimer] = useState(30);
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    const initialValues = {
        phoneNumber: '',
    };
    const loginValidationSchema = yup.object().shape({
        phoneNumber: yup
            .string()
            .trim()
            .required('Mobile number is required')
            .matches(RegExp(/^[0-9]{10}$/), 'Enter 10 digit phone number'),
    });

    const handleReset = () => {
        if (formikRef.current) {
            formikRef.current.resetForm();
        }
    };

    useEffect(() => {
        let interval = setInterval(() => {
            setOtpTimer(prevTimer => {
                if (prevTimer > 0) {
                    return prevTimer - 1;
                } else {
                    return prevTimer;
                }
            });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    const handleLogin = (values: any) => {
        if (showOtpField == true) {
            handleVerifyOTP();
        } else {
            LoginWithOtpHandle(values);
        }
    };


    const handleVerifyOTP = async () => {
        if (value.length === 6) {
            setLoading(true);
            const formData = {
                mobileNo: phoneNumber,
                otp: value
            }
            try {
                const response = await axios.post(
                    `/auth/guest/verifyOTP`,
                    formData,
                );
                Toast.show({
                    type: 'success',
                    text1: '',
                    text2: `  You have successfully logged in.`,
                    visibilityTime: 2500,
                    autoHide: true,
                });

                await AsyncStorage.setItem('token', response?.data?.data?.token);
                dispatch(setLoginDataAction(response?.data));
                
                setTimeout(() => {
                    getUserDetail()
                }, 400);

                setLoading(false);
            } catch (error: any) {
                Toast.show({
                    type: 'error',
                    text1: '',
                    text2: `  Incorrect OTP`,
                    visibilityTime: 2500,
                    autoHide: true,
                });
                console.log(error.response);
                setLoading(false);
            }
        }
    };


    const LoginWithOtpHandle = async (values: any) => {
        setLoading(true);
        const data = {
            mobileNo: values.phoneNumber
        }
        try {
            const response = await axios.post(
                '/auth/guest/generateOTP',
                data,
            );
            setPhonenumber(values.phoneNumber);
            Toast.show({
                type: 'success',
                text1: '',
                text2: `  OTP has been sent to your number`,
                visibilityTime: 2500,
                autoHide: true,
            });
            // handleUserDetails();
            setShowOtpField(true);
            setLoading(false);
        } catch (error: any) {
            Toast.show({
                type: 'error',
                text1: '',
                text2: `${error?.response?.data?.errors[0]}`,
                visibilityTime: 2500,
                autoHide: true,
            });
            console.log(error.response);
            setLoading(false);
        }
    };


    const getUserDetail = async () => {
        try {
            const response = await axios.get(`auth/guest/me`)
            // console.log(response?.data?.data, '??????????????????????????')
            dispatch(setUserDataAction(response?.data?.data))
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <ImageBackground style={styles.container} source={images.Backgroundlogin}>
            <View style={{ alignItems: 'center', marginTop: height / 10 - 10 }}>
                <Image
                    source={images.logo}
                    style={{ height: 60, width: 170 }}
                />
            </View>
            {loading && <Loader />}
            <View style={{ marginTop: height / 9 }}>
                <Text style={styles.heading}>Log In</Text>
                <Text style={styles.subheading}>Log into comfortable PG living</Text>
            </View>
            <Formik
                validationSchema={loginValidationSchema}
                initialValues={initialValues}
                onSubmit={values => handleLogin(values)}
                innerRef={formikRef}>
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    resetForm,
                }) => (
                    <>
                        <View style={styles.inputOuterView}>
                            <Image
                                source={icons.mobile}
                                style={{ width: 9.88, height: 17, tintColor: '#003F6F', bottom: 2 }}
                            />
                            <TextInput
                                placeholder="Enter your mobile no."
                                placeholderTextColor={'#003F6F'}
                                secureTextEntry={false}
                                keyboardType={'phone-pad'}
                                maxLength={10}
                                style={styles.textInput}
                                value={values.phoneNumber}
                                onBlur={handleBlur('phoneNumber')}
                                onChangeText={text => {
                                    handleChange('phoneNumber')(text);
                                    if (text.length === 10) {
                                        Keyboard.dismiss();
                                    }
                                }}
                                returnKeyType='done'
                            />
                        </View>
                        {errors.phoneNumber && touched.phoneNumber && (
                            <Text style={styles.errorText}>{errors.phoneNumber}</Text>
                        )}

                        {showOtpField == true ? (
                            <View style={{ marginTop: height / 20 - 20 }}>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: 'rgba(239, 239, 239, 1)',
                                        fontFamily: 'Poppins-Regular',
                                    }}>
                                    Enter OTP received on your registered mobile number
                                </Text>
                                <View style={{ marginTop: 18 }}>
                                    <CodeField
                                        ref={ref}
                                        {...props}
                                        value={value}
                                        onChangeText={setValue}
                                        cellCount={CELL_COUNT}
                                        autoComplete={'sms-otp'}
                                        // rootStyle={styles.codeFiledRoot}
                                        keyboardType="phone-pad"
                                        textContentType="oneTimeCode"
                                        renderCell={({ index, symbol, isFocused }) => (
                                            <View
                                                onLayout={getCellOnLayoutHandler(index)}
                                                key={index}
                                                style={[
                                                    styles.cellRoot,
                                                    isFocused && styles.focusCell,
                                                ]}>
                                                <Text
                                                    style={[
                                                        styles.cellText,
                                                        {
                                                            color: isFocused
                                                                ? 'rgba(239, 239, 239, 1)'
                                                                : '#003F6F',
                                                        },
                                                    ]}>
                                                    {symbol || (isFocused ? <Cursor /> : null)}
                                                </Text>
                                            </View>
                                        )}
                                    />
                                    {showOtpField == true ? (
                                        <View style={{ alignItems: 'center', }}>
                                            <TouchableOpacity
                                                disabled={otpTimer == 0 ? false : true}
                                                onPress={() => {
                                                    setOtpTimer(30), LoginWithOtpHandle(values);
                                                }}
                                            >
                                                <Text style={{
                                                    fontSize: 18,
                                                    color:
                                                        otpTimer == 0
                                                            ? '#003F6F'
                                                            : '#2B2B2B',
                                                    fontFamily: 'Poppins-Bold',
                                                }}>
                                                    {
                                                        otpTimer == 0
                                                            ? `Resend OTP`
                                                            : `Resend OTP (${otpTimer}s)`
                                                    }
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                    ) : null}
                                </View>
                            </View>
                        ) : null}
                        <View style={{ marginTop: showOtpField ? height / 7 : height / 3, alignItems: 'center' }}>
                            <TouchableOpacity style={styles.buttoncontainer} onPress={handleSubmit}>
                                <Text style={styles.buttontext}>{showOtpField ? 'Log In' : 'Request OTP'}</Text>
                            </TouchableOpacity>
                        </View>
                    </>
                )}
            </Formik>
        </ImageBackground>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
    },
    heading: {
        color: '#ffffff',
        fontFamily: 'Poppins-Medium',
        fontSize: 20
    },
    subheading: {
        color: '#ffffff',
        fontFamily: 'Poppins-Regular',
        fontSize: 16
    },
    textInput: {
        color: '#003F6F',
        fontFamily: 'Poppins-Medium',
        paddingHorizontal: 13,
        width: 300,
        fontSize: 16
    },
    inputOuterView: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        marginTop: height / 15 - 30,
        paddingHorizontal: 10,
        borderRadius: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderColor: '#003F6F'
    },
    errorText: {
        fontSize: 12,
        color: 'red',
        fontFamily: 'Poppins-Regular',
        top: 5,
    },
    cellRoot: {
        width: width / 8 - 4,
        height: height / 9 - 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#003F6F',
        borderWidth: 1,
        borderRadius: 10,
    },
    cellText: {
        fontSize: width / 8 - 9,
        textAlign: 'center',
        fontFamily: 'Poppins-Medium',
    },
    focusCell: {},
    buttontext: {
        fontSize: 15,
        color: 'rgba(239, 239, 239, 1)',
        fontFamily: 'Poppins-Medium',
    },
    buttoncontainer: {
        backgroundColor: '#003F6F',
        alignItems: 'center',
        justifyContent: 'center',
        height: 49,
        borderRadius: 10,
        width: width / 1.2

    }
})