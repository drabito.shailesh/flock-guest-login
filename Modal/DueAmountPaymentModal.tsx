import React, { useEffect, useRef, useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    Animated,
    Modal,
    Dimensions,
    StyleSheet,
    Platform,
    TouchableOpacity,
} from 'react-native';
import icons from '../Constant/icons';
import AllInOneSDKManager from 'paytm_allinone_react-native';
import axios from '../axiosConfig';

const { height, width } = Dimensions.get('window');

const DueAmountPaymentModal = ({ isVisible, onClose, navigation, DueAmountBookingList }: any) => {
    // console.log(items, '>>>>>>>>>>>>>>>>>')
    const modalAnimatedValue = useRef(new Animated.Value(0)).current;
    const [showDueAmountPaymentModal, setShowDueAmountPaymentModal] = useState(isVisible);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (showDueAmountPaymentModal) {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start();
        } else {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start(() => onClose());
        }
    }, [showDueAmountPaymentModal]);
    const modalY = modalAnimatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [height, height / 1.23]
    });


    const startPayment = async (amount: any, bookingId: any) => {
        const data = {
            amount: amount,
            paymentType: "Token",
        };
        try {
            const response = await axios.post(
                `properties/${''}/bookings/${bookingId}/payments/paytm`,
                data,
            );

            // console.log(response.data.data, '........../////')
            let orderId = response?.data?.data?.content?.body?.orderId
            let merId = response?.data?.data?.content?.body?.mid
            let txnt = response?.data?.data?.content?.payment_gateway_response?.txnToken
            let val = response?.data?.data?.content?.body?.value
            let callbackUrl = response?.data?.data?.content?.body?.callBackUrl
            let stag = true
            let appInkeReted = true


            await openPaytm(orderId, merId, txnt, val, callbackUrl, stag, appInkeReted)
        } catch (error: any) {
            console.log(error, '1111111111111111111111');
        }
    };

    const openPaytm = async (
        ordrId: any,
        merId: any,
        txnt: any,
        val: any,
        callbackUrl: any,
        stag: any,
        appInkeReted: any) => {
        AllInOneSDKManager.startTransaction(
            ordrId,
            merId,
            txnt,
            val,
            callbackUrl,
            stag,
            appInkeReted,
            ''
        )
            .then(async result => {
                console.log(result, 'then');
                // console.warn(res2.data);
            })
            .catch(err => {
                console.log(err, '/////////???????????????')
            });
    }

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={isVisible}
            statusBarTranslucent
            onRequestClose={() => setShowDueAmountPaymentModal(false)}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.4)',
                }}>
                {/* Transparent Background */}
                <TouchableWithoutFeedback onPress={() => setShowDueAmountPaymentModal(false)}>
                    <View
                        style={{
                            top: 0,
                            left: 0,
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                        }}
                    />
                </TouchableWithoutFeedback>
                <Animated.View
                    style={{
                        width: '100%',
                        height: '100%',
                        padding: width / 20,
                        borderTopRightRadius: 35,
                        borderTopLeftRadius: 35,
                        top: modalY,
                        backgroundColor: '#EEEBEB',
                    }}>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ height: 4, backgroundColor: '#003F6F', width: 50, borderRadius: 10 }} />
                    </View>
                    <View style={{ marginTop: 20 }}>
                        {DueAmountBookingList?.map((item: any, index: any) => {
                            return (
                                <>
                                    <TouchableOpacity key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "space-between", height: 45 }} onPress={() => startPayment(item?.dueAmount, item?.bookingId)}>
                                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>#{item?.bookingNo}</Text>
                                        <Text style={{ color: '#EF4141', fontSize: 14, fontFamily: 'Poppins-Medium' }}>{'\u20B9'} {item?.dueAmount}</Text>
                                    </TouchableOpacity>
                                    {
                                        DueAmountBookingList?.length == index + 1 ? null :
                                            <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 5 }} />
                                    }
                                </>

                            )
                        })}
                    </View>



                </Animated.View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 22,
        color: '#2B2B2B',
        textAlign: 'center',
    },
    containter: {
        alignItems: 'center',
    },
    buttonstyle: {
        //backgroundColor: 'red',
        height: 49,
        width: 140,
        justifyContent: 'center',
        borderRadius: 12,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#F8D491',
    },
    text1: {
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        color: '#22242580',
    },
    text2: {
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        color: '#222425',
    },
});

export default DueAmountPaymentModal;
