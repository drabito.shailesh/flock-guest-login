import React, { useEffect, useRef, useState } from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    Dimensions,
    Platform,
    TouchableOpacity,
    Animated,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';
import { useDispatch } from 'react-redux';
import icons from '../Constant/icons';
import { setBookingTypeAction } from '../redux/actions/BookingType-action';
import { setCurrentBookingDataAction } from '../redux/actions/CurrentBookingData-action';
import { useSelector } from 'react-redux';
import { setBookingStep1Action } from '../redux/actions/Step_1_bookingData-action';

const { height, width } = Dimensions.get('window');

const PaymentsTab = ({ navigation }: any) => {

    const dispatch = useDispatch()

    const [showBookingOptionModal, setShowBookingOptionModal] = useState(false);

    const modalAnimatedValue = useRef(new Animated.Value(0)).current;

    const handleTabPress = () => {
        // Open the modal when the tab is pressed
        setShowBookingOptionModal(true); // Replace 'YourModal' with the name of your modal screen
    };


    useEffect(() => {
        handleTabPress()
    }, [])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            handleTabPress()
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    return (
        <View style={{ flex: 1 }}>

            <Modal
                animationType="fade"
                transparent={true}
                visible={showBookingOptionModal}
                statusBarTranslucent
                onRequestClose={() => { navigation.goBack(), setShowBookingOptionModal(false) }}>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.7)',
                    }}>
                    {/* Transparent Background */}
                    <TouchableWithoutFeedback onPress={() => setShowBookingOptionModal(false)}>
                        <View
                            style={{
                                top: 0,
                                left: 0,
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                            }}
                        />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            height: '100%',
                            padding: width / 20,
                            borderTopRightRadius: 35,
                            borderTopLeftRadius: 35,
                            top: height / 1.17,
                            backgroundColor: '#EEEBEB',
                        }}>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ height: 4, backgroundColor: '#003F6F', width: 50, borderRadius: 10 }} />
                        </View>

                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { navigation.navigate('Step_1'), dispatch(setBookingTypeAction("DIRECT")), dispatch(setCurrentBookingDataAction({})), dispatch(setBookingStep1Action([])), setShowBookingOptionModal(false) }}>
                            <Image source={icons.direct} style={{ height: 30, width: 30 }} />
                            <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Direct Bookingvj</Text>
                        </TouchableOpacity>

                        <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 15 }} />

                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => {navigation.navigate('Step_1'), dispatch(setBookingTypeAction("CORPORATE")), dispatch(setCurrentBookingDataAction({})), setShowBookingOptionModal(false) }}>
                            <Image source={icons.business} style={{ height: 30, width: 30 }} />
                            <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Business Booking</Text>
                        </TouchableOpacity>

                    </Animated.View>
                </View>
            </Modal>

        </View>

    );
};


export default PaymentsTab