import React, { useEffect, useRef, useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    Animated,
    Modal,
    Dimensions,
    StyleSheet,
    Platform,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import icons from '../Constant/icons';
import moment from 'moment'

const { height, width } = Dimensions.get('window');

const PenaltiDetailModal = ({ isVisible, onClose, navigation, item }: any) => {
    // console.log(item, '>>>>>>>>>>>>>>>>>')
    const modalAnimatedValue = useRef(new Animated.Value(0)).current;
    const [showPenaltiDetailModal, setShowPenaltiDetailModal] = useState(isVisible);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (showPenaltiDetailModal) {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start();
        } else {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start(() => onClose());
        }
    }, [showPenaltiDetailModal]);
    const modalY = modalAnimatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [height, height / 2.3]
    });



    const Card = ({ label, value,style }: any) => {
        return (
            <View style={styles.wrap}>
                <Text style={styles.label}>{label}</Text>
                <Text style={[styles.value,{...style}]}>{value}</Text>
            </View>
        )
    }


    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={isVisible}
            statusBarTranslucent
            onRequestClose={() => setShowPenaltiDetailModal(false)}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.4)',
                }}>
                {/* Transparent Background */}
                <TouchableWithoutFeedback onPress={() => setShowPenaltiDetailModal(false)}>
                    <View
                        style={{
                            top: 0,
                            left: 0,
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                        }}
                    />
                </TouchableWithoutFeedback>
                <Animated.View
                    style={{
                        width: '100%',
                        height: '100%',
                        padding: 20,
                        borderTopRightRadius: 35,
                        borderTopLeftRadius: 35,
                        top: modalY,
                        backgroundColor: '#EEEBEB',
                    }}>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ height: 4, backgroundColor: '#003F6F', width: 50, borderRadius: 10 }} />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: height / 2.1, marginTop: 20 }}>
                        <Card
                            label='Date:'
                            value={moment(item?.date).format('MMMM DD, YYYY')}
                        />
                        <Card
                            label='Booking No:'
                            value={item?.bookingNo}
                        />
                        <Card
                            label='Amount:'
                            value={`\u20B9` + item?.amount}
                            style={{color:'#EF4141'}}
                        />
                        <Card
                            label='Guest Name:'
                            value={item?.guestName}
                        />
                        <Card
                            label='Create At:'
                            value={moment(item?.createdAt).format('MMMM DD, YYYY')}
                        />
                        <Card
                            label='Room Type:'
                            value={item?.roomType}
                        />
                        <Card
                            label='Room No:'
                            value={item?.roomNo}
                        />
                        <Card
                            label='Bed No:'
                            value={item?.bedNo}
                        />

                        <Card
                            label='Created By:'
                            value={item?.createdByUserName}
                        />
                        <Card
                            label='Description:'
                            value={item?.description}
                        />

                        <Card
                            label='Remarks:'
                            value={item?.remarks}
                        />
                        <View style={{ alignItems: 'center' }}>
                            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#003F6F', width: width / 2, borderRadius: 8, height: 40, marginTop: 15 }} onPress={() => { setShowPenaltiDetailModal(false) }}>
                                <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#ffffff" }}>Close</Text>
                            </TouchableOpacity>
                        </View>

                    </ScrollView>
                </Animated.View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    label: {
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
        color: '#22242580',
    },
    value: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 15,
        color: '#000000',
        // backgroundColor:'red',
        width: width / 1.6,
        textAlign: 'right'
    },
    wrap: {
        // borderColor: '#ccc',
        // borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        marginVertical: 5,
        paddingHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.1,
        elevation: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 8,
    }
});

export default PenaltiDetailModal;
