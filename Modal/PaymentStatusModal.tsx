import React from 'react';
import {
    Dimensions,
    ImageBackground,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import Modal from 'react-native-modal';
import icons from '../Constant/icons';
import Clipboard from '@react-native-community/clipboard';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import {successCustomMessage} from '../utils/SuccessFail';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('screen').height;

const PaymentStatusModal = (props: any) => {
    // console.log(props, '??????????????????');

    const copyLink = () => {
        Clipboard.setString(
            props.status.ORDERID
        );

        // successCustomMessage('Transaction ID copied successfully');
    };

    return (
        <View>
            <Modal
                isVisible={props.visible}
                animationIn={'fadeInUp'}
                animationOut={'fadeOutDown'}
                deviceWidth={deviceWidth}
                deviceHeight={deviceHeight}
                animationInTiming={500}
                animationOutTiming={500}
                swipeDirection={'down'}
                onSwipeComplete={() => {
                    props.dismiss_drawer();
                }}
                onBackdropPress={() => {
                    props.dismiss_drawer();
                }}
                backdropOpacity={0.2}
                onBackButtonPress={() => {
                    props.dismiss_drawer();
                }}
                style={styles.modal}
                useNativeDriver={true}>
                <Text></Text>
                <View style={styles.container}>
                    <View style={styles.seprator} />

                    <Text style={[styles.paymentHeading]}>
                        {' '}
                        {props?.status?.STATUS == 'TXN_SUCCESS' ||
                            props?.status?.result == 'payment_successfull'
                            ? 'Payment Successful!'
                            : props?.status?.STATUS == 'TXN_PENDING' ||
                                props?.status?.result == 'payment_pending'
                                ? 'Payment Pending!'
                                : 'Payment Failed!'}
                    </Text>

                    <Image
                        source={
                            props?.status?.STATUS == 'TXN_SUCCESS'
                                ? icons.paymentSuccess
                                : props?.status?.STATUS == 'TXN_PENDING'
                                    ? icons.paymentPending
                                    : icons.paymentFailed
                        }
                        style={{ height: 142, width: 142 }}
                    />
                    {props?.status?.TXNAMOUNT ? (
                        <Text
                            style={[
                                styles.amountHeading,
                                {
                                    color:
                                        props?.status?.STATUS == 'TXN_SUCCESS'
                                            ? '#5EE021'
                                            : props?.status?.STATUS == 'TXN_PENDING'
                                                ? '#E9B521'
                                                : '#EC3535',
                                    right: 10,
                                },
                            ]}>

                            {'\u20B9'} {props?.status?.TXNAMOUNT}
                        </Text>
                    ) : null}

                    {/* <Text style={[styles.heading, {textDecorationLine: 'underline'}]}>
            Status
          </Text>
          <Text style={styles.transactionID}>{props.status.RESPMSG}</Text> */}
                    {props.status.ORDERID ? (
                        <>
                            <Text
                                style={[
                                    styles.heading,
                                    { marginTop: 10, textDecorationLine: 'underline' },
                                ]}>
                                Transaction ID
                            </Text>
                            <View
                                style={{
                                    borderWidth: 1,
                                    paddingVertical: 5,
                                    borderRadius: 10,
                                    borderColor: '#003F6F',
                                    backgroundColor: '#003F6F20',
                                }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={[styles.transactionID, {}]}>
                                        {props.status.ORDERID}
                                    </Text>
                                    <TouchableOpacity
                                        style={{ height: 25, width: 25 }}
                                        onPress={() => copyLink()}>
                                        <Image
                                            source={icons.copy}
                                            style={{ height: 25, width: 25, tintColor: '#003F6F' }}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </>
                    ) : null}

                    <TouchableOpacity
                        style={styles.Button}
                        onPress={() => {
                            props.dismiss_drawer();
                        }}>
                        <Text style={styles.Buttonlabel}>Close</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        </View>
    );
};

export default PaymentStatusModal;

const styles = StyleSheet.create({
    modal: { margin: 0, flexDirection: 'column', justifyContent: 'space-between' },
    seprator: {
        backgroundColor: '#000000',
        height: 5,
        width: 76,
        borderRadius: 20,
    },
    container: {
        alignItems: 'center',
        backgroundColor: '#F9F9F9',
        padding: 25,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        marginTop: deviceHeight / 3.1,
    },
    paymentHeading: {
        fontSize: 21,
        fontFamily: 'NexaBold',
        color: '#222425',
        marginVertical: 20,
        textAlign: 'center',
    },
    amountHeading: {
        fontSize: 30,
        fontFamily: 'NexaBold',
        color: '#5EE021',
        marginVertical: 20,
        textAlign: 'center',
    },
    transactionID: {
        fontSize: 12.5,
        fontFamily: 'NexaBold',
        color: '#222425',
        paddingHorizontal: 10,
        textAlign: 'center',
    },
    heading: {
        fontSize: 18,
        fontFamily: 'NexaBold',
        color: '#000000',
        marginBottom: 10,
        textAlign: 'center',
    },
    Button: {
        backgroundColor: '#003F6F1a',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        width: 250,
        borderRadius: 10,
        marginVertical: 30,
        borderWidth:1,
        borderColor:"#003F6F"
    },
    Buttonlabel: {
        color: '#003F6F',
        fontSize: 16,
        fontFamily: 'Inter-Bold',
    },
});
