import React, { useEffect, useRef, useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    Animated,
    Modal,
    Dimensions,
    StyleSheet,
    Platform,
    TouchableOpacity,
} from 'react-native';
import icons from '../Constant/icons';

const { height, width } = Dimensions.get('window');

const BookingFilterModal = ({ isVisible, onClose, navigation, setBookingStatus }: any) => {
    // console.log(items, '>>>>>>>>>>>>>>>>>')
    const modalAnimatedValue = useRef(new Animated.Value(0)).current;
    const [showBookingFilterModal, setShowBookingFilterModal] = useState(isVisible);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (showBookingFilterModal) {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start();
        } else {
            Animated.timing(modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: false,
            }).start(() => onClose());
        }
    }, [showBookingFilterModal]);
    const modalY = modalAnimatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [height, height / 1.3]
    });


    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={isVisible}
            statusBarTranslucent
            onRequestClose={() => setShowBookingFilterModal(false)}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.4)',
                }}>
                {/* Transparent Background */}
                <TouchableWithoutFeedback onPress={() => setShowBookingFilterModal(false)}>
                    <View
                        style={{
                            top: 0,
                            left: 0,
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                        }}
                    />
                </TouchableWithoutFeedback>
                <Animated.View
                    style={{
                        width: '100%',
                        height: '100%',
                        padding: width / 20,
                        borderTopRightRadius: 35,
                        borderTopLeftRadius: 35,
                        top: modalY,
                        backgroundColor: '#EEEBEB',
                    }}>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ height: 4, backgroundColor: '#003F6F', width: 50, borderRadius: 10 }} />
                    </View>

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { setShowBookingFilterModal(false), setBookingStatus(4) }}>
                        <Image source={icons.ongoingbooking} style={{ height: 30, width: 30, tintColor: '#003F6F' }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Booked</Text>
                    </TouchableOpacity>

                    <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 15 }} />

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { setShowBookingFilterModal(false), setBookingStatus(6) }}>
                        <Image source={icons.completebooking} style={{ height: 30, width: 37.2, tintColor: '#003F6F' }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Completed</Text>
                    </TouchableOpacity>

                    <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 15 }} />

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { setShowBookingFilterModal(false), setBookingStatus(5) }}>
                        <Image source={icons.cancelbooking} style={{ height: 30, width: 30, tintColor: '#003F6F' }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Cancelled</Text>
                    </TouchableOpacity>

                </Animated.View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 22,
        color: '#2B2B2B',
        textAlign: 'center',
    },
    containter: {
        alignItems: 'center',
    },
    buttonstyle: {
        //backgroundColor: 'red',
        height: 49,
        width: 140,
        justifyContent: 'center',
        borderRadius: 12,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#F8D491',
    },
    text1: {
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        color: '#22242580',
    },
    text2: {
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        color: '#222425',
    },
});

export default BookingFilterModal;
