import React from 'react';
import {
    Dimensions,
    ImageBackground,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import Modal from 'react-native-modal';
import icons from '../Constant/icons';
import Clipboard from '@react-native-community/clipboard';
import { useDispatch } from 'react-redux';
import { setBookingTypeAction } from '../redux/actions/BookingType-action';
import { setCurrentBookingDataAction } from '../redux/actions/CurrentBookingData-action';
import { setBookingStep1Action } from '../redux/actions/Step_1_bookingData-action';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import {successCustomMessage} from '../utils/SuccessFail';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('screen').height;

const CreateBookingOptionModal = (props: any) => {
    // console.log(props, '??????????????????');

    const dispatch = useDispatch()

    return (
        <View>
            <Modal
                isVisible={props.visible}
                animationIn={'fadeInUp'}
                animationOut={'fadeOutDown'}
                deviceWidth={deviceWidth}
                deviceHeight={deviceHeight}
                animationInTiming={500}
                animationOutTiming={500}
                swipeDirection={'down'}
                onSwipeComplete={() => {
                    props.dismiss_drawer();
                }}
                onBackdropPress={() => {
                    props.dismiss_drawer();
                }}
                backdropOpacity={0.2}
                onBackButtonPress={() => {
                    props.dismiss_drawer();
                }}
                style={styles.modal}
                useNativeDriver={true}>
                <Text></Text>
                <View style={styles.container}>

                    <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text></Text>
                        <View style={{ height: 4, backgroundColor: '#003F6F', width: 50, borderRadius: 10, marginBottom: 20 }} />
                        <TouchableOpacity
                            style={styles.Button}
                            onPress={() => {
                                props.dismiss_drawer();
                            }}>
                             <Image source={icons.cross} style={{ height: 20, width: 20,opacity:0.5 }} />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { props.navigation.navigate('CreateBookings'), dispatch(setBookingTypeAction("DIRECT")), dispatch(setCurrentBookingDataAction({})),dispatch(setBookingStep1Action([])), props.dismiss_drawer() }}>
                        <Image source={icons.direct} style={{ height: 30, width: 30 }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Direct Booking</Text>
                    </TouchableOpacity>

                    <View style={{ height: 1, backgroundColor: '#003F6F', marginVertical: 15 }} />

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { props.navigation.navigate('CreateBookings'), dispatch(setBookingTypeAction("CORPORATE")), dispatch(setCurrentBookingDataAction({})),dispatch(setBookingStep1Action([])), props.dismiss_drawer() }}>
                        <Image source={icons.business} style={{ height: 30, width: 30 }} />
                        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', paddingHorizontal: 15, color: "#003F6F" }}>Business Booking</Text>
                    </TouchableOpacity>

                    {/* <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity
                            style={styles.Button}
                            onPress={() => {
                                props.dismiss_drawer();
                            }}>
                            <Text style={styles.Buttonlabel}>Close</Text>
                        </TouchableOpacity>
                    </View> */}

                </View>
            </Modal>
        </View>
    );
};

export default CreateBookingOptionModal;

const styles = StyleSheet.create({
    modal: { margin: 0, flexDirection: 'column', justifyContent: 'space-between' },
    seprator: {
        backgroundColor: '#000000',
        height: 5,
        width: 76,
        borderRadius: 20,
    },
    container: {
        // alignItems: 'center',
        backgroundColor: '#F9F9F9',
        padding: 25,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        // marginTop: deviceHeight / 2,
    },
    paymentHeading: {
        fontSize: 21,
        fontFamily: 'NexaBold',
        color: '#222425',
        marginVertical: 20,
        textAlign: 'center',
    },
    amountHeading: {
        fontSize: 30,
        fontFamily: 'NexaBold',
        color: '#5EE021',
        marginVertical: 20,
        textAlign: 'center',
    },
    transactionID: {
        fontSize: 12.5,
        fontFamily: 'NexaBold',
        color: '#222425',
        paddingHorizontal: 10,
        textAlign: 'center',
    },
    heading: {
        fontSize: 18,
        fontFamily: 'NexaBold',
        color: '#000000',
        marginBottom: 10,
        textAlign: 'center',
    },
    Button: {
        // backgroundColor: '#003F6F1a',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        borderRadius: 30,
        bottom:15
        // marginVertical: 30,
        // borderWidth: 1,
        // borderColor: "#003F6F"
    },
    Buttonlabel: {
        color: '#003F6F',
        fontSize: 16,
        fontFamily: 'Inter-Bold',
    },
});
