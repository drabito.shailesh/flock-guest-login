export const options = [
  {
    id: '28',
    name: 'Punjab',
  },
  {
    id: '31',
    name: 'Tamil Nadu',
  },
  {
    id: '16',
    name: 'Karnataka',
  },
  {
    id: '34',
    name: 'Uttar Pradesh',
  },
  {
    id: '23',
    name: 'Meghalaya',
  },
  {
    id: '22',
    name: 'Manipur',
  },
  {
    id: '29',
    name: 'Rajasthan',
  },
  {
    id: '9',
    name: 'Delhi',
  },
  {
    id: '6',
    name: 'Chandigarh',
  },
  {
    id: '17',
    name: 'Kerala',
  },
  {
    id: '19',
    name: 'Lakshadweep',
  },
  {
    id: '36',
    name: 'West Bengal',
  },
  {
    id: '12',
    name: 'Haryana',
  },
  {
    id: '10',
    name: 'Goa',
  },
  {
    id: '8',
    name: 'Dadra and Nagar Haveli and Daman and Diu',
  },
  {
    id: '5',
    name: 'Bihar',
  },
  {
    id: '21',
    name: 'Maharashtra',
  },
  {
    id: '33',
    name: 'Tripura',
  },
  {
    id: '7',
    name: 'Chhattisgarh',
  },
  {
    id: '4',
    name: 'Assam',
  },
  {
    id: '37',
    name: 'New Delhi',
  },
  {
    id: '27',
    name: 'Puducherry',
  },
  {
    id: '24',
    name: 'Mizoram',
  },
  {
    id: '3',
    name: 'Arunachal Pradesh',
  },
  {
    id: '15',
    name: 'Jharkhand',
  },
  {
    id: '1',
    name: 'Andaman and Nicobar Island',
  },
  {
    id: '18',
    name: 'Ladakh',
  },
  {
    id: '35',
    name: 'Uttarakhand',
  },
  {
    id: '32',
    name: 'Telangana',
  },
  {
    id: '2',
    name: 'Andhra Pradesh',
  },
  {
    id: '11',
    name: 'Gujarat',
  },
  {
    id: '20',
    name: 'Madhya Pradesh',
  },
  {
    id: '14',
    name: 'Jammu and Kashmir',
  },
  {
    id: '30',
    name: 'Sikkim',
  },
  {
    id: '26',
    name: 'Odisha',
  },
  {
    id: '13',
    name: 'Himachal Pradesh',
  },
  {
    id: '25',
    name: 'Nagaland',
  },
];


export const genderOptions = [
  {
    id: 1,
    name: 'MALE'
  },
  {
    id: 2,
    name: 'FEMALE'
  }
]


export const bloodGroupOption = [
  { id: 1, name: "AB+", enum: "AB_Positive" },
  { id: 2, name: "AB-", enum: "AB_Negative" },
  { id: 3, name: "A+", enum: "A_Positive" },
  { id: 4, name: "A-", enum: "A_Negative" },
  { id: 5, name: "B+", enum: "B_Positive" },
  { id: 6, name: "B-", enum: "B_Negative" },
  { id: 7, name: "O+", enum: "O_Positive" },
  { id: 8, name: "O-", enum: "O_Negative" },
]