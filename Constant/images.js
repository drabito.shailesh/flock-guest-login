export const logo = require('../assets/images/logo.png')
export const Backgroundlogin = require('../assets/images/login_background.png')
export const nobooking = require('../assets/images/nobooking.png')
export const noNotification = require('../assets/images/noNotification.png')
export const noPenalty = require('../assets/images/noPenalty.png')


export default {
    logo,
    Backgroundlogin,
    nobooking,
    noNotification,
    noPenalty
}