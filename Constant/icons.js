export const mobile = require('../assets/icons/mobile.png')
export const error = require('../assets/icons/error.png')
export const success = require('../assets/icons/success.png')
export const drawer = require('../assets/icons/drawer.png')
export const user = require('../assets/icons/user.png')
export const search = require('../assets/icons/search.png')
export const filter = require('../assets/icons/filter.png')
export const booking = require('../assets/icons/booking.png')

export const guest = require('../assets/icons/guest.png')
export const date = require('../assets/icons/date.png')
export const location = require('../assets/icons/location.png')
export const bed = require('../assets/icons/bed.png')
export const arrowright = require('../assets/icons/arrowright.png')
export const back = require('../assets/icons/back.png')
export const invoice = require('../assets/icons/invoice.png')
export const document = require('../assets/icons/document.png')
export const payment = require('../assets/icons/payment.png')
export const logout = require('../assets/icons/logout.png')
export const penalty = require('../assets/icons/penalty.png')
export const notification = require('../assets/icons/notification.png')

export const ongoingbooking = require('../assets/icons/ongoingbooking.png')
export const completebooking = require('../assets/icons/completebooking.png')
export const cancelbooking = require('../assets/icons/cancelbooking.png')

export const profile = require('../assets/icons/profile.png')
export const home = require('../assets/icons/home.png')
export const profile2 = require('../assets/icons/profile2.png')
export const home2 = require('../assets/icons/home2.png')
export const booking2 = require('../assets/icons/bookingicon.png')

export const downarrow = require('../assets/icons/downarrow.png')

export const rightArrow = require('../assets/icons/rightArrow.png')

export const view = require('../assets/icons/view.png')
export const download = require('../assets/icons/download.png')

export const wallet = require('../assets/icons/wallet.png')
export const tabbooking = require('../assets/icons/tabbooking.png')
export const room = require('../assets/icons/hotel.png')
export const notificationIcon = require('../assets/icons/notificationIcon.png')
export const Line = require('../assets/icons/Line.png')
export const direct = require('../assets/icons/direct.png')
export const business = require('../assets/icons/business.png')
export const plusTab = require('../assets/icons/plusTab.png')
export const delet = require('../assets/icons/delete.png')
export const plus = require('../assets/icons/plus.png')

export const paymentFailed = require('../assets/icons/paymentFailed.png')
export const paymentSuccess = require('../assets/icons/paymentSuccessful.png')
export const paymentPending = require('../assets/icons/paymentPending.png')
export const copy = require('../assets/icons/copy.png')
export const price = require('../assets/icons/price.png')
export const addbed = require('../assets/icons/addBed.png')
export const minus = require('../assets/icons/minus.png')
export const cross = require('../assets/icons/cross.png')
export default {
    mobile,
    success,
    error,
    drawer,
    user,
    search,
    filter,
    booking,
    location,
    bed,
    date,
    guest,
    arrowright,
    back,
    document,
    payment,
    invoice,
    logout,
    penalty,
    notification,
    cancelbooking,
    completebooking,
    ongoingbooking,
    profile,
    home,
    profile2,
    home2,
    booking2,
    downarrow,
    rightArrow,
    view,
    download,
    wallet,
    tabbooking,
    room,
    notificationIcon,
    Line,
    direct,
    business,
    plusTab,
    delet,
    plus,
    paymentFailed,
    paymentSuccess,
    paymentPending,
    copy,
    price,
    addbed,
    minus,
    cross
}