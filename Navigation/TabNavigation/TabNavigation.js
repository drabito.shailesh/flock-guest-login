import React, { useEffect, useRef, useState } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  View,
  Image,
  Text,
  Dimensions,
  Platform,
  TouchableOpacity,
  Animated,
} from 'react-native';
import icons from '../../Constant/icons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import InnerStackNavigation from '../InnerNavigation/InnerStackNavigation';
import Invoices from '../../Screens/Invoices/Invoices';
import Profile from '../../Screens/Profile';
import DashBoard from '../../Screens/DashBoard';
import Payments from '../../Screens/Payments/Payments';
import BookingList from '../../Screens/BookingList';
import OngoingBookings from '../../Screens/Bookings/OngoingBookings';
import CompletedBookings from '../../Screens/Bookings/CompletedBookings';
import UpComingBooking from '../../Screens/Bookings/UpComingBooking';
import UnPaidInvoice from '../../Screens/Invoices/UnPaidInvoice';
import PaidInvoice from '../../Screens/Invoices/PaidInvoice';
import { createStackNavigator } from '@react-navigation/stack';
import BookingDetail from '../../Screens/Bookings/BookingDetail';
import BookingPayments from '../../Screens/Bookings/BookingPayments';
import BookingPaidInvoice from '../../Screens/Bookings/BookingPaidInvoices';
import BookingUnPaidInvoice from '../../Screens/Bookings/BookingUnPaidInvoice';
import BookingPenalties from '../../Screens/Bookings/BookingPenalties';
import BookingDocument from '../../Screens/Bookings/BookingDocuments';
import PaymentsTab from '../../Modal/BookingOptionModal';
import Step_1 from '../../Screens/Create Booking/Step_1';
import Step_2 from '../../Screens/Create Booking/Step_2';
import Step_3 from '../../Screens/Create Booking/Step_3';
import Step_4 from '../../Screens/Create Booking/Step_4';
import Step_5 from '../../Screens/Create Booking/Step_5';
import CancelledBookings from '../../Screens/Bookings/CancelledBookings';

const { height, width } = Dimensions.get('window');

const Tab = createBottomTabNavigator();

const TopTab = createMaterialTopTabNavigator();

const Stack = createStackNavigator();



const TabBar = ({ navigation }) => {


  return (
    <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
      <Tab.Navigator
        // tabBar={CustomBottomTab}
        screenOptions={{
          tabBarStyle: {
            height: height / 11,
            borderTopLeftRadius: 32,
            borderTopRightRadius: 32,
            backgroundColor: '#FFFFFF',
            borderColor: '#FFFFFF',
            paddingTop: Platform.OS == 'ios' ? 15 : 0,
          },
          // tabBarLabelStyle: {
          // },
          tabBarShowLabel: false,
          headerShown: false,
        }}
        initialRouteName={'Bookings'}>
        <Tab.Screen name="Bookings" component={MyTopTabs}

          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View style={{ alignItems: 'center' }}>
                <Image
                  source={icons.tabbooking}
                  resizeMode="contain"
                  style={{ width: 22, height: 22, tintColor: focused ? '#003F6F' : '#BFBFBF' }}
                />
                <Text style={{ fontSize: 12, color: focused ? '#003F6F' : '#BFBFBF', fontFamily: 'Inter-Medium' }}>Bookings</Text>
              </View>
            ),
          }}

        />
        <Tab.Screen name="Payments" component={Payments}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View style={{ alignItems: 'center' }}>
                <Image
                  source={icons.wallet}
                  resizeMode="contain"
                  style={{ width: 24, height: 24, tintColor: focused ? '#003F6F' : '#BFBFBF' }}
                />
                <Text style={{ fontSize: 12, color: focused ? '#003F6F' : '#BFBFBF', fontFamily: 'Inter-Medium' }}>Payments</Text>
              </View>
            ),
          }}
        />
        {/* <Tab.Screen name="PaymentsTab" component={PaymentsTab}
          options={{
            headerShown: false,
            // tabBarButton: () => PaymentsTab(),
            tabBarIcon: ({ focused }) => (
              <View style={{ alignItems: 'center', justifyContent: 'center', bottom: 20 }}>
                <Image
                  source={icons.plusTab}
                  resizeMode="contain"
                  style={{ width: 64, height: 64 }}
                />
              </View>
            ),
          }}
        /> */}
        <Tab.Screen name="Invoices" component={InvoiceTopTabs}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View style={{ alignItems: 'center' }}>
                <Image
                  source={icons.invoice}
                  resizeMode="contain"
                  style={{ width: 18.57, height: 22, tintColor: focused ? '#003F6F' : '#BFBFBF' }}
                />
                <Text style={{ fontSize: 12, color: focused ? '#003F6F' : '#BFBFBF', fontFamily: 'Inter-Medium' }}>Invoices</Text>
              </View>
            ),
          }}
        />
        <Tab.Screen name="Profile" component={Profile}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View style={{ alignItems: 'center' }}>
                <Image
                  source={icons.user}
                  resizeMode="contain"
                  style={{ width: 24, height: 24, tintColor: focused ? '#003F6F' : '#BFBFBF' }}
                />
                <Text style={{ fontSize: 12, color: focused ? '#003F6F' : '#BFBFBF', fontFamily: 'Inter-Medium' }}>Profile</Text>
              </View>
            ),
          }}
        />

        <Tab.Screen name="BookingDetail" component={BookingDetailNavigation}
          options={{
            headerShown: false,
            tabBarButton: () => null,
          }}
        />
        {/* <Tab.Screen name="CreateBooking" component={CreateBookingNavigation}
          options={{
            headerShown: false,
            tabBarButton: () => null,
          }}
        /> */}
      </Tab.Navigator>

    </View>
  );
};

export default TabBar;



function MyTabBar({ state, descriptors, navigation, position }) {

  return (
    <View style={{ backgroundColor: '#FFFFFF', height: height / 6.5, elevation: 1, shadowColor: "#000", }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: height / 10 - 30, paddingHorizontal: 20,flex:1 }}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>

          <Image source={icons.drawer} style={{ height: 21, width: 21,tintColor:'#000000' }} />
        </TouchableOpacity>
        <Text style={{ color: '#272727', fontSize: 18, fontFamily: 'Inter-SemiBold' }}>Bookings</Text>


        <TouchableOpacity onPress={() => navigation.navigate('Notification')}>

          <Image source={icons.notification} style={{ height: 21, width: 21 }} />
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginTop:"5%",flex:1,marginBottom:5 }} >

        {state?.routes?.map((route, index) => {
          const { options } = descriptors[route.key];

          const label =
            options?.tabBarLabel !== undefined
              ? options?.tabBarLabel
              : options?.title !== undefined
                ? options?.title
                : route?.name;
          const isFocused = state?.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route?.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route?.name, route?.params);
            }
          };

          const inputRange = state?.routes?.map((_, i) => i);
          const opacity = position.interpolate({
            inputRange,
            outputRange: inputRange?.map(i => (i === index ? 1 : 0.6)),
          });

          return (
            <TouchableOpacity
              onPress={onPress}
              style={{ marginHorizontal: 10, alignItems: 'center', justifyContent: 'center' }}
            >
              <Animated.Text style={{ color: isFocused ? '#003F6F' : '#BFBFBF', opacity, fontFamily: 'Poppins-Medium', fontSize: 15, }}>
                {label}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

function InvoiceTabBar({ state, descriptors, navigation, position }) {

  return (
    <View style={{ backgroundColor: '#FFFFFF', height: height / 6.5, paddingHorizontal: 10, elevation: 2, shadowColor: "#000", }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: height / 10 - 40, paddingHorizontal: 10 }}>
        <TouchableOpacity style={{
          // backgroundColor: '#FAFAFA',
          height: 35,
          width: 35,
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center'
        }} onPress={() => navigation.goBack()}>
          <Image
            source={icons.back}
            style={{ height: 22, width: 12 }}
          />
        </TouchableOpacity>
        <Text style={{ color: '#272727', fontSize: 18, fontFamily: 'Inter-SemiBold' }}>Invoices</Text>
        <Text style={{ color: '#272727', fontSize: 18, fontFamily: 'Inter-SemiBold' }}></Text>
      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', marginTop: height / 15 - 35 }} >

        {state?.routes?.map((route, index) => {
          const { options } = descriptors[route.key];

          const label =
            options?.tabBarLabel !== undefined
              ? options?.tabBarLabel
              : options?.title !== undefined
                ? options?.title
                : route?.name;
          const isFocused = state?.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route?.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route?.name, route?.params);
            }
          };

          const inputRange = state?.routes?.map((_, i) => i);
          const opacity = position.interpolate({
            inputRange,
            outputRange: inputRange?.map(i => (i === index ? 1 : 0.6)),
          });

          return (
            <TouchableOpacity
              onPress={onPress}
              style={{ marginHorizontal: 10, alignItems: 'center', justifyContent: 'center' }}
            >
              <Animated.Text style={{ color: isFocused ? '#003F6F' : '#BFBFBF', opacity, fontFamily: 'Poppins-Medium', fontSize: 15 }}>
                {label}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

export const MyTopTabs = () => {
  return (
    <TopTab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      initialRouteName='Ongoing'
    >
      <TopTab.Screen
        options={{
          tabBarLabel: "Ongoing"
        }}
        name="Ongoing" component={OngoingBookings} />


      <TopTab.Screen
        options={{
          tabBarLabel: "Completed"
        }}
        name="Completed" component={CompletedBookings} />


      <TopTab.Screen
        options={{
          tabBarLabel: "Upcoming"
        }}
        name="Upcoming" component={UpComingBooking} />
        <TopTab.Screen
        options={{
          tabBarLabel: "Cancelled"
        }}
        name="Cancelled" component={CancelledBookings} />
    </TopTab.Navigator>
  );
}

export const InvoiceTopTabs = () => {
  return (
    <TopTab.Navigator
      tabBar={props => <InvoiceTabBar {...props} />}
      initialRouteName='Unpaid'
    >
      <TopTab.Screen
        options={{
          tabBarLabel: "Unpaid"
        }}
        name="Unpaid" component={UnPaidInvoice} />


      <TopTab.Screen
        options={{
          tabBarLabel: "Paid"
        }}
        name="Paid" component={PaidInvoice} />

    </TopTab.Navigator>
  );
}

function BookingInvoiceTabBar({ state, descriptors, navigation, position }) {

  return (
    <View style={{ backgroundColor: '#FFFFFF', height: height / 6.5, paddingHorizontal: 10, elevation: 2, shadowColor: "#000", }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: height / 10 - 40, paddingHorizontal: 10 }}>
        <TouchableOpacity style={{
          // backgroundColor: '#FAFAFA',
          height: 35,
          width: 35,
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center'
        }} onPress={() => navigation.goBack()}>
          <Image
            source={icons.back}
            style={{ height: 22, width: 12 }}
          />
        </TouchableOpacity>
        <Text style={{ color: '#272727', fontSize: 18, fontFamily: 'Inter-SemiBold' }}>Booking Invoices</Text>
        <Text style={{ color: '#272727', fontSize: 18, fontFamily: 'Inter-SemiBold' }}></Text>
      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', marginTop: height / 15 - 35 }} >

        {state?.routes?.map((route, index) => {
          const { options } = descriptors[route.key];

          const label =
            options?.tabBarLabel !== undefined
              ? options?.tabBarLabel
              : options?.title !== undefined
                ? options?.title
                : route?.name;
          const isFocused = state?.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route?.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route?.name, route?.params);
            }
          };

          const inputRange = state?.routes?.map((_, i) => i);
          const opacity = position.interpolate({
            inputRange,
            outputRange: inputRange?.map(i => (i === index ? 1 : 0.6)),
          });

          return (
            <TouchableOpacity
              onPress={onPress}
              style={{ marginHorizontal: 10, alignItems: 'center', justifyContent: 'center' }}
            >
              <Animated.Text style={{ color: isFocused ? '#003F6F' : '#BFBFBF', opacity, fontFamily: 'Poppins-Medium', fontSize: 15 }}>
                {label}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}


export const BookingInvoiceTopTabs = () => {
  return (
    <TopTab.Navigator
      tabBar={props => <BookingInvoiceTabBar {...props} />}
      initialRouteName='BookingUnpaid'
    >
      <TopTab.Screen
        options={{
          tabBarLabel: "Unpaid"
        }}
        name="BookingUnpaid" component={BookingUnPaidInvoice} />

      <TopTab.Screen
        options={{
          tabBarLabel: "Paid"
        }}
        name="BookingPaid" component={BookingPaidInvoice} />

    </TopTab.Navigator>
  );
}

export const BookingDetailNavigation = () => {

  const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
        animation: 'slide_from_right'
      }}
      initialRouteName={'BookingDetails'}>
      <Stack.Screen name="BookingDetails" component={BookingDetail}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="BookingPayments" component={BookingPayments}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="BookingInvoices" component={BookingInvoiceTopTabs}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}

      />

      <Stack.Screen name="BookingPenalties" component={BookingPenalties}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />

      <Stack.Screen name="BookingDocument" component={BookingDocument}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />

    </Stack.Navigator>

  );
};

export const CreateBookingNavigation = () => {

  const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
        animation: 'slide_from_right'
      }}
      initialRouteName={'Step_1'}>
      <Stack.Screen name="Step_1" component={Step_1}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="Step_2" component={Step_2}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="Step_3" component={Step_3}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}

      />

      <Stack.Screen name="Step_4" component={Step_4}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />


      <Stack.Screen name="Step_5" component={Step_5}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />

    </Stack.Navigator>

  );
};