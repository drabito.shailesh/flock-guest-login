import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import icons from '../../Constant/icons';

// type Props = {
//   route: string;
//   isFocused: boolean;
// };

const BottomTabIcon = ({route, isFocused}) => {
  const renderIcon = (route, isFocused) => {
    switch (route) {
      case 'Home':
        return (
          <Image
            source={isFocused ? icons.home : icons.home2}
            style={{height: 21.94, width: 20,marginHorizontal:4}}
          />
        );

      case 'Invoices':
        return (
          <Image
            source={icons.invoice}
            style={{height: 20, width: 23.34,marginHorizontal:4,tintColor:isFocused?'#273749':"#ffffff"}}
          />
        );
      case 'Bookings':
        return (
          <Image source={icons.booking2} style={{height:isFocused? 18:18, width:isFocused?17.79 :17.79,marginHorizontal:4,tintColor:isFocused?'#273749':"#ffffff"}} />
        );
      case 'Message':
        return (
          <Image
            source={isFocused ?icons.message:icons.message2}
            style={{height: 20, width: 21.43,right:8,top:2.5}}
          />
        );
      default:
        break;
    }
  };
  return <View>{renderIcon(route, isFocused)}</View>;
};

export default BottomTabIcon;

const styles = StyleSheet.create({});
