import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    // Animated,
} from 'react-native';
import BottomTabIcon from './BottomTabIcon';
import Animated, {
    useAnimatedStyle,
    withSpring,
    withTiming,
} from 'react-native-reanimated';

const { height, width } = Dimensions.get('window');

const CustomBottomTabs = ({ state, descriptors, navigation }) => {
    const MARGIN = 20;
    const TAB_BAR_WIDTH = width - 2 * 17;
    const TAB_WIDTH = TAB_BAR_WIDTH / state.routes.length;

    const [translateX] = useState(new Animated.Value(0));

    const handleTabPress = index => {
        // Animated.spring(translateX, {
        //   toValue: index * (TAB_WIDTH - 10),
        //   useNativeDriver: false,
        // }).start();

        navigation.navigate(state.routes[index].name, { merge: true });
    };

    // useEffect(() => {
    //   const unsubscribe = navigation.addListener('focus', () => {
    //     handleTabPress(3)
    //   });
    //   return unsubscribe;
    // }, [navigation]);

    // console.log(state.routes.length);

    const translateAnimation = useAnimatedStyle(() => {
        return {
            transform: [{ translateX: withTiming(state.index * TAB_WIDTH) }],
        };
    });

    return (
        <View
            style={[Styles.tabBarContainer, { width: TAB_BAR_WIDTH, bottom: MARGIN }]}>
            <Animated.View
                style={[
                    Styles.slidingTabContainer,
                    { width: TAB_WIDTH },
                    translateAnimation,
                ]}>
                <View
                    style={[
                        Styles.slidingTab,
                        {
                            width: TAB_WIDTH - 15
                            //     state.index == 0 || state.index == 3
                            //         ? TAB_WIDTH
                            //         : TAB_WIDTH + 17,
                            // right: state.index == 3 ? 15 : state.index == 0 ? -15 : 0,
                        },
                    ]}
                />
            </Animated.View>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const isFocused = state.index === index;

                return (
                    <TouchableOpacity
                        key={route.key}
                        // accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={() => handleTabPress(index)}>
                        <View style={Styles.contentContainer}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    // marginLeft: 10,
                                    alignItems: 'center',
                                }}>
                                <BottomTabIcon route={route.name} isFocused={isFocused} />
                                {isFocused ? (
                                    <Text
                                        style={{
                                            color: '#273749',
                                            fontSize: 13,
                                            fontFamily: 'Poppins-Medium',
                                            top: 2,
                                            // right: route.name == 'Message' ? 4 : 0,
                                        }}>
                                        {route.name}
                                    </Text>
                                ) : null}
                            </View>
                        </View>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
};

const Styles = StyleSheet.create({
    tabBarContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 65,
        position: 'absolute',
        alignSelf: 'center',
        backgroundColor: '#273749',
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'space-between',
        overflow: 'hidden',
        paddingHorizontal: 22
    },
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    slidingTab: {
        height: 40,
        borderRadius: 30,
        backgroundColor: '#ffffff',
    },
    // slidingTabContainer: {
    //   position: 'absolute',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    slidingTabContainer: {
        ...StyleSheet.absoluteFill,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default CustomBottomTabs;
