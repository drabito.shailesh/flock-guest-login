import React, { useEffect } from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Login from "../../Screens/AuthScreens/Login";

const Stack = createStackNavigator();

const AuthStackNavigation = () => {
   
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
            initialRouteName={'Login'}
        >
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
       
    )
}

export default AuthStackNavigation;