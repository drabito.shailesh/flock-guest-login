import React from 'react';
import { useColorScheme, StatusBar, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import InnerStackNavigation from '../InnerNavigation/InnerStackNavigation';
import AuthStackNavigation from '../AuthNavigation/AuthStackNavigation';
import { useSelector } from 'react-redux';
import Drawer from '../DrawerNavigation/DrawerNavigation';


const MainStack = () => {
    const token = useSelector((state) => state?.loginDataReducer?.loginData);
// console.log(token?.data?.token)
    return (
        <NavigationContainer
        // theme={scheme === 'dark' ? DarkTheme : MyTheme}
        >
            <StatusBar
                animated={true}
                translucent={true}
                backgroundColor={'transparent'}
            />

            {token?.data?.token == undefined ? (
                <AuthStackNavigation />
            ) : (
                <Drawer />
            )}


        </NavigationContainer>
    );
};

export default MainStack;
