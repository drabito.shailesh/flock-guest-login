import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  useNavigationContainerRef,
} from '@react-navigation/native';
import Step_1 from '../../Screens/Create Booking/Step_1';
import Step_2 from '../../Screens/Create Booking/Step_2';
import Step_3 from '../../Screens/Create Booking/Step_3';
import Step_4 from '../../Screens/Create Booking/Step_4';
import Step_5 from '../../Screens/Create Booking/Step_5';


const Stack = createStackNavigator();

const InnerStackNavigation = () => {
  const navigationRef = useNavigationContainerRef();

  const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
        animation: 'slide_from_right'
      }}
      initialRouteName={'Notification'}>
      {/* <Stack.Screen name="Notification" component={Notifications}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      /> */}

      <Stack.Screen name="Step_1" component={Step_1}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="Step_2" component={Step_2}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />
      <Stack.Screen name="Step_3" component={Step_3}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}

      />

      <Stack.Screen name="Step_4" component={Step_4}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />

      <Stack.Screen name="Step_5" component={Step_5}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      />

      {/* <Stack.Screen name="BookingDetail" component={BookingDetail}
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      /> */}
    </Stack.Navigator>

  );
};

export default InnerStackNavigation;
