import React from "react";
import { Dimensions, View, ScrollView } from "react-native";
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerItem from "../../components/DrawerItem";
import InnerStackNavigation from "../InnerNavigation/InnerStackNavigation";
import TabBar from "../TabNavigation/TabNavigation";
import Notifications from "../../Screens/Notification/Notifications";

const { height, width } = Dimensions.get('window')


const Drawer = ({ navigation }) => {

    const Drawer = createDrawerNavigator();
    return (
        <Drawer.Navigator
            drawerStyle={{
                backgroundColor: 'transparent',
            }}
            screenOptions={{
                swipeEdgeWidth: 0,
                drawerType: 'front',
                drawerStyle: {
                    width: width / 1.3 - 2,
                    borderTopRightRadius: 20,
                    borderBottomRightRadius: 20,
                    backgroundColor: '#FFFFFF'
                },
                headerShown: false,
                drawerContentOptions: {
                    labelStyle: {
                        marginLeft: 5,
                        fontFamily: 'Poppins-Medium',
                        color: '#FFFFFF',
                    },
                }
            }}

            drawerContent={
                props => (
                    <ScrollView style={{ height: height, marginTop: height / 10 - 20 }} showsVerticalScrollIndicator={false} >
                        <DrawerItem
                            {...props}
                        />
                    </ScrollView>
                )
            }
        >
            <Drawer.Screen name="TabBar" component={TabBar} />
            <Drawer.Screen name="Notification" component={Notifications} />
            <Drawer.Screen name="CreateBookings" component={InnerStackNavigation} />
        </Drawer.Navigator>
    )
}

export default Drawer;