import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
    LayoutAnimation,
    ScrollView,
    StyleSheet,
} from 'react-native';
import { useSelector } from 'react-redux';
import { bloodGroupOption } from '../Constant/DummyData';

const CustomDropdown = ({ options, onSelect, label, customStyle, setSelectBloodGroup, disabled }: any) => {

    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    // console.log(userDetails)

    const [isVisible, setIsVisible] = useState<boolean>(false);
    const [selectedOption, setSelectedOption] = useState<any>('');
    const [dropdownTop, setDropdownTop] = useState(0);

    const toggleDropdown = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        setIsVisible(!isVisible);
    };

    const handleSelect = (item: any) => {

        setSelectedOption(item?.name);
        if (label == 'Gender') {
            onSelect(item?.name);
        } else {
            setSelectBloodGroup(item?.enum)
        }
        toggleDropdown();
    };


    useEffect(() => {
        if (label == 'Gender') {
            onSelect(userDetails?.gender);
            setSelectedOption(userDetails?.gender);
        } else {
            const bloodgroup = bloodGroupOption?.find((item: any) => item.enum === userDetails?.bloodGroup);
            setSelectedOption(bloodgroup?.name)
            setSelectBloodGroup(bloodgroup?.enum)
        }
    }, [])


    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity
                onPress={toggleDropdown}
                disabled={disabled}
                style={styles.dropDownStyle}>
                <Text
                    style={{ fontSize: 14, fontFamily: 'Lato-Regular', color: '#003F6F' }}>
                    {selectedOption ? selectedOption : label}
                </Text>
            </TouchableOpacity>

            {isVisible && (
                <View style={[styles.dropDownListStyle, customStyle]}>
                    <FlatList
                        nestedScrollEnabled={true}
                        showsVerticalScrollIndicator={false}
                        data={options}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={() => handleSelect(item)}
                                style={{ height: 30 }}>
                                <Text
                                    style={{
                                        fontSize: 14,
                                        fontFamily: 'Lato-Regular',
                                        color: '#373737',
                                    }}>
                                    {item?.name}
                                </Text>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item?.id.toString()}
                    />
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    dropDownStyle: {
        height: 50,
        borderWidth: 1,
        justifyContent: 'center',
        paddingHorizontal: 15,
        borderRadius: 8,
        borderColor: '#E3E5E5',
        // marginVertical: 10,
    },
    dropDownListStyle: {
        backgroundColor: '#F5F5F5',
        borderColor: '#E3E5E5',
        zIndex: 1,
        height: 300,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        padding: 15,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1
    },
});

export default CustomDropdown;