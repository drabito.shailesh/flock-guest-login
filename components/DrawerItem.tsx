import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, Alert } from 'react-native'
import React from 'react'
import icons from '../Constant/icons';
import { useDispatch, useSelector } from 'react-redux';
import { setLoginDataAction } from '../redux/actions/login-action';
const { width, height } = Dimensions.get('window');


const DrawerItem = ({ ...props }) => {

    const dispatch = useDispatch();

    const userDetails = useSelector((state: any) => state?.userDataReducer?.userData);
    // console.log(userDetails, '>>>>>>>>>>>>>>>>>')

    const ButtonCard = ({ source, onPress, imageStyle, heading, source2, labelstyle }: any) => {
        return (
            <View style={{ marginTop: height / 25 - 10 }}>
                <TouchableOpacity style={styles.buttonCard} onPress={onPress}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={source} style={imageStyle} />
                        <Text style={{ color: '#212121', fontSize: 14, fontFamily: 'Poppins-Medium', paddingHorizontal: 10, ...labelstyle }}>{heading}</Text>
                    </View>
                    {/* <Image source={icons.arrowright} style={{ height: 10, width: 13, tintColor: "#003F6F" }} /> */}
                </TouchableOpacity>
            </View>
        )
    }


    const userLogout = () => {
        Alert.alert(
            "Logout",
            "Do you want to logout?",
            [
                {
                    text: "No",
                    onPress: () => {

                    },
                    style: "cancel"
                },
                {
                    text: "Yes", onPress: () => {
                        
                        dispatch(setLoginDataAction({}));


                    }
                }
            ]);
    }


    return (
        <View style={styles.container}>
            <View style={styles.profileHeader}>
                <TouchableOpacity style={styles.profilrbutton} onPress={() => props.navigation.navigate("Profile")}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={userDetails?.filePath != null ? { uri: userDetails?.filePath } : icons.user} style={{ height: 60, width: 60, borderRadius: 30,borderWidth:1,borderColor:"#0000001a" }} />
                        <View style={{ paddingHorizontal: 6,flex:1 }}>
                            <Text style={styles.userName} >{userDetails?.firstName?.charAt(0)?.toUpperCase() + userDetails?.firstName?.slice(1)} {userDetails?.lastName?.charAt(0)?.toUpperCase() + userDetails?.lastName?.slice(1)}</Text>
                            <View style={{}}>
                                <Text style={styles.propertyName}>{userDetails?.organizationName}</Text>
                                {/* <Text style={styles.flatdetail}>House 2-2</Text> */}
                            </View>
                        </View>
                    </View>
                    {/* <Image source={icons.arrowright} style={{ height: 10, width: 13, top: 16, tintColor: '#003F6F' }} /> */}
                </TouchableOpacity>
            </View>

            <View style={styles.seprator} />
            <View style={{ paddingHorizontal: 25 }}>
                <ButtonCard
                    source={icons.booking2}
                    heading={'Bookings'}
                    imageStyle={{ height: 21, width: 21, tintColor: "#003F6F" }}
                    onPress={() => props.navigation.navigate('Bookings')}
                />
                <ButtonCard
                    source={icons.invoice}
                    heading={'Invoice'}
                    imageStyle={{ width: 18.57, height: 22, tintColor: "#003F6F" }}
                    onPress={() => props.navigation.navigate('Invoices')}
                />
                <ButtonCard
                    source={icons.payment}
                    heading={'Payments'}
                    imageStyle={{ height: 21, width: 21, tintColor: "#003F6F" }}
                    onPress={() => props.navigation.navigate('Payments')}
                />
                <ButtonCard
                    source={icons.notification}
                    heading={'Notification'}
                    imageStyle={{ height: 21, width: 21, tintColor: "#003F6F" }}
                    onPress={() => props.navigation.navigate('Notification')}
                />
            </View>

            <View style={{ paddingHorizontal: 25 }}>
                <ButtonCard
                    source={icons.logout}
                    heading={'Log out'}
                    imageStyle={{ height: 21, width: 21, tintColor: "#CD172E" }}
                    labelstyle={{ color: '#CD172E' }}
                    onPress={() =>{ userLogout(),props.navigation.closeDrawer()}}
                />
            </View>
        </View>
    )
}

export default DrawerItem

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 20
    },
    profilrbutton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    profileHeader: {
        padding: 20
    },
    userName: {
        fontFamily: 'Manrope-Bold',
        fontSize: 18,
        color: '#003F6F',
        // flex:1
    },
    propertyName: {
        fontFamily: 'Manrope-Bold',
        fontSize: 15,
        color: '#616161',
        flex:1
    },
    flatdetail: {
        fontFamily: 'Manrope-Medium',
        fontSize: 12,
        color: '#616161',
        top: 2,
        paddingHorizontal: 5
    },
    seprator: {
        backgroundColor: "#f5f5f5",
        height: 5,
        marginTop: 15
    },
    updatetext: {
        color: '#BFBEBE',
        fontSize: 12,
        fontFamily: 'Manrope-SemiBold',
        marginTop: 15
    },
    buttonCard: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 5

    }

})