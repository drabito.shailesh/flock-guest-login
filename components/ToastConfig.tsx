import { StyleSheet, View,Image,Text } from "react-native";
import icons from "../Constant/icons";

export const toastConfig:any = {
    success: ({ text1, text2 }:any) => (
      <View style={styles.toast}>
        <Image source={icons.success} style={{height:25,width:25}} />
        <Text style={styles.toastText}>{text1}</Text>
        <Text style={styles.toastSubtext}>{text2}</Text>
      </View>
    ),
    error: ({ text1, text2 }:any) => (
      <View style={styles.errorToast}>
        <Image source={icons.error} style={{height:25,width:25}} />
        <Text style={styles.toastText}>{text1}</Text>
        <Text style={styles.toastSubtext}>{text2}</Text>
      </View>
    ),
    info: ({ text1, text2 }:any) => (
      <View style={styles.errorToast}>
        {/* <Image source={icons.error} style={{height:25,width:25}} /> */}
        <Text style={styles.toastText}>{text1}</Text>
        <Text style={styles.toastSubtext}>{text2}</Text>
      </View>
    ),
  };


  const styles = StyleSheet.create({
    toast: {
      backgroundColor: '#242C32',
      padding: 10,
      borderRadius: 5,
      flexDirection:"row",
      alignItems:'center',
      zIndex:1
      // width:width/1.02,
    },
    errorToast: {
      backgroundColor: '#242C32',
      padding: 10,
      borderRadius: 5,
      flexDirection:"row",
      alignItems:'center',
      zIndex:1
      // width:width/1.02,
    },
    toastText: {
      color: '#FFFFFF',
      fontSize: 16,
      fontFamily:'Poppins-SemiBold'
    },
    toastSubtext: {
      color: '#C8C5C5',
      fontSize: 16,
      fontFamily:'Poppins-Medium'
    },
  });