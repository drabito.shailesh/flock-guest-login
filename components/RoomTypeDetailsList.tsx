import React, { useState } from 'react'
import { StyleSheet, Text, View, LayoutChangeEvent, TouchableWithoutFeedback, Image } from 'react-native'
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
    FadeIn,
    Layout
} from "react-native-reanimated";
import icons from '../Constant/icons';
import moment from 'moment'



export const CollapsableContainer = ({
    children,
    expanded,
}: {
    children: React.ReactNode;
    expanded: boolean;
}) => {
    const [height, setHeight] = useState(0);
    const animatedHeight = useSharedValue(0);

    const onLayout = (event: LayoutChangeEvent) => {
        const onLayoutHeight = event.nativeEvent.layout.height;

        if (onLayoutHeight > 0 && height !== onLayoutHeight) {
            setHeight(onLayoutHeight);
        }
    };

    const collapsableStyle = useAnimatedStyle(() => {
        animatedHeight.value = expanded ? withTiming(height) : withTiming(0);

        return {
            height: animatedHeight.value,
        };
    }, [expanded]);

    return (
        <Animated.View style={[collapsableStyle, { overflow: "hidden" }]}>
            <View style={{ position: "absolute" }} onLayout={onLayout}>
                {children}
            </View>
        </Animated.View>
    );
};

const RoomTypeDetailsList = (item: any, navigation: any) => {

    const [expanded, setExpanded] = useState<boolean>(false);

    const onPressItem = () => {
        // console.log('first')
        setExpanded(!expanded)
    }
    // console.log(item.item.item, '???????????????????')
    return (
        <View>
            <TouchableWithoutFeedback
                // key={rooms?.roomType} 
                onPress={onPressItem}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ color: '#003F6F', fontFamily: 'Poppins-Medium', }}>{item?.item?.item?.roomType}</Text>
                    <Image source={icons.downarrow} style={{ height: 5, width: 8.75, marginHorizontal: 4 }} />
                </View>
            </TouchableWithoutFeedback>
            <CollapsableContainer expanded={expanded}>
                <View style={{}}>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Poppins-Medium' }}>Room no/Bed no: <Text style={{ color: '#000000' }}>{item?.item?.item?.roomNo}/{item?.item?.item?.bedNo}</Text> </Text>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Poppins-Medium' }}>Guest Name: <Text style={{ color: '#000000' }}>{item?.item?.item?.assignGuestName}</Text> </Text>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Poppins-Medium' }}>Check-In Date: <Text style={{ color: '#000000' }}>{moment(item?.item?.item?.actualCheckInDate).format('MMMM DD, YYYY')}</Text> </Text>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Poppins-Medium' }}>Check-Out Date: <Text style={{ color: '#000000' }}>{item?.item?.item?.actualCheckOutDate?moment(item?.item?.item?.actualCheckOutDate).format('MMMM DD, YYYY'):''}</Text> </Text>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Poppins-Medium' }}>Monthly Rent: <Text style={{ color: '#000000' }}>{item?.item?.item?.rate}</Text> </Text>

                </View>
            </CollapsableContainer>
        </View>
    )
}

export default RoomTypeDetailsList

const styles = StyleSheet.create({})



