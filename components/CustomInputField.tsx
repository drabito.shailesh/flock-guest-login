import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Animated,
  Easing,
  Image
} from 'react-native';
import icons from '../Constant/icons';

const AnimatedTextInput = ({
  label,
  secureTextEntry,
  value,
  onChangeText,
  editable,
  keyboardType,
  inputStyle
}:any) => {
  const [isFocused, setIsFocused] = useState(false);

  // Animation values
  const labelPosition = new Animated.Value(isFocused || value ? -10 : 12);
  const borderColor = isFocused ? '#003F6F' : '#E3E5E5';

  const handleFocus = () => {
    setIsFocused(true);
    // Animate the label when focused
    Animated.timing(labelPosition, {
      toValue: 0,
      duration: 500, // Adjust duration as needed
      easing: Easing.out(Easing.ease), // Smooth easing function
      useNativeDriver: false,
    }).start();
  };

  const handleBlur = () => {
    setIsFocused(false);
    // Animate the label back to its original position when blurred
    if (!value) {
      Animated.timing(labelPosition, {
        toValue: 1,
        duration: 500, // Adjust duration as needed
        easing: Easing.out(Easing.ease), // Smooth easing function
        useNativeDriver: false,
      }).start();
    }
  };

  return (
    <View style={styles.container}>
    <Animated.Text
      style={[styles.label, {top: labelPosition}]}
      >
      {label}
    </Animated.Text>
    <TextInput
      value={value}
      style={[styles.input, {borderColor,...inputStyle}]}
      onFocus={handleFocus}
      keyboardType={keyboardType}
      onBlur={handleBlur}
      secureTextEntry={secureTextEntry}
      // multiline={editable?}
      autoComplete='off'
      onChangeText={onChangeText}
      editable={editable}
    />
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 10,
  },
  label: {
    position: 'absolute',
    left: 10,
    paddingHorizontal: 5,
    color: '#003F6F',
    paddingBottom: 5,
    backgroundColor: '#FAFAFA',
    zIndex: 1,
  },
  input: {
    borderWidth: 1,
    paddingHorizontal: 15,
    borderRadius: 8,
    height: 50,
    backgroundColor: '#FAFAFA',
    color: '#373737',
  },
});

export default AnimatedTextInput;
