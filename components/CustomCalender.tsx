import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Animated,
  Modal,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {Calendar} from 'react-native-calendars';

const {height, width} = Dimensions.get('window');

const CustomCalendar = ({
  isVisible,
  onClose,
  currentDate,
  onDayPress,
  minDate,
  maxDate,
}:any) => {
  const modalAnimatedValue = useRef(new Animated.Value(0)).current;
  const [showCalendar, setShowCalendar] = useState(isVisible);

  useEffect(() => {
    if (showCalendar) {
      Animated.timing(modalAnimatedValue, {
        toValue: 1,
        duration: 100,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(modalAnimatedValue, {
        toValue: 1,
        duration: 100,
        useNativeDriver: false,
      }).start(() => onClose());
    }
  }, [showCalendar]);
  const modalY = modalAnimatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [height, height / 1.9],
  });

  return (
    <Modal
      animationType="fade"
      transparent={true}
      statusBarTranslucent
      visible={isVisible}
      onRequestClose={() => setShowCalendar(false)}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0, 0, 0, 0.65)',
        }}>
        {/* Transparent Background */}
        <TouchableWithoutFeedback onPress={() => setShowCalendar(false)}>
          <View
            style={{
              top: 0,
              left: 0,
              position: 'absolute',
              right: 0,
              bottom: 0,
            }}
          />
        </TouchableWithoutFeedback>
        <Animated.View
          style={{
            width: '100%',
            height: '100%',
            borderTopRightRadius: 35,
            borderTopLeftRadius: 35,
            backgroundColor: '#EEEBEB',
            top: modalY,
          }}>
          <Calendar
            current={currentDate}
            minDate={minDate}
            maxDate={maxDate}
            onDayPress={onDayPress}
            enableSwipeMonths={true}
            style={{
              borderTopRightRadius: 35,
              borderTopLeftRadius: 35,
              marginTop: 20,
            }}
            customStyles={{
              selectedDayBackgroundColor: {
                backgroundColor: 'red',
              },
            }}
            theme={{
              backgroundColor: '#EEEBEB',
              calendarBackground: '#EEEBEB',
              textSectionTitleColor: '#b6c1cd',
              textSectionTitleDisabledColor: '#d9e1e8',
              selectedDayTextColor: '#ffffff',
            //   selectedDayBackgroundColor: {},
              todayTextColor: '#003F6F',
              textDisabledColor: '#B5BEC6',
              arrowColor: '#B5BEC6',
              disabledArrowColor: '#d9e1e8',
              monthTextColor: '#3F3F3F',
              textDayFontFamily: 'Poppins-Bold',
              textMonthFontFamily: 'Poppins-Bold',
              textDayHeaderFontFamily: 'Poppins-Bold',
              textMonthFontWeight: 'Poppins-Bold',
              textDayFontSize: 16,
              textMonthFontSize: 20,
              textDayHeaderFontSize: 16,
              textDayHeaderFontWeight: '300',
              textDayFontWeight: '300',
              textMonthFontWeight: 'bold',
            }}
          />
        </Animated.View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 22,
    color: '#222425',
    textAlign: 'center',
    width: 270,
  },
});

export default CustomCalendar;
