import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist';
import { rootReducer } from './rootReducer';
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};
// const RootReducers = combineReducers({
//     // reducers
//     reducer,
// });
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
    let store = createStore(persistedReducer, applyMiddleware(thunk));
    let persistor = persistStore(store);
    return { store, persistor, thunk };
};