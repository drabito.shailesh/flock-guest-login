import { SET_BOOKING_ID } from "../constants/BookingId-contant";

const initialState = {
    isLoading: true,
    BookingId: {},
}

export default function BookingIdReducer(state = initialState, action) {
    const { type, payload } = action;


    switch (type) {
        case SET_BOOKING_ID: {
            return {
                ...state,
                BookingId: payload,
                isLoading:false
            }
        }
        default:
            return state
    }
}