import { SET_CURRENT_BOOKING_DATA } from "../constants/CurrentBookingData-constant";

const initialState = {
    isLoading: true,
    CurrentBookingData:{},
}

export default function CurrentBookingDataReducer(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case SET_CURRENT_BOOKING_DATA: {
            return {
                ...state,
                CurrentBookingData: payload,
                isLoading:false
            }
        }
        default:
            return state
    }
}