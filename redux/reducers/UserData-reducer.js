import { SET_USER_DATA } from "../constants/UserDetail-constant";

const initialState = {
    isLoading: true,
    userData: {},
}

export default function userDataReducer(state = initialState, action) {
    const { type, payload } = action;


    switch (type) {
        case SET_USER_DATA: {
            return {
                ...state,
                userData: payload,
                isLoading:false
            }
        }
        default:
            return state
    }
}