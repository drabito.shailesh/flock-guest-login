import { SET_LOGIN_TOKEN_DATA } from "../constants/Logintoken-constant";

const initialState = {
    token: null,
}

export default function LoginTokenDataReducer(state = initialState, action) {
    const { type, payload } = action;


    switch (type) {
        case SET_LOGIN_TOKEN_DATA: {
            return {
                ...state,
                token:payload
            }
        }
        default:
            return state
    }
}