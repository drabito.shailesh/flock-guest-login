import { SET_BOOKING_TYPE } from "../constants/BookingType-constant";

const initialState = {
    isLoading: true,
    BookingType: "",
}

export default function BookingTypeReducer(state = initialState, action) {
    const { type, payload } = action;
    // console.log(payload,'5666666666666666666666666')


    switch (type) {
        case SET_BOOKING_TYPE: {
            return {
                ...state,
                BookingType: payload,
                isLoading:false
            }
        }
        default:
            return state
    }
}