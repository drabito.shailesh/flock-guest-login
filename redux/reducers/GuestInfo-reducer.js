import { SET_BOOKING_INFO_DATA } from "../constants/GuestInfo-constant";

const initialState = {
    isLoading: true,
    GuestInfoData: {},
}

export default function GuestInfoDataReducer(state = initialState, action) {
    const { type, payload } = action;
    // console.log(payload,'5666666666666666666666666')


    switch (type) {
        case SET_BOOKING_INFO_DATA: {
            return {
                ...state,
                GuestInfoData: payload,
                isLoading: false
            }
        }
        default:
            return state
    }
}