import { SET_BOOKING_PROPERTY_ID } from "../constants/BookingProperty-constant";

const initialState = {
    isLoading: true,
    BookingProprtyId: null,
}

export default function BookingPropertyIdReducer(state = initialState, action) {
    const { type, payload } = action;
    // console.log(payload,'5666666666666666666666666')


    switch (type) {
        case SET_BOOKING_PROPERTY_ID: {
            return {
                ...state,
                BookingProprtyId: payload,
                isLoading: false
            }
        }
        default:
            return state
    }
}