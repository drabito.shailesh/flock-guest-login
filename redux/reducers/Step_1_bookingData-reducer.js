import { SET_BOOKING_STEP_1,SET_BOOKING_STEP_1_DATES } from "../constants/Step_1_bookingData-constant";

const initialState = {
    isLoading: true,
    BookingStep1: [],
    bookingDates:{}
}

export default function BookingStep1Reducer(state = initialState, action) {
    const { type, payload } = action;
    // console.log(payload,'?????????????')
    switch (type) {
        case SET_BOOKING_STEP_1: {
            return {
                ...state,
                BookingStep1: payload,
                isLoading: false
            }
        }
        case SET_BOOKING_STEP_1_DATES: {
            return {
                ...state,
                bookingDates: payload,
                isLoading: false
            }
        }
        default:
            return state
    }
}