import { SET_LOGIN_DATA } from "../constants/login-constant";

const initialState = {
    isLoading: true,
    loginData: {},
}

export default function loginDataReducer(state = initialState, action) {
    const { type, payload } = action;


    switch (type) {
        case SET_LOGIN_DATA: {
            return {
                ...state,
                loginData: payload,
                isLoading:false
            }
        }
        default:
            return state
    }
}