import { combineReducers } from "redux";
import LoginTokenDataReducer from "./reducers/LoginToken-Reducer";
import loginDataReducer from "./reducers/login-reducer";
import BookingIdReducer from "./reducers/BookingId-reducer";
import userDataReducer from "./reducers/UserData-reducer";
import BookingTypeReducer from "./reducers/BookingType-reducer";
import BookingStep1Reducer from "./reducers/Step_1_bookingData-reducer";
import BookingPropertyIdReducer from "./reducers/BookingProperty-reducer";
import GuestInfoDataReducer from "./reducers/GuestInfo-reducer";
import CurrentBookingDataReducer from "./reducers/CurrentBookingData-reducer";

const appReducer = combineReducers({
  LoginTokenDataReducer,
  loginDataReducer,
  BookingIdReducer,
  userDataReducer,
  BookingTypeReducer,
  BookingStep1Reducer,
  BookingPropertyIdReducer,
  GuestInfoDataReducer,
  CurrentBookingDataReducer
});

// const appReducer = combineReducers({
//   /* your app’s top-level reducers */
// })

export const rootReducer = (state, action) => {
  if (action?.type === 'USER_LOGOUT') {
    return appReducer(undefined, action)
  }

  return appReducer(state, action)
}
