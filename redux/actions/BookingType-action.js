import { SET_BOOKING_TYPE } from "../constants/BookingType-constant"


export const setBookingTypeAction = (data) => {
    return { type: SET_BOOKING_TYPE, payload: data }
}