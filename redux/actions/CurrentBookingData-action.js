import { SET_CURRENT_BOOKING_DATA } from "../constants/CurrentBookingData-constant"


export const setCurrentBookingDataAction = (data) => {

    return { type: SET_CURRENT_BOOKING_DATA, payload: data }
}