import { SET_BOOKING_INFO_DATA } from "../constants/GuestInfo-constant"

export const setGuestInfoDataAction = (data) => {
    return { type: SET_BOOKING_INFO_DATA, payload: data }
}