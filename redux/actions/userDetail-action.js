import { SET_USER_DATA } from "../constants/UserDetail-constant"


export const setUserDataAction = (data) => {
    return { type: SET_USER_DATA, payload: data }
}