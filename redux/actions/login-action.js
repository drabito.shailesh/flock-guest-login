import { SET_LOGIN_DATA } from "../constants/login-constant";


export const setLoginDataAction = (data) => {
    return { type: SET_LOGIN_DATA, payload: data }
}