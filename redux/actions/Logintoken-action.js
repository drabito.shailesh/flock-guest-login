import { SET_LOGIN_TOKEN_DATA } from "../constants/Logintoken-constant";

export const setBiometricPermissionAction = data => {
  return {type: SET_LOGIN_TOKEN_DATA, payload: data};
};
