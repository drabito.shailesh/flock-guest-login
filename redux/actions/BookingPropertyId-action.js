import { SET_BOOKING_PROPERTY_ID } from "../constants/BookingProperty-constant"


export const setBookingPropertyIdAction = (data) => {
    return { type: SET_BOOKING_PROPERTY_ID, payload: data }
}