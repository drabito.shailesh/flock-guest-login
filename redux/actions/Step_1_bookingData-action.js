import { SET_BOOKING_STEP_1, SET_BOOKING_STEP_1_DATES } from "../constants/Step_1_bookingData-constant"


export const setBookingStep1Action = (data) => {
    return { type: SET_BOOKING_STEP_1, payload: data }
}

export const setBookingStep1DatesAction = (data) => {
    
    return { type: SET_BOOKING_STEP_1_DATES, payload: data }
}