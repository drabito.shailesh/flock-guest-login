import { SET_BOOKING_ID } from "../constants/BookingId-contant"


export const setBookingIdAction = (data) => {

    return { type: SET_BOOKING_ID, payload: data }
}