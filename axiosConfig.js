import axios from 'axios';
import { getToken } from './utils/functions/index';



const API = axios.create({

  // production

  baseURL: 'https://pgapidev.inn4smart.com/api/',

  // devlopment 

  // baseURL: 'https://app.myfotocart.com:8090/api',
  timeout: 60000,
});
API.interceptors.request.use(
  async config => {
    const token = await getToken();
    config.headers = {
      Authorization: `Bearer ${token}`,
      // origin: 'http://localhost:3021'
      origin:'https://develop.d1nlz14addr1w4.amplifyapp.com'
    };
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);


export default API;









