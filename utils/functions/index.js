import AsyncStorage from "@react-native-async-storage/async-storage";

export const getToken = async () => {
    let token = null;
    try {
      token = await AsyncStorage.getItem('token');
    } catch (e) {
      console.log(e);
    }
    return token;
  };


  
export const objectToFormData = (obj, form, namespace) => {
  console.log('objectToFormData called')
  var fd = form || new FormData();
  var formKey;
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (namespace) {
        formKey = namespace + '.' + property;
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File;
      // use recursively.
      if (obj[property] instanceof Date || obj[property] instanceof moment) {
        fd.append(formKey, moment(obj[property]).toISOString());
      } else if (
        typeof obj[property] === 'object' &&
        !(obj[property] instanceof File)
      ) {
        objectToFormData(obj[property], fd, formKey);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }
  return fd;
};