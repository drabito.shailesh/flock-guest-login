import React from 'react'
import { View } from 'react-native'
import {ActivityIndicator} from 'react-native-paper';

const CustomIndicator = () => {
    return (
        <View style={{height:60, alignItems: 'center',justifyContent:'flex-start',}}>
        <ActivityIndicator size="large" color="#003F6F" />
      </View>
    )
}

export default CustomIndicator