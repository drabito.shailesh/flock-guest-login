import React from 'react';
import {StyleSheet,ActivityIndicator, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');

const Loader = () => {
  return (
    <ActivityIndicator style={{zIndex:1,position:'absolute',top:height/2,right:width/2-10}} size="large" color="#003F6F" />
  );
};

export default Loader;

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  lottieView: {
    zIndex: 2,
  },
});
